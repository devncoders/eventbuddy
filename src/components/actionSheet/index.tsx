import { Colors, Constants, Utils } from 'common';
import * as React from 'react';
import { Text, View, StyleSheet, Pressable } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/FontAwesome5';
import *  as ImagePicker from 'react-native-image-picker';
import { updateProfile, uploadMedia } from 'reduxStore/services/user';
import { MediaType, PhotoQuality } from 'react-native-image-picker';
import { RouteProp, useNavigation } from '@react-navigation/native';
import _ from 'lodash'
interface ActionSheetProps {
    onImageSuccess: (mediaObj: {
        type: string,
        name: string,
        uri: string
    }) => void
    onCancel: () => void
}

let initial = {
    fileName: '',
    imageType: '',
    picture: '',
    isLoading: false,
    showLoginView: false
}

let cameraOptions = {
    mediaType: 'photo' as MediaType,
    quality: __DEV__ ? 0.01 : 0.7 as PhotoQuality,
    maxWidth: 300,
    maxHeight: 300,
}
interface VerifyPictureProps {
    route: RouteProp<{
        params: {
            picture: '',
            fileName: '',
            imageType: ''
        }
    }, 'params'>
}

const ActionSheet = (props: ActionSheetProps) => {
    let navigation = useNavigation();
    const [{
        isLoading,
        fileName,
        imageType,
        picture
    }, setState] = React.useState(initial);

    const updateState = (state: {}) => {
        setState(prevState => ({ ...prevState, ...state }));
    }


    const onImageSuccess = (response: ImagePicker.ImagePickerResponse) => {
        let assets = response.assets || [];
        let uri = assets[0] && assets[0].uri || '';
        let type = assets[0] && assets[0].type || '';
        let name = assets[0] && assets[0].fileName || '';
        const realPath = Constants.platform === 'ios' ? uri.replace('file://', '') : uri;
        Utils.getResolvedPath(realPath).then(pathRes => {
            var str1 = "file://";
            var str2 = pathRes.path;
            var correctpath = str1.concat(str2);
            props.onImageSuccess({
                type,
                name,
                uri: correctpath
            });
            onCancel();
        }).catch(err => {
            console.log('=====> ', err)
        })
    }

    const openCamera = (type: number) => {
        if (type === 2 || __DEV__) {
            ImagePicker.launchImageLibrary(
                cameraOptions,
                async (response: ImagePicker.ImagePickerResponse) => {
                    if (!response.errorCode && !response.didCancel) {
                        onImageSuccess(response)
                    } else {
                        updateState({ isLoading: false });
                    }
                },
            );
            return
        }
        else {
            Utils.checkPermissionForUsage('camera')
                .then(res => {
                    ImagePicker.launchCamera(
                        cameraOptions,
                        async response => {
                            onImageSuccess(response);
                        },
                    );
                })
                .catch(error => {
                    Utils.showToast('Camera Permssion required');
                    updateState({ isLoading: false });
                })
        }
    }
    let viewRef = React.useRef<Animatable.View>(null);
    let viewRefMain = React.useRef<Animatable.View>(null);
    const onCancel = () => {
        viewRef.current.fadeOutDown(800)
        viewRefMain.current.fadeOut(1000).then(() => props.onCancel())
    }
    return (
        <Animatable.View duration={500} ref={viewRefMain} animation={'fadeIn'} style={styles.container}>
            <Pressable onPress={() => props.onCancel()} style={{ flex: 1, }} />
            <View style={styles.subContainer}>
                <Animatable.View duration={500} ref={viewRef} animation={'fadeInUp'} >
                    <Pressable style={styles.button} onPress={() => openCamera(1)}>
                        <Icon size={wp(5)} color={Colors.primary} name='camera' />
                        <Text style={styles.buttonTitle}>Camera</Text>
                    </Pressable>
                    <Pressable style={styles.button} onPress={() => openCamera(2)}>
                        <Icon size={wp(5)} color={Colors.primary} name='image' />
                        <Text style={styles.buttonTitle}>Gallery</Text>
                    </Pressable>
                    <Pressable style={styles.button} onPress={() => onCancel()}>
                        <Text style={[styles.buttonTitle, styles.cancelButton]}>Cancel</Text>
                    </Pressable>
                </Animatable.View>
            </View>
        </Animatable.View>
    );
};

export { ActionSheet };

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        zIndex: 999,
        width: Constants.width,
        height: '100%',
        backgroundColor: 'rgba(0,0,0,0.6)',
    },
    subContainer: {
        width: wp(100),
        padding: wp(4)
    },
    button: {
        marginVertical: wp(2),
        paddingHorizontal: wp(3),
        height: hp(5),
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: Colors.white,
        borderRadius: wp(2)
    },
    buttonTitle: {
        paddingHorizontal: wp(4),
        color: Colors.textDark
    },
    cancelButton: {},
});
