import React, { useState } from 'react';
import { Text, View, Image, ImageProps, StyleSheet, TextInputProps, TextInput, Pressable } from 'react-native';
import { Colors, Constants } from 'common';
import { IconButton, Surface } from 'react-native-paper';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
interface AuthInputProps extends TextInputProps {
    isPassword?: boolean
    isPhone?: boolean
    rightIcon?: ImageProps
    leftIcon?: any
    call_code?: string
    label?: string
    selectCallCode?: (code: string) => void
}

const AuthInput = (props: AuthInputProps) => {
    const [isSecure, setSecure] = useState(props.isPassword)
    const [callCode, setCallCode] = useState(props.call_code)
    const [showFLags, setFlagState] = useState(false);

    return (
        <Surface style={[styles.inputContainer, props.multiline && { height: 150, alignItems: 'flex-start', }, props.style]}>
            {props.leftIcon && <View style={{ paddingHorizontal: wp(3) }}>
                <Image source={props.leftIcon} style={{ width: 20, height: 20, resizeMode: 'contain' }}
                />
            </View>}
            <View style={{ flex: 1 }}>
                <TextInput
                    {...props}
                    style={[styles.input, props.multiline && { height: 140, paddingTop: 12 }, props.isPhone && { paddingLeft: 10 }]}
                    secureTextEntry={isSecure}
                    textAlignVertical={props.multiline ? 'top' : undefined}
                    autoCapitalize='none'
                    autoCorrect={false}
                    placeholderTextColor={Colors.placeholderColor}
                />
            </View>
            {props.isPassword && <Pressable
                onPress={() => setSecure(!isSecure)}
            >
                <Image style={[styles.eyeIcon, !isSecure && { tintColor: Colors.primary }]} source={Constants.eye_icon} />
            </Pressable>}
            {props.rightIcon && <Image source={props.rightIcon} style={styles.rightIconImage} />}
        </Surface>
    );
};

AuthInput.defaultProps = {
    rightIcon: null,
    call_code: '',
    label: '',
}

export { AuthInput };

const styles = StyleSheet.create({
    eyeIcon: {
        width: 20,
        height: 20,
        marginHorizontal: wp(2)
    },

    rightIconImage: {
        width: wp(5),
        height: wp(5),
        marginHorizontal: wp(2),
        resizeMode: 'contain'
    },
    phoneCodeContainer: {
        paddingHorizontal: 0,
        borderRightColor: Colors.borderColor,
        borderRightWidth: .5
    },
    phoneCode: {
        paddingHorizontal: 10,
        color: Colors.textDark,
    },
    container: {
    },
    lableContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    inputContainer: {
        paddingVertical: hp(.5),
        paddingHorizontal: wp(2),
        elevation: 3,
        borderRadius: 10,
        alignItems: 'center',
        flexDirection: 'row',
    },
    input: {
        paddingVertical: wp(2),
        paddingRight: wp(2),
        height: hp(5.5),
        fontSize: wp(3),
        color: Colors.black,
        fontWeight: '400',
        fontFamily: Constants.popins_regular
    },
    label: {
        color: '#C0C6CD',
        fontSize: wp(2.5),
        fontFamily: Constants.popins_regular
    }
});
