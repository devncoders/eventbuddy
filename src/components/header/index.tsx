
import { useNavigation } from '@react-navigation/core';
import { Colors, Constants, Utils } from 'common';
import * as React from 'react';
import { Text, View, StyleSheet, Image, Pressable } from 'react-native';
import { Appbar } from 'react-native-paper';
import { widthPercentageToDP } from 'react-native-responsive-screen';

interface HeaderProps {
    title: string
    onRightPress?: () => void
    leftImage: JSX.Element
    rightIcon?: any
    headerStyle?: any
    defaultColor?: any
}

const Header = (props: HeaderProps) => {
    const navigation = useNavigation();
    return (
        <View style={{ overflow: 'hidden', borderBottomRightRadius: 20, borderBottomLeftRadius: 20, ...props.headerStyle }}>
            <Appbar.Header style={{ height: 90, borderBottomRightRadius: 15, borderBottomLeftRadius: 15, backgroundColor: Colors.white }}>
                {navigation.canGoBack() && <Pressable
                    onPress={() => navigation.goBack()}>
                    <Image source={Constants.back} style={{ paddingHorizontal: 6, width: 35, height: 23, resizeMode: 'contain' }} />
                </Pressable>}
                {props.leftImage && <Appbar.Action icon={() => props.leftImage} onPress={() => navigation.toggleDrawer()} />}
                <Appbar.Content titleStyle={{ fontFamily: Constants.NiveauGroteskBold, fontWeight: '700', color: '#0E134F', fontSize: widthPercentageToDP(5) }} title={props.title} />
                {props.rightIcon && <Appbar.Action icon={props.rightIcon} color={props.defaultColor} onPress={() => props.onRightPress && props.onRightPress()} />}
                {!props.rightIcon && <Appbar.Action icon={'camera'} color='transparent' onPress={() => { }} />}
            </Appbar.Header>
        </View>
    );
};

Header.defaultProps = {
    title: '',
    onRightPress: null,
    leftImage: null,
    rightIcon: null,
    defaultColor: Colors.primary
}

export { Header };

const styles = StyleSheet.create({
    container: {}
});
