import * as React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Colors, Constants } from 'common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

interface LabelProps {
    textEng?: string
    style?: any
}

const Label = (props: LabelProps) => {
    return (
        <View style={styles.lableContainer}>
            <Text style={{ ...styles.label, ...props.style }}>{props.textEng}</Text >
        </View>
    );
};

export { Label };

const styles = StyleSheet.create({
    label: {
        color: '#333333',
        fontFamily: Constants.popinsMed,
        fontSize: wp(4),
        paddingBottom: 10,
        paddingHorizontal: wp(4),
    },
    lableContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
});
