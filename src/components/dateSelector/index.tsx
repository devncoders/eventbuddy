import React, { useState } from 'react';
import { Text, View, StyleSheet, TextInputProps, TextInput, Pressable, Image } from 'react-native';
import { Colors, Constants } from 'common';
import { Label } from 'components';
import Icon from 'react-native-vector-icons/FontAwesome5';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

interface DateSelectorProps extends TextInputProps {
    placeholder: string
    iconName?: any,
    type: any,
    onDateSelect?: (date: string) => void
}

const DateSelector = (props: DateSelectorProps) => {
    const [
        dateSelected,
        setDate
    ] = useState(props.placeholder);
    var maxDate = new Date();
    // maxDate.setFullYear(maxDate.getFullYear() - );
    const [
        pickerDate,
        setPickerDate
    ] = useState(maxDate);
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };

    const handleConfirm = (date: any) => {
        let newDob = moment(date).format(props.type === 'date' ? 'DD-MMM-YYYY' : 'hh:mm A');
        setDate(newDob);
        props.onDateSelect && props.onDateSelect(newDob);
        setPickerDate(date);
        hideDatePicker();
    };
    return (
        <View style={styles.container}>
            <Pressable onPress={() => showDatePicker()} style={styles.inputContainer}>
                <Image source={props.type === 'date' ? Constants.setting_3 : Constants.clock}
                    style={{ width: 17, height: 17, resizeMode: 'contain' }}
                />
                <View style={{ paddingHorizontal: 5, flex: 1, }}>
                    <Text style={[styles.input, dateSelected == props.placeholder && { color: Colors.placeholderColor }]}>{dateSelected || 'Date Of Birth'}</Text>
                </View>
            </Pressable>
            <DateTimePickerModal
                isVisible={isDatePickerVisible}
                mode={props.type}
                date={pickerDate}
                // display={props.type === 'date' ? 'calendar' : 'clock'}
                minimumDate={maxDate}
                onConfirm={handleConfirm}
                onCancel={hideDatePicker}
            />
        </View>
    );
};

export { DateSelector };

const styles = StyleSheet.create({
    container: {},
    phoneCodeContainer: {
        paddingHorizontal: 10,
        borderRightColor: Colors.primary,
        borderRightWidth: 1
    },
    phoneCode: {
        paddingHorizontal: 10,
        color: Colors.textDark,
    },
    /* container: {
        paddingVertical: 10
    }, */
    lableContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    inputContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        borderRadius: 10,
        height: 50,
        // backgroundColor: Colors.tabBg,
        borderWidth: 1,
        borderColor: Colors.borderColor,
        paddingHorizontal: 10
    },
    input: {
        paddingVertical: 15,
        fontSize: wp(3),
        color: Colors.primary,
        paddingHorizontal: wp(2)
    },
    label: {
        color: Colors.primary,
        fontSize: 15,
        fontWeight: '500'
    }
});
