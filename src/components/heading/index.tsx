import { Colors, Constants } from 'common';
import * as React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, } from 'react-native-responsive-screen';

interface HeadingProps {
    style: any,
    text: string,
    type: number
    spacing: number
}

const Heading = (props: HeadingProps) => {
    let {
        type = 1,
        text = '',
        spacing = 1
    } = props;
    let styleKey = { ...styles.heading, paddingVertical: wp(spacing) };
    if (type === 2) {
        styleKey = { ...styles.subHeading, paddingVertical: wp(spacing) };
    }
    return (
        <Text style={[styleKey, props.style]}>{text}</Text>
    );
};

Heading.defaultProps = {
    type: 1,
    text: '',
    style: {},
    spacing: 1
}

export { Heading };

const styles = StyleSheet.create({
    heading: {
        fontSize: wp(4.5),
        color: Colors.fontPrimary,
        fontFamily: Constants.popinsMed
    },
    subHeading: {
        fontSize: wp(3.5),
        color: Colors.fontSeconday,
        fontFamily: Constants.popins_regular
    }
});
