import { Colors, Constants } from 'common';
import * as React from 'react';
import { Text, View, TextInput, StyleSheet, TextInputProps, Dimensions, Image } from 'react-native';
import { Surface } from 'react-native-paper';
const {
    width, height
} = Dimensions.get('window')
import Icon from 'react-native-vector-icons/FontAwesome'

interface SearchBarProps extends TextInputProps {
    iconRight: boolean,
    searchIconColor: number
}

const SearchBar = (props: SearchBarProps) => {

    let {
        iconRight,
        searchIconColor
    } = props;
    return (
        <View style={styles.container}>
            <Surface style={styles.subContainer}>
                {!iconRight && <View style={styles.iconView}>
                    <Icon size={width * .04} color={searchIconColor} name='search' /></View>}
                <View style={{ flex: 1 }}>
                    <TextInput
                        style={[{ color: Colors.primary, paddingHorizontal: width * .02, height: height * .05, }, props.style]}
                        {...props}
                    />
                </View>
                {iconRight && <View style={styles.iconView}>
                    <Icon size={width * .04} color={searchIconColor} name='search' /></View>}
            </Surface>
        </View>
    );
};
SearchBar.defaultProps = {
    searchIconColor: Colors.textGrey,
    iconRight: true,
    style: {},
    returnKeyType: 'search',
    placeholderTextColor: Colors.placeholderColor,
}

export { SearchBar };

const styles = StyleSheet.create({
    subContainer: {
        elevation: 3,
        paddingHorizontal: width * .05,
        borderRadius: 25,
        height: height * .05,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Colors.white,
    },
    container: {
        backgroundColor: Colors.tabBg,
        padding: width * .05,
    },
    iconView: {
        width: width * .07,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    icon: {
        width: width * .04,
        height: width * .04,
        resizeMode: 'contain'
    },
});
