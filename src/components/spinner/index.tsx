import { Colors } from 'common';
import React from 'react';
import { ActivityIndicator, View, Dimensions, StyleSheet, Text } from 'react-native';

const { width, height } = Dimensions.get("window");
interface SpinnerProps {
    text?: string,
    bgColor?: any
}
const Spinner = (props: SpinnerProps) => {

    return (
        <View style={[styles.spinnerStyle, props.bgColor ? { backgroundColor: props.bgColor } : null, StyleSheet.absoluteFill]}>
            <ActivityIndicator color={Colors.primary} animating size={'large'} />
            {props.text ? <Text numberOfLines={1} style={styles.loadingText}>{props.text}</Text> : null}
        </View>
    );
};

const styles = StyleSheet.create({
    loadingText: {
        color: Colors.primary,
        padding: 10,
    },
    spinnerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: "absolute",
        zIndex: 9999,
        backgroundColor: 'rgba(24, 68, 97, 0.2)',
    }
});

export { Spinner };