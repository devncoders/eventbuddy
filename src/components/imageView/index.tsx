import * as React from 'react';
import {
  Image,
  ImageProps,
  StyleSheet,
  ImageBackground,
  Text,
} from 'react-native';
import { Colors, Constants } from 'common';

interface ImageViewProps extends ImageProps {
  containerStyle?: any;
  placeholderImage?: string;
}

const ImageView = (props: ImageViewProps) => {
  const [isLoaded, setIsLoaded] = React.useState(false);
  const [isError, setError] = React.useState(false);
  return (
    <>
      <ImageBackground
        onError={() => {
          setError(true);
        }}
        onLoadEnd={() => {
          setIsLoaded(true);
        }}
        // source={{uri:'https://wallpaperaccess.com/full/295657.jpg'}}
        source={props.source}
        resizeMode={props.resizeMode}
        style={[props.style, { position: 'relative' }]}>
        {/* <Image style={[styles.image,props.style]} source={props.source}/> */}
        {props.source == '' || !props.source || !isLoaded || (isLoaded && isError) ? (
          <>
            <Image
              resizeMode='contain'
              style={[
                styles.image,
                props.style,
                { resizeMode: 'contain', position: 'absolute', zIndex: 9 },
              ]}
              source={Constants.error_logo}
            />
            {/* <Text style={{color:'red'}}>{`${loading} ${isError}`}</Text> */}
          </>
        ) : null}
      </ImageBackground>
    </>
  );
};
ImageView.defaulProps = {
  resizeMode: 'cover',
  placeholderImage: '',
  style: {
    width: 0,
    height: 0,
  },
};

export { ImageView };

const styles = StyleSheet.create({
  /* placeholderImage:{
    width:
  }, */
  image: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    tintColor: Colors.placeholderColor
  },
});
