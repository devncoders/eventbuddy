import React, { useState } from 'react';
import { Text, View, StyleSheet, TextInputProps, TextInput, Pressable } from 'react-native';
import { Colors } from 'common';
import { Label } from 'components';
import Icon from 'react-native-vector-icons/FontAwesome5';

interface InputSelectorProps extends TextInputProps {
    lableEng: string
    lableUrdu?: string
    selectedValue: string
    placeholder: string
    onValueChange: (value: string) => void
    data: Array<any>
}

const InputSelector = (props: InputSelectorProps) => {
    let {
        selectedValue,
        placeholder,
        onValueChange,
        data: dataSet
    } = props;
    const [
        data,
        setData
    ] = useState(dataSet);
    const [
        isPickerShown,
        setPickerState
    ] = useState(false);
    return (
        <View style={styles.container}>
            <Label textEng={props.lableEng} textUrdu={props.lableUrdu || props.lableEng} />
            <Pressable onPress={() => setPickerState(true)} style={styles.inputContainer}>
                <View style={{ flex: 1, }}>
                    <Text style={[styles.input, selectedValue == '' && { color: Colors.placeholderColor }]}>{selectedValue || placeholder}</Text>
                </View>
                <Icon
                    color={Colors.primary}
                    size={17}
                    name='chevron-down'
                />
            </Pressable>

        </View>
    );
};

InputSelector.defaultProps = {
    data: [],
    placeholder: '',
    selectedValue: '',
}

export { InputSelector };

const styles = StyleSheet.create({
    phoneCodeContainer: {
        paddingHorizontal: 10,
        borderRightColor: Colors.primary,
        borderRightWidth: 1
    },
    phoneCode: {
        paddingHorizontal: 10,
        color: Colors.textDark,
    },
    container: {
        paddingVertical: 10
    },
    lableContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    inputContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        // borderRadius: 10,
        borderBottomWidth: 0.5,
        borderBottomColor: Colors.borderColor,
        // backgroundColor: Colors.tabBg,
        marginTop: 10,
        paddingRight: 10
    },
    input: {
        paddingVertical: 15,
    },
    label: {
        color: Colors.primary,
        fontSize: 15,
        fontWeight: '500'
    }
});
