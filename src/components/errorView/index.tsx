import { Colors, Constants } from 'common';
import * as React from 'react';
import { Text, View, StyleSheet, Image, Pressable } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/FontAwesome5';

interface ErrorViewProps {
    style?: any,
    errorMessage?: string
    onRefresh?: () => void
}

const ErrorView = (props: ErrorViewProps) => {
    return (
        <Pressable onPress={() => props.onRefresh && props.onRefresh()} style={[styles.container, props.style]}>
            <Image source={Constants.error_logo} style={styles.logo} />
            <Text style={styles.errorText}>{props.errorMessage}</Text>
            {props.onRefresh && <Icon style={{ marginTop: 10 }} name='redo' size={wp(5)} color={Colors.textGrey} />}
        </Pressable>

    );
};

export { ErrorView };

const styles = StyleSheet.create({
    logo: {
        width: wp(25),
        height: wp(25),
        resizeMode: 'contain',
        tintColor: Colors.textGrey
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    errorText: {
        textAlign: 'center',
        fontSize: wp(4),
        color: Colors.textGrey,
        fontFamily: Constants.NiveauGroteskRegular
    }
});
