import { Colors, Constants } from 'common';
import * as React from 'react';
import { Text, Pressable, StyleSheet, TouchableOpacityProps, Image } from 'react-native';
import { ActivityIndicator, Button as ButtonPaper, } from 'react-native-paper';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';

interface ButtonProps extends TouchableOpacityProps {
    text: string,
    emptyButton?: boolean
    isLoading?: boolean
    titleColor?: any
    leftIcon?: any,
    children?: JSX.Element
}

const Button = (props: ButtonProps) => {
    return (
        <Pressable disabled={props.disabled} onPress={props.onPress}>
            <LinearGradient
                start={{ x: 0.0, y: 0.80 }}
                end={{ x: 0.80, y: 1.0 }}
                style={[styles.buttonContainer, props.emptyButton ? { backgroundColor: Colors.white, borderColor: Colors.primary, borderWidth: 1 } : Constants.boxShadow, props.style, props.disabled && { opacity: 0.4 }]}
                colors={!props.emptyButton ? [Colors.button1, Colors.button2] : [Colors.white, Colors.white]}>
                {props.leftIcon && <Image style={styles.buttonIconStyle} source={props.leftIcon} />}
                {props.children}
                {!props.isLoading && <Text style={[styles.title, props.emptyButton && { color: Colors.primary }, props.titleColor && { color: props.titleColor }]}>{props.text}</Text>}
                {props.isLoading && <ActivityIndicator color={Colors.white} size={12} />}
            </LinearGradient>
        </Pressable>
    );
};

Button.defaultProps = {
    text: '',
    disabled: false,
    isLoading: false,
}

export { Button };

const styles = StyleSheet.create({
    buttonIconStyle: {
        width: widthPercentageToDP(6),
        resizeMode: 'contain',
        marginHorizontal: widthPercentageToDP(1),
        height: widthPercentageToDP(5),
        tintColor: Colors.white
    },
    buttonBg: {
    },
    buttonContainer: {
        paddingHorizontal: widthPercentageToDP(8),
        // width: widthPercentageToDP(84),
        height: heightPercentageToDP(5.6),
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50,
        // backgroundColor: Colors.primary,

    },
    title: {
        color: Colors.white,
        fontSize: widthPercentageToDP(4),
        fontFamily: Constants.NiveauGroteskBold
    }
});
