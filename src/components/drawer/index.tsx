import { Colors, Constants, Utils } from 'common';
import * as React from 'react';
import { Text, View, Image, StyleSheet, ImageBackground, Pressable, Alert } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { Avatar, Button, Subheading, Title } from 'react-native-paper';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { NavigationScreenProp, NavigationState } from "react-navigation";

interface DrawerProps {
    navigation: NavigationScreenProp<NavigationState>;
}
const getIcon = (title: string) => {
    let iconName: any = Constants.home;
    if (title === 'Edit Profile') return Constants.user
    else if (title === 'About Us') return Constants.about
    else if (title === 'Privacy Policy') return Constants.home_tab
    else if (title === 'Change Password') return Constants.terms
    else if (title === 'Contact Us') return Constants.contact
    else return iconName
}
const DrawerItem = ({ title, onPress }: { title: string, onPress: () => void }) => {

    return <Pressable onPress={onPress} style={styles.drawerItemContainer}>
        <Image source={getIcon(title)} style={styles.drawerItemIcon} />
        <Text style={styles.drawerItemTitle}>{title}</Text>
    </Pressable>
}

const Drawer = (props: DrawerProps) => {

    const onLogout = () => {
        Alert.alert('Logout!', 'Are you sure you want to logout?', [
            { text: 'No', onPress: () => { } },
            { text: 'Yes', onPress: () => props.navigation.replace('login') }
        ])
    }

    return (
        <ImageBackground source={Constants.splashBg} style={[styles.container, Constants.platform === 'android' && { paddingHorizontal: wp(2) }]}>
            {Constants.platform === 'android' && <View style={{ height: 30 }} />}
            <View style={styles.userInfoView}>
                <Avatar.Image source={Constants.profile_image} />
                <Text style={styles.title}>John Snow</Text>
                <Text style={styles.subTitle}>johnbotham@gmail.com</Text>
            </View>
            <View style={{ paddingVertical: hp(2), flex: 1, }}>
                <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
                    <DrawerItem onPress={() => props.navigation.replace('main', { screen: 'dashboard' })} title='Home' />
                    <DrawerItem onPress={() => props.navigation.navigate('aboutUs', { title: 'About Us' })} title='About Us' />
                    <DrawerItem onPress={() => props.navigation.navigate('myAccount')} title='Edit Profile' />
                    <DrawerItem onPress={() => props.navigation.navigate('changePassword')} title='Change Password' />
                    <DrawerItem onPress={() => props.navigation.navigate('aboutUs', { title: 'Privacy Policy' })} title='Privacy Policy' />
                    <DrawerItem onPress={() => props.navigation.navigate('contactUs')} title='Contact Us' />
                    <View style={{ height: hp(2) }} />
                </ScrollView>
            </View>
            <Button
                onPress={() => onLogout()}
                uppercase={false}
                labelStyle={{ fontFamily: Constants.popinsMed }}
                color={Colors.tabSelected}
                mode='contained'
                style={styles.logoutButton}
                contentStyle={styles.logoutButtonContent} >
                Logout
            </Button>
            {Constants.platform === 'android' && <View style={{ height: 50 }} />}
        </ImageBackground>
    );
};

export default Drawer;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    userInfoView: {
        paddingHorizontal: wp(3),
        paddingVertical: hp(2),
    },
    title: {
        color: Colors.white,
        fontFamily: Constants.popins_bold,
        fontSize: wp(5),
        paddingTop: hp(1.5)
    },
    subTitle: {
        color: Colors.white,
        fontFamily: Constants.popins_regular,
        opacity: .6,
        fontSize: wp(3),
        paddingTop: hp(1)
    },
    drawerItemContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: hp(1),
        paddingHorizontal: wp(1.5),
        marginBottom: hp(1)
    },
    drawerItemIcon: {
        tintColor: Colors.white,
        width: wp(5),
        height: wp(5),
        resizeMode: 'contain',
        marginHorizontal: wp(2)
    },
    drawerItemTitle: {
        fontFamily: Constants.popins,
        color: Colors.white,
        fontSize: wp(3.8)
    },
    logoutButton: {
        alignSelf: 'flex-start',
        marginHorizontal: wp(5),
        marginTop: hp(1),
        borderRadius: 50,
    },
    logoutButtonContent: {
        paddingHorizontal: wp(3.5),
        paddingVertical: wp(1),
    }
});
