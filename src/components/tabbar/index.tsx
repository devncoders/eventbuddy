import { Colors, Constants } from 'common';
import * as React from 'react';
import { Text, Image, View, StyleSheet, Dimensions, Pressable, TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/FontAwesome';
import BottomBarComponent from './curvedTabbar';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { FAB } from 'react-native-paper';
let {
    width,
    height
} = Dimensions.get('window');
interface TabbarProps {
    routeName: string
    onScanPress: () => void
    onTabPress: (routeName: string) => void
}

const Tabbar = (props: TabbarProps) => {
    const [isOpen, setOpenState] = React.useState(false);
    const ref = React.useRef();

    const _renderIcon = (routeName: string, selectTab: string) => {
        let icon = '';

        switch (routeName) {
            case 'profile':
                icon = 'address-card';
                break;
            default:
                icon = 'home';
                break;
        }
        return (
            <Icon name={icon} size={wp(6)} color={routeName === props.routeName ? Colors.tabSelected : Colors.textGrey} />
        );
    }

    return (
        <>
            <View style={styles.container}>
                <BottomBarComponent
                    ref={ref}
                    type={'down'}
                    height={60}
                    circleWidth={55}
                    bgColor="white"
                    borderTopLeftRight={true}
                    strokeWidth={2}
                    swipeEnabled={true}
                    initialRouteName="dashboard"
                    renderCircle={({ selectTab, navigate }) => (
                        <View
                            style={[styles.btnCircle]}>

                            <TouchableOpacity
                                onPress={() => props.onScanPress()}
                            // onPress={() => setOpenState(true)}
                            >
                                <Image source={Constants.scan} style={styles.scanIcon} />
                            </TouchableOpacity>
                        </View>
                    )}
                    tabBar={({ routeName, selectTab, navigate }) => {
                        return (
                            <TouchableOpacity
                                onPress={() => props.onTabPress(routeName)}
                                style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                {_renderIcon(routeName, selectTab)}
                            </TouchableOpacity>
                        );
                    }}
                    leftItems={[
                        { title: 'Home', view: 'dashboard' }
                    ]}
                    rightItems={[
                        { title: 'My Card', view: 'profile' }
                    ]}
                >
                </BottomBarComponent>
            </View>
        </>
    );
};

export default Tabbar;

const styles = StyleSheet.create({
    btnCircle: {
        width: 60,
        height: 60,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.tabSelected,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 1,
        bottom: 28
    },
    container: {
        minHeight: height * .07,
    },
    subContainner: {
        flexDirection: 'row',
        height: height * .05,
    },
    homeTabIcon: {
        width: width * .2,
        height: height * .04,
        resizeMode: 'contain',
        tintColor: Colors.textGrey
    },
    scanIcon: {
        width: 30,
        height: 40,
        resizeMode: 'contain',
        tintColor: Colors.white
    }
});
