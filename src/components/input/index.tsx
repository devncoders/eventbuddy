import React, { useState } from 'react';
import { Text, View, Image, ImageProps, StyleSheet, TextInputProps, TextInput, Pressable } from 'react-native';
import { Colors, Constants } from 'common';
// import CountryPicker, { CountryCode } from 'react-native-country-picker-modal'

import { IconButton, Surface } from 'react-native-paper';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
interface InputProps extends TextInputProps {
    isPassword?: boolean
    isButton?: boolean
    leftIcon?: boolean
    rightIcon?: ImageProps
    selectCallCode?: (code: string) => void
}

const Input = (props: InputProps) => {
    const [isSecure, setSecure] = useState(props.isPassword);
    return (
        <View style={[styles.inputContainer, props.multiline && { height: 150, alignItems: 'flex-start', }]}>
            {props.leftIcon && <Image style={{ width: 15, height: 15, resizeMode: 'contain' }} source={Constants.location} />}
            <View style={{ flex: 1 }}>
                {!props.isButton ? <TextInput
                    {...props}
                    style={[styles.input, props.multiline && { paddingTop: 12 }]}
                    secureTextEntry={isSecure}
                    placeholderTextColor={Colors.placeholderColor}
                /> : <Text style={[styles.input, props.value === '' && { color: Colors.placeholderColor }]}>{props.value || props.placeholder}</Text>}
            </View>
            {props.isPassword && <IconButton
                color={Colors.seconday}
                size={20}
                onPress={() => setSecure(!isSecure)} icon={isSecure ? 'lock' : 'lock-open'} />}
            {props.rightIcon && <Image source={props.rightIcon} style={styles.rightIconImage} />}
        </View>
    );
};

Input.defaultProps = {
    rightIcon: null,
    call_code: ''
}

export { Input };

const styles = StyleSheet.create({
    rightIconImage: {
        width: wp(5),
        height: wp(5),
        marginHorizontal: wp(2),
        resizeMode: 'contain'
    },
    phoneCodeContainer: {
        paddingHorizontal: 0,
        borderRightColor: Colors.borderColor,
        borderRightWidth: .5
    },
    phoneCode: {
        paddingHorizontal: 10,
        color: Colors.textDark,
    },
    container: {
    },
    lableContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    inputContainer: {
        height: 50,
        // paddingVertical: hp(.5),
        paddingHorizontal: wp(2),
        borderWidth: .5,
        borderColor: Colors.borderColor,
        borderRadius: 10,
        alignItems: 'center',
        flexDirection: 'row',
    },
    input: {
        paddingVertical: 15,
        paddingHorizontal: wp(2),
        color: Colors.primary,
        fontSize: wp(3),
        fontFamily: Constants.popinsMed
    },
    label: {
        color: Colors.primary,
        fontSize: 15,
        fontWeight: '500'
    }
});
