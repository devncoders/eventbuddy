import * as React from 'react';
import { Text, TouchableOpacity, View, StyleSheet, Pressable, ScrollView, KeyboardAvoidingView, Image, Alert } from 'react-native';
import { NavigationScreenProp, NavigationState } from "react-navigation";
import { Header, Input, Button, Label } from 'components';
import { Colors, Constants, Utils } from 'common';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { Avatar, IconButton } from 'react-native-paper';



interface ContactUsProps {
    navigation: NavigationScreenProp<NavigationState>
}
let initialState = {
    first_name: '',
    last_name: '',
    email: '',
    call_code: '71',
    phone_number: '',
    address: '',
    isLoading: false,
}
const ContactUs = (props: ContactUsProps) => {
    const [
        {
            isLoading,
            first_name,
            last_name,
            email,
            call_code,
            phone_number,
            address
        }, setState
    ] = React.useState(initialState);

    const updateState = (newState: {}) => {
        setState(prevState => ({ ...prevState, ...newState }))
    }

    const isDisabled = () => {
        return first_name === '' || last_name === ''
            || !Utils.validateEmail(email) || phone_number === '' || address === '';
    }

    return (
        <KeyboardAvoidingView behavior={Constants.platform === 'ios' ? 'padding' : undefined} style={{ flex: 1, backgroundColor: Colors.lightBg }}>
            <Header
                title='Contact Us' />
            {/* <View>
            </View> */}
            <View style={styles.subContainer}>
                <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
                    <View style={styles.otherInfoBox}>

                        <Text style={styles.labelDark}>You are unique – So are your Questions.</Text>


                        <Text style={styles.labelDescription}>At vero eos et accusamus et iusto odio dignissim ducimus qui blanditiis praesentium voluptatum deleniti.</Text>


                        <Text style={styles.phoneNumber}>+971 111 222 333</Text>
                        <Text style={styles.otherInfoText}>Call or text us at the number above</Text>

                    </View>
                    <View style={{ paddingHorizontal: wp(5), backgroundColor: Colors.white }}>
                        <View style={styles.spacer} />

                        <Input
                            value={first_name}
                            returnKeyType='next'
                            onChangeText={(first_name) => updateState({ first_name })}
                            placeholder='Full Name' />

                        <View style={styles.spacer} />

                        <Input
                            value={email}
                            onChangeText={(email) => updateState({ email })}
                            placeholder='Email'
                            autoCapitalize='none'
                            autoCorrect={false}
                            returnKeyType='next'
                            keyboardType='email-address'
                        />

                        <View style={styles.spacer} />

                        <Input
                            value={phone_number}
                            isPhone
                            call_code={call_code}
                            selectCallCode={(call_code) => updateState({ call_code })}
                            onChangeText={(phone_number) => updateState({ phone_number })}
                            placeholder='Phone Number'
                            returnKeyType='done'
                            keyboardType='phone-pad'

                        />
                        <View style={styles.spacer} />
                        <Input
                            value={address}
                            multiline
                            onChangeText={(address) => updateState({ address })}
                            placeholder='Your Message'
                            returnKeyType='done'
                        />
                        <View style={styles.spacer} />
                        <View style={styles.spacer} />
                        <Button
                            // isDisabled={isDisabled()}
                            onPress={() => { }}
                            text='Submit' />
                        <View style={styles.spacer} />
                        <View style={styles.spacer} />
                    </View>
                </ScrollView>
            </View>
        </KeyboardAvoidingView>
    );
};

export default ContactUs;

const styles = StyleSheet.create({
    cameraButton: {
        borderRadius: 50,
        backgroundColor: Colors.primary,
        position: 'absolute',
        right: -5,
        bottom: -5,
        zIndex: 99
    },
    cameraButtonImage: {
        width: wp(8),
        height: wp(8),
        resizeMode: 'contain'
    },
    spacer: {
        height: hp(3)
    },
    headerTitle: {
        color: Colors.black
    },
    subContainer: {
        flex: 1,
        backgroundColor: Colors.greyBg,
    },
    uploadCoverButton: {
        position: 'absolute',
        right: 0,
        bottom: 0,
        flexDirection: 'row',
        alignItems: 'center',
        borderTopLeftRadius: 20,
        backgroundColor: Colors.white,
        paddingVertical: hp(1),
        paddingHorizontal: wp(4)
    },
    uploadCoverButtonIcon: {
        width: wp(4),
        height: wp(4),
        resizeMode: 'contain'
    },
    uploadCoverButtonText: {
        fontFamily: Constants.popins_regular,
        color: Colors.primary,
        paddingLeft: wp(3)
    },
    otherInfoBox: {
        paddingVertical: hp(3.5),
        paddingHorizontal: wp(4),
        backgroundColor: Colors.lightBg
    },
    labelDark: {
        fontFamily: Constants.popinsMed,
        color: Colors.primary,
        textAlign: 'center',
        fontSize: wp(4)
    },
    labelDescription: {
        fontFamily: Constants.popins_regular,
        color: Colors.textGrey,
        fontSize: wp(3),
        paddingHorizontal: wp(10),
        textAlign: 'center',
        paddingVertical: hp(1.8)
    },
    phoneNumber: {
        fontFamily: Constants.popins,
        color: Colors.tabSelected,
        fontSize: wp(8),
        textAlign: 'center'
    },
    otherInfoText: {
        fontFamily: Constants.popins_regular,
        color: Colors.textGrey,
        textAlign: 'center',
        fontSize: wp(2.8)
    }
});
