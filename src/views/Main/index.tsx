
import React, { useEffect, useState } from 'react';
import {
    Image,
    View,
    StyleSheet,
    KeyboardAvoidingView,
    Pressable,
    FlatList,
    Modal
} from 'react-native';
import { Appbar, Text } from 'react-native-paper';
import { Colors, Constants, Utils } from 'common';
import notifee, { EventType } from '@notifee/react-native';


import { getNearbyUsers } from 'reduxStore/services/users';
import UserItem from './UserItem';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import MainMap from 'views/MainMap';
import { RouteProp, useNavigation, useRoute } from '@react-navigation/native';
import Geolocation, { GeolocationResponse } from '@react-native-community/geolocation';
import { UsersItemModel } from 'reduxStore/models/users';
import { ErrorView, Spinner } from 'components';
import messaging from '@react-native-firebase/messaging';
import { useDispatch } from 'react-redux';
import { updateProfile } from 'reduxStore/services/user';
import { openSettings } from 'react-native-permissions';
import EventDetailView from 'views/MessageDetail/EventDetailView';
import _ from 'lodash';
import { InvitationItemModel } from 'reduxStore/models/invitation';
import { getInvitationById } from 'reduxStore/services/invitations';
import WriteReviewCard from 'views/Reviews/WriteReviewCard';

let initial = {
    title: '',
    description: '',
    isLoading: false,
    isFetching: true,
    showMap: false,
    showDetailView: false,
    showWriteReview: false,
    invitationDetails: {} as InvitationItemModel,
    users: [] as Array<UsersItemModel>,
    invitationDetailId: null,
    reviewUserId: null,
}
interface MainProps {
    route: RouteProp<{ params: { userLocation: string, userPosition: GeolocationResponse['coords'] } }, 'params'>
}

const Main = (props: MainProps) => {
    let navigation = useNavigation();
    let address = props?.route?.params?.userLocation || '';
    const [{
        users,
        showMap,
        isFetching,
        isLoading,
        invitationDetails,
        invitationDetailId,
        reviewUserId,
        showDetailView,
        showWriteReview
    }, setState] = useState(initial);

    const updateState = (state: {}) => {
        setState(prevState => ({ ...prevState, ...state }));
    }

    const askLocationPermission = () => {
        return new Promise(async (resolve, reject) => {
            if (Constants.platform === 'android') {
                // await PermissionsAndroid.request('android.permission.ACCESS_FINE_LOCATION')
                Utils.checkPermissionForUsage('location')
                    .then(res => {
                        Geolocation.getCurrentPosition(
                            (position) => {
                                updateState({ userPosition: position.coords });
                                return resolve(position.coords);
                            },
                            (error) => {
                                // console.log('error = = = >', error);
                                Utils.showToast('Please check & enable your GPS to continue using the application', '', (status) => {
                                    openSettings()
                                        .then(() => console.warn('open settings'))
                                        .catch(() => console.warn('cannot open settings'));
                                });
                                return reject(error);
                            },
                            { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 }
                        );
                    }).catch(err => {
                        Utils.showToast('Please check & enable your GPS to continue using the application', '', (status) => {
                        });
                        openSettings()
                            .then(() => console.warn('open settings'))
                            .catch(() => console.warn('cannot open settings'));
                        return reject('error')
                    })
            } else {
                await Geolocation.requestAuthorization();
                Geolocation.getCurrentPosition(
                    (position) => {
                        updateState({ userPosition: position.coords });
                        return resolve(position.coords);
                    },
                    (error) => {
                        console.log('error = = = >', error);
                        Utils.showToast('Please check & enable your GPS to continue using the application', '', (status) => {
                            openSettings()
                                .then(() => console.warn('open settings'))
                                .catch(() => console.warn('cannot open settings'));
                        });
                        return reject(error);
                    },
                    { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 }
                );
            }
        });
    };

    const _discoverUsers = () => {
        askLocationPermission().then((res) => {
            let position = res as GeolocationResponse['coords'];
            updateProfile({ Lat: position.latitude, Lng: position.longitude })
            getNearbyUsers({ Lat: position.latitude, Lng: position.longitude })
                .then(res => {
                    if (res.Status === 200 && res.Data.Users) {
                        updateState({
                            users: res.Data.Users,
                            isLoading: false,
                            isFetching: false
                        })
                    } else {
                        updateState({
                            isLoading: false,
                            isFetching: false
                        })
                    }
                })
                .catch(err => {
                    updateState({
                        isLoading: false,
                        isFetching: false
                    })
                })
        }).catch(err => {

        })
    }
    let dispatch = useDispatch();

    const getNotificationPermission = () => {
        return new Promise(async (resolve, reject) => {
            const authStatus = await messaging().requestPermission();
            const enabled =
                authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
                authStatus === messaging.AuthorizationStatus.PROVISIONAL;

            if (enabled) {
                var token = await messaging().getToken();
                // console.log('token = = = > ', token);
                // dispatch(updateUserToken(token));
                updateProfile({ FCMToken: token })
                return resolve(token);
            } else {
                return reject('error')
            }
        });
    };

    const route = useRoute();

    const onInvitationReceive = (InvitationId: number) => {
        updateState({ isLoading: true })
        getInvitationById(InvitationId).then(res => {
            if (res.Status === 200 && !_.isEmpty(res.Data)) {
                let _data = res.Data;
                console.log('_data', _data);
                updateState({ isLoading: false, showDetailView: true, invitationDetails: _data });
            }
        }).catch(err => {
            updateState({ isLoading: false });
        })
    }

    useEffect(() => {
        _discoverUsers();
        // Check whether an initial notification is available
        messaging()
            .getInitialNotification()
            .then(remoteMessage => {
                if (remoteMessage) {
                    if (remoteMessage.data.Type === 2 || remoteMessage.data.Type === "2") {
                        navigation.navigate('messageDetail', { eventId: remoteMessage.data.InvitationId })
                    } else if (remoteMessage.data.Type === 3 || remoteMessage.data.Type === "3") {

                    } else {
                        onInvitationReceive(remoteMessage.data.InvitationId)
                    }
                }
            });

        getNotificationPermission().then(res => {

        });
        const unsubscribe = messaging().onMessage(async remoteMessage => {
            console.log('route = = == > ', route);

            if (Constants.platform === 'android') {
                // Create a channel
                const channelId = await notifee.createChannel({
                    id: 'default',
                    name: 'Default Channel',
                });

                // Display a notification
                await notifee.displayNotification({
                    title: remoteMessage.notification.title,
                    body: remoteMessage.notification.body,
                    data: remoteMessage.data,
                    android: {
                        channelId,

                    },
                });
            }
            /* navigation.navigate('main', { screen: "notifications" })
            if (remoteMessage.data && remoteMessage.data.Type === 2) {
                navigation.navigate('messageDetail', { eventId: remoteMessage.data.InvitationId })
            } */
            // console.log('A new FCM message arrived!', remoteMessage.data);
        });

        const unsubscribeNotifee = notifee.onForegroundEvent(({ type, detail }) => {
            switch (type) {
                case EventType.DISMISSED:
                    console.log('User dismissed notification', detail.notification);
                    break;
                case EventType.PRESS:
                    if (detail.notification.data.Type === 2 || detail.notification.data.Type === "2") {
                        navigation.navigate('messageDetail', { eventId: detail.notification.data.InvitationId })
                    } else if (detail.notification.data.Type === 3 || detail.notification.data.Type === "3") {

                    } else {
                        onInvitationReceive(detail.notification.data.InvitationId)
                    }
                    break;
            }
        });

        return (() => {
            unsubscribe();
            unsubscribeNotifee();
        })
    }, [])


    return <KeyboardAvoidingView behavior={Constants.platform == 'ios' ? 'padding' : undefined} style={{ flex: 1, backgroundColor: 'white' }}>
        <Appbar.Header style={{ height: 90, elevation: 0, justifyContent: 'space-between', backgroundColor: Colors.white }}>

            <View style={{ flex: 2, paddingHorizontal: 10 }}
            >
                <Text style={{ fontFamily: Constants.NiveauGroteskBold, fontWeight: '700', color: '#0E134F', fontSize: wp(5) }}>Discover Users</Text>
                <Pressable onPress={() => navigation.navigate('searchView', { userPosition: props.route.params.userPosition })}><View style={styles.locationDetailView}>
                    <Image style={styles.locationIcon} source={Constants.location} />
                    <Text style={styles.locationTextDetail} numberOfLines={1}>{address}</Text>
                </View>
                </Pressable>
            </View>
            <View style={{ flex: .5 }} />
            <Pressable onPress={() => updateState({ showMap: !showMap })} style={styles.locationView}>
                <Image style={styles.mapIcon} source={showMap ? Constants.map_view : Constants.list_view} />
                <Text style={styles.locationLabel}>{showMap ? 'Map View' : 'List View'}</Text>
            </Pressable>
        </Appbar.Header>

        <View style={{ flex: 1 }}>
            {!showMap && users.length !== 0 ? <FlatList
                refreshing={isLoading}
                onRefresh={() => _discoverUsers()}
                data={users}
                ListHeaderComponent={() => <View style={{ height: 20 }} />}
                ListFooterComponent={() => <View style={{ height: hp(3) }} />}
                ItemSeparatorComponent={() => <View style={{ height: hp(3) }} />}
                keyExtractor={(item) => item.Id.toString()}
                renderItem={(props) => <UserItem onPress={() => navigation.navigate('profileView', { user: props.item })} {...props} />}
            /> : !showMap && !isFetching && <ErrorView errorMessage='No Nearby Users Found!' />}
            {showMap && <MainMap
                userPosition={props.route.params.userPosition}
                users={users} />}
        </View>
        {isFetching && <Spinner />}
        <Modal
            transparent
            animationType='slide'
            onRequestClose={() => updateState({ showDetailView: false, invitationDetails: {} })}
            visible={!_.isEmpty(invitationDetails) && showDetailView}
        >
            <EventDetailView
                showActionButtons={invitationDetails.Status === 'pending'}
                onPress={() => updateState({ showDetailView: false, invitationDetails: {} })}
                invitationDetails={invitationDetails} />
        </Modal>
        <Modal
            transparent
            animationType='slide'
            visible={showWriteReview}
            onRequestClose={() => updateState({ showWriteReview: false })}>
            <WriteReviewCard
                invitationId={invitationDetailId}
                userId={reviewUserId}
                onSuccess={() => Utils.showToast('Review sent successfully!', 'Success')}
                onPress={() => updateState({ showWriteReview: false })}
            />
        </Modal>
    </KeyboardAvoidingView>
}

export default Main;

const styles = StyleSheet.create({
    spacer: {
        height: 12
    },
    locationView: {
        backgroundColor: '#FCEDE4',//Colors.primary,
        paddingHorizontal: 12,
        paddingRight: 20,
        borderTopLeftRadius: 25,
        borderBottomLeftRadius: 25,
        height: 40,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    locationDetailView: {
        paddingVertical: 7,
        flexDirection: 'row',
        alignItems: 'center',
    },
    locationTextDetail: {
        fontFamily: Constants.NiveauGroteskLight,
        color: '#0E134F',
        fontSize: 13
    },
    locationIcon: {
        width: 20,
        height: wp(4),
        marginRight: wp(1),
        resizeMode: 'contain'
    },
    mapIcon: {
        width: wp(4),
        marginHorizontal: wp(2),
        height: '80%',
        resizeMode: 'contain'
    },
    locationLabel: {
        color: '#42526E',
        fontSize: wp(3),
        fontFamily: Constants.NiveauGroteskBold
    },
    errorText: {
        color: '#ff5252',
        paddingTop: 5,
        fontSize: 12
    },
    textInput: {
        marginTop: 10,
        height: 35,
        backgroundColor: '#eee',
        borderRadius: 5,
        paddingHorizontal: 10,
        color: Colors.black
    },
    add_photoIcon: {
        width: 100,
        height: 100,
        resizeMode: 'contain',
    }
});
