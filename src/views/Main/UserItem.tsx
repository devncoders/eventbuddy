import { Colors, Constants } from 'common';
import _ from 'lodash';
import * as React from 'react';
import { Text, View, StyleSheet, Image, Pressable } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { UsersItemModel } from 'reduxStore/models/users';
interface UserItemProps {
    item: UsersItemModel,
    index: number,
    onPress: () => void
}

const UserItem = (props: UserItemProps) => {
    let image = Constants.logo;
    if (!_.isEmpty(props.item.Media)) {
        image = {
            uri: props.item.Media.BasePath + props.item.Media.ThumbPath
        }
    }
    return (
        <Pressable onPress={props.onPress} style={styles.container}>
            <Image style={styles.image} source={image} />
            <View style={{ flex: 1, paddingHorizontal: wp(3) }}>
                <Text style={styles.reviewCount}>{props.item?.ReviewCount || 0} Reviews</Text>
                <Text numberOfLines={1} style={styles.userTitle}>{props.item.FullName}</Text>
                <Text numberOfLines={3} style={styles.userDescription}>{props.item.About}</Text>
            </View>
        </Pressable>
    );
};

export default UserItem;

const styles = StyleSheet.create({
    image: {
        width: wp(25),
        height: wp(25),
        resizeMode: 'cover',
        borderRadius: 10
    },
    container: {
        backgroundColor: Colors.white,
        flexDirection: 'row',
        padding: wp(3),
        marginHorizontal: wp(3),
        borderRadius: 6,
        ...Constants.boxShadow
    },
    reviewCount: {
        color: Colors.primary,
        fontFamily: Constants.NiveauGroteskMedium,
        fontSize: wp(3)
    },
    userTitle: {
        color: '#0E134F',
        fontFamily: Constants.NiveauGroteskMedium,
        fontSize: wp(4.2),
        paddingVertical: wp(1)
    },
    userDescription: {
        color: '#42526EB2',
        fontFamily: Constants.NiveauGroteskMedium,
        fontSize: wp(3.5)
    },
});
