
import React, { useEffect, useState } from 'react';
import {
    SafeAreaView,
    Pressable,
    View,
    Dimensions,
    StyleSheet,
    Modal
} from 'react-native';
import { Appbar, Avatar, Text, Button, Card, Subheading, TextInput, Title } from 'react-native-paper';
import * as Animated from 'react-native-animatable';
import { Constants, Colors } from 'common';
import { FlatList, ScrollView } from 'react-native-gesture-handler';
import { Header } from 'components';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

let initial = {

    loading: false,
    showLoginView: false
}
const { width } = Dimensions.get('window')
interface PrivacyPolicyProps {
    route?: {
        params?: {
            title?: string
        }
    }
}
const PrivacyPolicy = (props: PrivacyPolicyProps) => {

    const [{

    }, setState] = useState(initial);

    const updateState = (state: {}) => {
        setState(prevState => ({ ...prevState, ...state }));
    }

    useEffect(() => {

    }, [])


    return <View style={{ flex: 1, backgroundColor: Colors.white }}>
        <Header title={'Privacy Policy'} />
        <View style={{ flex: 1 }}>
            <ScrollView style={{ paddingVertical: hp(3), paddingHorizontal: wp(6) }}>
                <Text style={styles.title}>Terms of Use</Text>
                <Text style={styles.text}>The purpose of lorem ipsum is to create a natural looking block of text (sentence, paragraph, page, etc.) that doesn't distract from the layout. A practice not without controversy, laying out pages with meaningless filler text can be very useful when the focus is meant to be on design, not content. {'\n'}{'\n'}
                    The passage experienced a surge in popularity during the 1960s when Letraset used it on their dry-transfer sheets, and again during the 90s as desktop publishers bundled the text with their software. Today it's seen all around the web; on templates, websites, and stock designs. Use our generator to get your own, or read on for the authoritative history of lorem ipsum. software. {'\n'}{'\n'}
                    Today it's seen all around the web; on templates, websites, and stock designs. Use our generator to get your own, or read on for the authoritative history of lorem ipsum.</Text>
                <Text style={styles.title}>Request Accepting/Rejecting</Text>
                <Text style={styles.text}>The purpose of lorem ipsum is to create a natural looking block of text (sentence, paragraph, page, etc.) that doesn't distract from the layout. A practice not without controversy, laying out pages with meaningless filler text can be very useful when the focus is meant to be on design, not content. {'\n'}{'\n'}
                    The passage experienced a surge in popularity during the 1960s when Letraset used it on their dry-transfer sheets, and again during the 90s as desktop publishers bundled the text with their software. Today it's seen all around the web; on templates, websites, and stock designs. Use our generator to get your own, or read on for the authoritative history of lorem ipsum. software. {'\n'}{'\n'}
                    Today it's seen all around the web; on templates, websites, and stock designs. Use our generator to get your own, or read on for the authoritative history of lorem ipsum.</Text>
            </ScrollView>
        </View>
    </View>
}

export default PrivacyPolicy;

const styles = StyleSheet.create({
    title: {
        paddingBottom: wp(4),
        fontWeight: '600',
        color: Colors.primary,
        fontFamily: Constants.NiveauGroteskBold,
        fontSize: wp(4.6)
    },
    text: {
        paddingBottom: hp(3),
        fontSize: wp(2.7),
        lineHeight: 24,
        color: Colors.textGreyDark,
        fontFamily: Constants.popins_regular
    },

});
