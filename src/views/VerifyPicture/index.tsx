
import React, { useEffect, useState } from 'react';
import {
    Text,
    View,
    StyleSheet,
    Image
} from 'react-native';


import { Constants, Colors, Utils } from 'common';
import { Button, Header, Spinner } from 'components';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import *  as ImagePicker from 'react-native-image-picker';
import { updateProfile, uploadMedia } from 'reduxStore/services/user';
import { MediaType, PhotoQuality } from 'react-native-image-picker';
import { RouteProp, useNavigation } from '@react-navigation/native';
import _ from 'lodash'

let initial = {
    fileName: '',
    imageType: '',
    pictureNew: '',
    isLoading: false,
    showLoginView: false,
    SelfieMediaId: null
}
let cameraOptions = {
    mediaType: 'photo' as MediaType,
    quality: __DEV__ ? 0.02 : 0.7 as PhotoQuality,
}
interface VerifyPictureProps {
    route: RouteProp<{
        params: {
            picture: '',
            fileName: '',
            imageType: '',
            mediaId: ''
        }
    }, 'params'>
}
const VerifyPicture = (props: VerifyPictureProps) => {
    let picture = props?.route?.params?.picture;
    let navigation = useNavigation();
    const [{
        isLoading,
        fileName,
        imageType,
        pictureNew,
        SelfieMediaId
    }, setState] = useState(initial);

    const updateState = (state: {}) => {
        setState(prevState => ({ ...prevState, ...state }));
    }

    useEffect(() => {
        let fileName = props?.route?.params?.fileName || '';
        let imageType = props?.route?.params?.imageType || '';
        updateState({
            fileName,
            imageType
        })
    }, [])
    useEffect(() => {
        if (SelfieMediaId) {
            onSuccessPress()
        }
    }, [SelfieMediaId]);

    const goToNext = () => {
        navigation.reset({
            index: 0,
            routes: [
                { name: 'locationView' }
            ]
        })
    }

    const onSuccessPress = () => {
        updateProfile({ SelfieMediaId, MediaId: props.route.params.mediaId }).then(res => {
            updateState({ isLoading: false });
            goToNext();
        }).catch(err => {
            updateState({ isLoading: false });
            Utils.showToast(err.Message);
        });
    }



    const onVerify = () => {
        Utils.confirmAlert('CONFIRM!', 'are you sure you want to submit picture for verification?', (status) => {
            if (status === 'success') {
                updateState({ isLoading: true });
                uploadPicture().then(() => {
                }).catch(() => {
                    updateState({ isLoading: false });
                });
            }
        })
    }

    const uploadPicture = () => {
        return new Promise((resolve, reject) => {
            let body = new FormData();
            body.append('file',
                {
                    uri: pictureNew,
                    name: fileName,
                    type: imageType
                });
            uploadMedia(body).then(res => {
                if (res.Status === 200 && !_.isEmpty(res.Data)) {
                    updateState({ imageUploaded: true, SelfieMediaId: res.Data.Id });
                    return resolve('success')
                } else {
                    return reject('error')
                }
            }).catch(() => {
                return reject('error')
            })
        })
    }

    const onImageSuccess = (response: ImagePicker.ImagePickerResponse) => {
        let assets = response.assets || [];
        let uri = assets[0] && assets[0].uri || '';
        let imageType = assets[0] && assets[0].type || '';
        let fileName = assets[0] && assets[0].fileName || '';
        const realPath = Constants.platform === 'ios' ? uri.replace('file://', '') : uri;
        Utils.getResolvedPath(realPath).then(pathRes => {
            var str1 = "file://";
            var str2 = pathRes.path;
            var correctpath = str1.concat(str2);
            updateState({
                imageType,
                fileName,
                pictureNew: correctpath
            });
        }).catch(err => {
            console.log('=====> ', err)
        })
    }

    const openCamera = () => {
        if (__DEV__) {
            ImagePicker.launchImageLibrary(
                cameraOptions,
                async (response: ImagePicker.ImagePickerResponse) => {
                    if (!response.errorCode && !response.didCancel) {
                        onImageSuccess(response)
                    } else {
                        updateState({ isLoading: false });
                    }
                },
            );
            return
        }
        Utils.checkPermissionForUsage('camera')
            .then(res => {
                ImagePicker.launchCamera(
                    cameraOptions,
                    async response => {
                        onImageSuccess(response);
                    },
                );
            })
            .catch(error => {
                Utils.showToast('Camera Permssion required');
                updateState({ isLoading: false });
            })
    }

    return <View style={{ flex: 1, backgroundColor: Colors.white }}>
        <Header title={'Verify Picture'} />
        <View style={{ flex: 1, paddingHorizontal: wp(5) }}>
            <View style={{ flex: 1 }}>
                <Text style={styles.title}>Uploaded Picture</Text>
                <View style={styles.imageContainer}>
                    <Image style={styles.imageCaptured} source={{ uri: picture }} />
                </View>
                <Text style={styles.title}>Recent Captured Selfie</Text>
                <View style={[styles.imageContainer, pictureNew === '' && { borderWidth: 1, borderStyle: 'dashed', borderColor: Colors.primary }]}>
                    <Image style={styles.imageCaptured} source={{ uri: pictureNew }} />
                </View>
            </View>
            <Text style={[styles.description]}>Please make sure to be in a spot with sufficient
                light on your face.</Text>
        </View>
        <View style={{ paddingVertical: hp(2), paddingHorizontal: wp(5) }}>
            <Button
                emptyButton
                onPress={() => openCamera()} text='Capture Selfie Again' />
            <View style={styles.spacer} />
            <Button
                disabled={pictureNew === ''}
                onPress={() => onVerify()} text='Verify' />
        </View>
        {isLoading && <Spinner text='Please wait.....' />}
    </View>
}

export default VerifyPicture;

const styles = StyleSheet.create({
    spacer: {
        height: hp(1)
    },
    editButton: {
        position: 'absolute',
        width: wp(8),
        height: wp(8),
        borderRadius: wp(8) / 2,
        borderColor: Colors.borderColor,
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.white,
        right: 20,
        bottom: 10
    },
    cameraIconContainer: {
        width: wp(50),
        height: wp(50),
        backgroundColor: '#F9F9FA',
        borderRadius: wp(50) / 2,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: Colors.borderColor
    },
    categoryItem: {
        elevation: 4,
        borderRadius: 8,
        padding: 12,
        margin: 10,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: wp(3.5),
        color: '#000',
        fontFamily: Constants.NiveauGroteskRegular,
        fontWeight: '500',
        paddingVertical: hp(2.5)
    },
    description: {
        fontSize: wp(3.2),
        color: '#42526E',
        fontWeight: '400',
        fontFamily: Constants.NiveauGroteskRegular,
        paddingVertical: hp(2.2),
        textAlign: 'center',
        paddingHorizontal: wp(6)
    },
    imageContainer: {
        flex: 1,
        width: '100%',
        borderRadius: 10,
        overflow: 'hidden'
    },
    imageCaptured: {
        resizeMode: 'cover',
        width: '100%',
        height: '100%',

    }
});
