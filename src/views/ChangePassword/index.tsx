import React, { Component, useRef, useState } from "react";

import { Alert, View, Dimensions, Text, StyleSheet, Linking, Pressable, Image, ScrollView, KeyboardAvoidingView } from "react-native";

import * as Animatable from "react-native-animatable";
import { AuthInput, Button, Header, Spinner } from "components";
import { Colors, Constants, Utils } from "common";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import Icon from "react-native-vector-icons/FontAwesome5";
import { Appbar, Surface, Switch } from "react-native-paper";
import { useNavigation } from "@react-navigation/native";
import { updateProfile } from "reduxStore/services/user";
import ErrorHandler from "common/errorHandler";
import _ from "lodash";

console.disableYellowBox = true;

let validURL = (str: string) => {
    var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
        '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return !!pattern.test(str);
}

interface ChangePasswordProps {

}
let initialState = {
    old_password: '',
    password: '',
    confirm_password: '',
    isLoading: false,
}
const ChangePassword = (props: ChangePasswordProps) => {
    let navigation = useNavigation();
    const [
        {
            old_password,
            password,
            confirm_password,
            isLoading, }, setState
    ] = useState(initialState);
    const updateState = (newState = {}) => {
        setState(prevState => ({ ...prevState, ...newState }));
    }

    const isDisabled = () => {
        return old_password === '' || password === '' ||
            password !== confirm_password
    }

    const onSubmit = () => {
        if (isLoading) return;
        updateState({ isLoading: true })
        let data = {
            Password: password,
            OldPassword: old_password
        }
        updateProfile(data)
            .then(res => {
                updateState({ isLoading: false })
                if (res.Status === 200 && !_.isEmpty(res.Data)) {
                    Utils.showToast('Password updated successfully!', 'Success');
                    navigation.goBack();
                } else {
                    Utils.showToast(res.Message);
                }
            })
            .catch(err => {
                let msg = ErrorHandler(err)
                Utils.showToast(msg);
                updateState({ isLoading: false })
            });
    }

    return (
        <KeyboardAvoidingView behavior={Constants.platform === 'ios' ? 'padding' : undefined} style={{ flex: 1, backgroundColor: Colors.white }}>
            <Header title='Change Password' />
            <View style={{ flex: 1 }}>
                <ScrollView>
                    <View style={styles.spacer} />
                    <Text style={styles.buttonTitle}>Old Password</Text>
                    <View style={styles.spacer} />
                    <AuthInput
                        style={{ marginHorizontal: wp(8) }}
                        isPassword
                        value={old_password}
                        onChangeText={(old_password) => updateState({ old_password })}
                        placeholder={'Old Password'}
                    />
                    <View style={styles.spacer} />
                    <View style={styles.spacer} />
                    <Text style={styles.buttonTitle}>New Password</Text>
                    <View style={styles.spacer} />
                    <AuthInput
                        style={{ marginHorizontal: wp(8) }}
                        isPassword
                        value={password}
                        onChangeText={(password) => updateState({ password })}
                        placeholder={'New Password'}
                    />
                    <View style={styles.spacer} />
                    <View style={styles.spacer} />
                    <Text style={styles.buttonTitle}>Re-type New Password</Text>
                    <View style={styles.spacer} />
                    <AuthInput
                        style={{ marginHorizontal: wp(8) }}
                        isPassword
                        value={confirm_password}
                        onChangeText={(confirm_password) => updateState({ confirm_password })}
                        placeholder={'Re-type New Password'}
                    />
                    {password !== confirm_password && <Text style={styles.errorText}>Confirm Password Does not match</Text>}
                    <View style={styles.spacer} />
                    <View style={styles.spacer} />
                    <View style={{ marginHorizontal: wp(8) }}><Button
                        disabled={isDisabled()}
                        onPress={() => onSubmit()}
                        text="Save"
                    />
                    </View>
                    <View style={styles.spacer} />
                    <View style={styles.spacer} />
                </ScrollView>
            </View>
            {isLoading && <Spinner />}
        </KeyboardAvoidingView>
    );

}

const styles = StyleSheet.create({
    errorText: {
        paddingVertical: 8,
        fontFamily: Constants.NiveauGroteskRegular,
        fontSize: wp(2.8),
        color: Colors.red,
        marginHorizontal: wp(8)
    },
    spacer: {
        height: hp(2)
    },
    locationView: {
        backgroundColor: '#E4FAE2',//Colors.primary,
        width: wp(25),
        borderTopLeftRadius: 25,
        borderBottomLeftRadius: 25,
        height: 40,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    locationIcon: {
        marginHorizontal: wp(2),
    },
    locationLabel: {
        color: '#34AC29',
        fontSize: wp(3),
        fontFamily: Constants.NiveauGroteskRegular
    },
    settingButton: {
        flexDirection: 'row',
        height: hp(7),
        alignItems: 'center',
        borderRadius: 5,
        marginHorizontal: wp(4),
        paddingHorizontal: wp(5),
        ...Constants.boxShadow,
        backgroundColor: Colors.white
    },
    buttonIcon: {
        width: wp(5),
        height: wp(5),
        resizeMode: 'contain'
    },
    buttonTitle: {
        marginHorizontal: wp(8),
        color: '#0E134F',
        fontFamily: Constants.NiveauGroteskRegular,
        fontSize: wp(3.8),
    },
    profilePictureContainer: {
        alignSelf: 'center',
        marginVertical: hp(4),
        backgroundColor: Colors.primaryLight,
        width: wp(40),
        height: wp(40),
        borderRadius: wp(40) / 2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    editButton: {
        position: 'absolute',
        width: wp(8),
        height: wp(8),
        borderRadius: wp(8) / 2,
        borderColor: Colors.primary,
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.white,
        right: 10,
        bottom: 10
    },
});

export default ChangePassword;