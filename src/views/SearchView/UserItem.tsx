import { Colors, Constants } from 'common';
import * as React from 'react';
import { Text, View, StyleSheet, Image, Pressable } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

interface UserItemProps {
    item: {
        image?: any
    },
    index?: number,
    onPress?: () => void
}

const UserItem = (props: UserItemProps) => {
    return (
        <Pressable onPress={props.onPress} style={styles.container}>
            <Image style={styles.image} source={props?.item?.image || Constants.user} />
            <View style={{ flex: 1, paddingHorizontal: wp(3) }}>
                <Text style={styles.reviewCount}>12 Reviews</Text>
                <Text numberOfLines={1} style={styles.userTitle}>Andrew Wilson</Text>
                <Text numberOfLines={3} style={styles.userDescription}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</Text>
            </View>
        </Pressable>
    );
};

export default UserItem;

const styles = StyleSheet.create({
    image: {
        width: 100,
        height: 100,
        resizeMode: 'cover',
        borderRadius: 10
    },
    container: {
        width: wp(90),
        backgroundColor: Colors.white,
        flexDirection: 'row',
        paddingHorizontal: wp(3),
        alignItems: 'center',
        height: 130,
        marginVertical: 10,
        borderRadius: 6,
        ...Constants.boxShadow
    },
    reviewCount: {
        color: Colors.primary,
        fontFamily: Constants.NiveauGroteskMedium,
        fontSize: wp(3)
    },
    userTitle: {
        color: '#0E134F',
        fontFamily: Constants.NiveauGroteskMedium,
        fontSize: wp(4.2),
        paddingVertical: wp(1)
    },
    userDescription: {
        color: '#42526EB2',
        fontFamily: Constants.NiveauGroteskMedium,
        fontSize: wp(3.5)
    },
});
