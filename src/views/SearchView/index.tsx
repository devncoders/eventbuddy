import Geolocation, { GeolocationResponse } from '@react-native-community/geolocation';
import { RouteProp, useNavigation } from '@react-navigation/native';
import { Colors, Constants, Utils } from 'common';
import { Input, Spinner } from 'components';
import * as React from 'react';
import { Text, View, StyleSheet, Image, FlatList, Pressable, KeyboardAvoidingView, Keyboard } from 'react-native';
import MapView, { Marker, Region } from 'react-native-maps';
import { Appbar, Surface } from 'react-native-paper';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Geocoder from '@timwangdev/react-native-geocoder';
import { useDispatch } from 'react-redux';
import { updateUserLatlng } from 'reduxStore/actions/auth';
interface SearchViewProps {
    route: RouteProp<{ params: { userPosition: GeolocationResponse['coords'] } }, 'params'>
}
let data = [
    { image: Constants.user, latlng: { latitude: 30.330, longitude: 30.330 } },
    { image: Constants.user_1, latlng: { latitude: 30.740, longitude: 30.340 } },
    { image: Constants.user_2, latlng: { latitude: 30.450, longitude: 30.350 } },
    { image: Constants.user_3, latlng: { latitude: 30.360, longitude: 30.460 } },
    { image: Constants.user, latlng: { latitude: 30.370, longitude: 30.570 } },
    { image: Constants.user_2, latlng: { latitude: 30.290, longitude: 30.390 } },
];
const SearchView = (props: SearchViewProps) => {

    let navigation = useNavigation();
    let [showUsers, setUserState] = React.useState(true);
    let [mapState, setMapState] = React.useState(true);
    let [isLoading, setLoading] = React.useState(false);
    let mapViewRef = React.useRef<MapView>(null);
    const onMapReady = () => {
        mapViewRef.current?.fitToCoordinates([
            {
                latitude: props.route.params.userPosition.latitude,
                longitude: props.route.params.userPosition.longitude,
            }
        ], { edgePadding: { top: 50, left: 20, bottom: 200, right: 20 } })
        /* let coordniates: any = data.map(i => i.latlng);
        */
    }

    React.useEffect(() => {
        setLoading(false);
        const keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            () => {
                setMapState(false); // or some other action
            }
        );
        const keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            () => {
                setMapState(true);
            }
        );
        return () => {
            keyboardDidHideListener.remove();
            keyboardDidShowListener.remove();
        };
    }, []);
    React.useEffect(() => {
        if (mapViewRef) {
            mapViewRef.current?.fitToCoordinates([{
                latitude: props.route.params.userPosition.latitude,
                longitude: props.route.params.userPosition.longitude
            }], { edgePadding: { top: 20, left: 20, bottom: 20, right: 20 } })
        }
    }, [mapViewRef, props.route.params.userPosition]);

    const dispatch = useDispatch();

    const getLocationAddress = async (position: GeolocationResponse['coords']) => {
        try {
            const latLng = { lat: position.latitude, lng: position.longitude };
            let address = await Geocoder.geocodePosition(latLng);
            setLoading(false);
            dispatch(updateUserLatlng({ latitude: position.latitude, longitude: position.longitude }))
            let formattedAddress = address[0].formattedAddress || '';
            navigation.reset({
                index: 0,
                routes: [
                    {
                        name: 'main',
                        state: {
                            routes: [
                                {
                                    name: 'home', params: { userLocation: formattedAddress, userPosition: position }
                                }
                            ]
                        }

                    }
                ]
            })

        } catch (err) {
            console.log(err);
            Utils.showToast('Please check & enable your GPS to continue using the application');
            setLoading(false);
        }
    }

    const onShowMap = () => {
        setLoading(true);
        Geolocation.getCurrentPosition(
            (position) => {
                getLocationAddress(position.coords)
            },
            (error) => {
                Utils.showToast('Please check & enable your GPS to continue using the application');
                setLoading(false);
            },
            { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 }
        );
    }

    const renderMarker = () => {
        return <Marker
            coordinate={{
                latitude: props.route.params.userPosition.latitude,
                longitude: props.route.params.userPosition.longitude
            }}
            pinColor={Colors.primary}
        />
    }

    React.useEffect(() => {
        if (showUsers) {
            onMapReady();
        }
    }, [showUsers])

    return (
        <>
            <KeyboardAvoidingView behavior={Constants.platform === 'ios' ? 'padding' : undefined} style={styles.container}>
                {mapState && <MapView
                    onMapReady={onMapReady}
                    ref={mapViewRef}
                    style={{ flex: 1 }}>
                    {renderMarker()}
                </MapView>}
                {<Surface style={[styles.searchContainer, mapState && { flex: 1 }]}>
                    <View style={{ justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}>
                        <Text style={styles.title}>Your Location</Text>
                        <Pressable onPress={() => onShowMap()}><Text style={styles.skipButtonText}>Save</Text></Pressable>
                    </View>
                    <View style={{ marginVertical: 20 }}>
                        {/* <Pressable onPress={() => navigation.navigate('locationSearchView', { viewName: 'searchView' })}> */}
                        <Input
                            // isButton
                            blurOnSubmit
                            placeholder='Search Location'
                            leftIcon={Constants.search}
                            returnKeyType='search'
                        />
                        {/* </Pressable> */}
                    </View>
                    <Pressable onPress={() => onShowMap()} style={styles.currentLocationButton}>
                        <Image style={styles.gpsIcon} source={Constants.gpsLocation} />
                        <Text style={styles.currentLocationButtonText}>Use Your Current Location</Text>
                    </Pressable>
                </Surface>}

            </KeyboardAvoidingView>
            {isLoading && <Spinner />}
        </>
    );
};

export default SearchView;

const styles = StyleSheet.create({
    currentLocationButton: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10
    },
    currentLocationButtonText: {
        fontSize: 14,
        color: '#42526E',
        fontWeight: '400',
        fontFamily: Constants.NiveauGroteskRegular,
    },
    gpsIcon: {
        width: 25,
        height: 25,
        resizeMode: 'contain',
        marginRight: 12
    },
    title: {
        fontSize: 22,
        color: '#0E134F',
        fontFamily: Constants.NiveauGroteskBold,

    },
    skipButtonText: {
        color: Colors.primary,
        fontFamily: Constants.NiveauGroteskMedium,
        fontSize: 15,
        paddingVertical: 5,
        paddingHorizontal: 7
    },
    searchContainer: {
        paddingHorizontal: wp(7),
        paddingVertical: 30,
        borderTopLeftRadius: 25,
        borderTopRightRadius: 25,
        overflow: 'hidden',
        elevation: 4,
        width: '100%',
        position: 'absolute',
        bottom: 0,
        left: 0,
        backgroundColor: Colors.white,
        minHeight: hp(25)
    },
    triangle: {
        alignSelf: 'center',
        width: 0,
        top: -1,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderTopWidth: 7,
        borderBottomWidth: 0,
        borderRightWidth: 15 / 2,
        borderLeftWidth: 15 / 2,
        borderTopColor: '#ccc',
        borderRightColor: 'transparent',
        borderBottomColor: 'transparent',
        borderLeftColor: 'transparent',
    },
    imageContainer: {
        width: 50,
        height: 50,
        borderRadius: 50 / 2,
        borderWidth: 5, borderColor: '#ccc',
    },
    image: {
        width: 40,
        height: 40,
        borderRadius: 40 / 2,
    },
    container: {
        flex: 1
    },
    listContainer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        // backgroundColor: '#fff',//Colors.primary,
    },
    locationDetailView: {
        paddingVertical: 7,
        flexDirection: 'row',
        alignItems: 'center',
    },
    locationTextDetail: {
        fontFamily: Constants.NiveauGroteskLight,
        color: '#0E134F',
        fontSize: 13
    },
    locationIcon: {
        width: 20,
        height: wp(4),
        marginRight: wp(1),
        resizeMode: 'contain'
    },
    mapIcon: {
        width: wp(4),
        marginHorizontal: wp(1),
        height: '80%',
        resizeMode: 'contain'
    },
    locationLabel: {
        color: '#42526E',
        fontSize: wp(3),
        fontFamily: Constants.NiveauGroteskBold
    },
});
