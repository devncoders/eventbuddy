import React, { Component, useEffect, useRef, useState } from "react";

import { Alert, View, Dimensions, Text, StyleSheet, Linking, Pressable, Image, ScrollView, KeyboardAvoidingView } from "react-native";

import * as Animatable from "react-native-animatable";
import { AuthInput, Button, Header, Spinner } from "components";
import { Colors, Constants, Utils } from "common";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import *  as ImagePicker from 'react-native-image-picker';

import { ActivityIndicator, Appbar, Surface, Switch } from "react-native-paper";
import { getProfile, updateProfile, uploadMedia } from "reduxStore/services/user";
import _ from "lodash";
import ErrorHandler from "common/errorHandler";
import { useDispatch } from "react-redux";
import { updateUser } from "reduxStore/actions/auth";
import { UserModel } from "reduxStore/models";
import { MediaType, PhotoQuality } from "react-native-image-picker";

console.disableYellowBox = true;
let cameraOptions = {
    mediaType: 'photo' as MediaType,
    quality: __DEV__ ? 0.1 : 0.7 as PhotoQuality,
}
let validURL = (str: string) => {
    var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
        '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return !!pattern.test(str);
}

interface ProfileSettingsProps {
    navigation: any
}
let initialState = {
    isImageLoading: true,
    isVerified: null,
    profilePicture: null,
    FullName: '',
    AboutUs: '',
    PhoneNumber: '',
    isBack: true,
    userProfile: {} as UserModel,
    isOpen: true,
    isLoading: false
}
const ProfileSettings = (props: ProfileSettingsProps) => {
    const dispatch = useDispatch();
    const [
        {
            isImageLoading,
            isVerified,
            profilePicture,
            FullName,
            AboutUs,
            userProfile,
            PhoneNumber,
            isLoading }, setState
    ] = useState(initialState);
    const updateState = (newState = {}) => {
        setState(prevState => ({ ...prevState, ...newState }));
    }

    useEffect(() => {
        _getProfile();
    }, []);

    const _getProfile = () => {
        if (isLoading) return;
        updateState({ isLoading: true })
        getProfile()
            .then(res => {
                updateState({ isLoading: false })
                if (res.Status === 200 && !_.isEmpty(res.Data)) {
                    console.log('res.Data = = = > ', res.Data);

                    let imageUrl = null;
                    if (!_.isEmpty(res.Data.User.Media)) {
                        imageUrl = { uri: res.Data.User.Media.BasePath + res.Data.User.Media.ThumbPath }
                    }
                    updateState({
                        userProfile: res.Data.User,
                        FullName: res.Data.User.FullName,
                        AboutUs: res.Data.User.About,
                        PhoneNumber: res.Data.User.PhoneNumber,
                        isVerified: res.Data.User.IsVerified,
                        profilePicture: imageUrl
                    });
                    dispatch(updateUser(res.Data.User));
                } else {
                    Utils.showToast(res.Message);
                }
            })
            .catch(err => {
                let msg = ErrorHandler(err)
                Utils.showToast(msg);
                updateState({ isLoading: false })
            });
    }

    const updateProfileData = (data: {}) => {
        updateProfile(data)
            .then(res => {
                updateState({ isLoading: false })
                if (res.Status === 200 && !_.isEmpty(res.Data)) {
                    dispatch(updateUser(res.Data.User));
                    Utils.showToast('Profile Updated Successfully', 'Success');
                } else {
                    Utils.showToast(res.Message);
                }
            })
            .catch(err => {
                let msg = ErrorHandler(err)
                Utils.showToast(msg);
                updateState({ isLoading: false })
            });
    }

    const _updateProfile = () => {
        if (isLoading) return;
        updateState({ isLoading: true })
        let data = {
            FullName,
            PhoneNumber,
            About: AboutUs,
            Email: userProfile.Email
        }
        updateProfileData(data);
    }

    const onImageSuccess = (response: ImagePicker.ImagePickerResponse) => {
        let assets = response.assets || [];
        let uri = assets[0] && assets[0].uri || '';
        let imageType = assets[0] && assets[0].type || '';
        let fileName = assets[0] && assets[0].fileName || '';
        const realPath = Constants.platform === 'ios' ? uri.replace('file://', '') : uri;
        Utils.getResolvedPath(realPath).then(pathRes => {
            var str1 = "file://";
            var str2 = pathRes.path;
            var correctpath = str1.concat(str2);
            updateState({
                isImageLoading: true,
                imageType,
                fileName,
                profilePicture: { uri: correctpath }
            });
            let body = new FormData();
            body.append('file',
                {
                    uri: correctpath,
                    name: fileName,
                    type: imageType
                });
            uploadMedia(body).then(res => {
                updateState({ isLoading: false });
                if (res.Status === 200 && !_.isEmpty(res.Data)) {
                    updateProfileData({ MediaId: res.Data.Id });
                } else {
                    Utils.showToast(res.Message);
                }
            }).catch(err => {
                Utils.showToast(err.Message);
                updateState({ isLoading: false });
            });
        }).catch(err => {
            console.log('=====> ', err)
        })
    }

    const openCamera = () => {
        if (__DEV__) {
            ImagePicker.launchImageLibrary(
                cameraOptions,
                async (response: ImagePicker.ImagePickerResponse) => {
                    if (!response.errorCode && !response.didCancel) {
                        onImageSuccess(response)
                    } else {
                        updateState({ isLoading: false });
                    }
                },
            );
            return
        }
        Utils.checkPermissionForUsage('camera')
            .then(res => {
                // updateState({ isLoading: true });
                ImagePicker.launchCamera(
                    cameraOptions,
                    async response => {
                        onImageSuccess(response);
                    },
                );
            })
            .catch(error => {
                Utils.showToast('Camera Permssion required');
                updateState({ isLoading: false });
            })
    }

    return (
        <KeyboardAvoidingView behavior={Constants.platform === 'ios' ? 'padding' : undefined} style={{ flex: 1, backgroundColor: Colors.white }}>
            <Appbar.Header style={{ height: 90, elevation: 0, backgroundColor: Colors.white }}>
                <View style={{ width: wp(22) }}>
                    <Pressable
                        onPress={() => props.navigation.goBack()}>
                        <Image source={Constants.back} style={{ paddingHorizontal: 6, width: 35, height: 23, resizeMode: 'contain' }} />
                    </Pressable>
                </View>
                <Appbar.Content titleStyle={{ fontFamily: Constants.NiveauGroteskBold, fontWeight: '700', color: '#0E134F', fontSize: wp(5) }} title='Profile Settings' />
                {<View style={[styles.locationView, !isVerified && styles.locationViewUnVerified]}>
                    <Image style={[styles.locationIcon, !isVerified && { tintColor: Colors.unverfiedText }]} source={!isVerified ? Constants.unverified : Constants.check} />
                    <Text style={[styles.locationLabel, !isVerified && { color: Colors.unverfiedText }]}>{isVerified ? 'Verified' : 'Unverified'}</Text>
                </View>}
            </Appbar.Header>
            <View style={{ flex: 1 }}>
                <ScrollView>
                    <View style={styles.profilePictureContainer}>
                        <View
                            style={{ justifyContent: 'center', alignItems: 'center', width: wp(40), height: wp(40), }}>
                            <Image
                                onLoadEnd={() => updateState({ isImageLoading: false, imageError: false })}
                                onError={() => updateState({ profilePicture: null, isImageLoading: false, imageError: true })}
                                source={_.isEmpty(profilePicture) ? Constants.sampleProfile : profilePicture} style={{
                                    width: wp(40),
                                    borderRadius: wp(40) / 2,
                                    overflow: 'hidden',
                                    position: 'absolute',
                                    height: wp(40), resizeMode: _.isEmpty(profilePicture) ? 'contain' : 'cover'
                                }} />
                        </View>
                        <Pressable onPress={() => openCamera()} style={styles.editButton}>
                            <Image source={Constants.upload}
                                style={{ width: 35, height: 35, resizeMode: 'contain' }}
                            />
                        </Pressable>
                    </View>
                    <View style={styles.spacer} />
                    <Text style={styles.buttonTitle}>Name</Text>
                    <View style={styles.spacer} />
                    <AuthInput
                        value={FullName}
                        onChangeText={(FullName) => updateState({ FullName })}
                        style={{ marginHorizontal: wp(8) }}
                        placeholder={'Name'}
                    />
                    <View style={styles.spacer} />
                    <View style={styles.spacer} />
                    <Text style={styles.buttonTitle}>Contact Number</Text>
                    <View style={styles.spacer} />
                    <AuthInput
                        value={PhoneNumber}
                        onChangeText={(PhoneNumber) => updateState({ PhoneNumber })}
                        style={{ marginHorizontal: wp(8) }}
                        placeholder={'Contact Number'}
                    />
                    <View style={styles.spacer} />
                    <View style={styles.spacer} />
                    <Text style={styles.buttonTitle}>About Yourself</Text>
                    <View style={styles.spacer} />
                    <AuthInput
                        value={AboutUs}
                        onChangeText={(AboutUs) => updateState({ AboutUs })}
                        style={{ marginHorizontal: wp(8) }}
                        multiline
                        maxLength={200}
                        placeholder={'About Yourself'}
                    />
                    <View style={styles.spacer} />
                    <View style={styles.spacer} />
                    <View style={{ marginHorizontal: wp(8) }}>

                        <Button
                            onPress={() => _updateProfile()}
                            text="Save"
                        />
                    </View>
                    <View style={styles.spacer} />
                    <View style={styles.spacer} />
                </ScrollView>
            </View>
            {isLoading && <Spinner />}
        </KeyboardAvoidingView>
    );

}

const styles = StyleSheet.create({
    spacer: {
        height: hp(2)
    },
    locationViewUnVerified: {
        backgroundColor: Colors.unverified,
    },
    locationView: {
        backgroundColor: Colors.verified,
        minWidth: wp(22),
        paddingHorizontal: wp(2),
        borderTopLeftRadius: 25,
        borderBottomLeftRadius: 25,
        height: 40,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    locationIcon: {
        marginHorizontal: wp(2),
        tintColor: Colors.verfiedText,
        width: 16,
        height: 20,
        resizeMode: 'contain'
    },
    locationLabel: {
        color: Colors.verfiedText,
        fontSize: wp(3),
        fontFamily: Constants.NiveauGroteskRegular
    },
    settingButton: {
        flexDirection: 'row',
        height: hp(7),
        alignItems: 'center',
        borderRadius: 5,
        marginHorizontal: wp(4),
        paddingHorizontal: wp(5),
        ...Constants.boxShadow,
        backgroundColor: Colors.white
    },
    buttonIcon: {
        width: wp(5),
        height: wp(5),
        resizeMode: 'contain'
    },
    buttonTitle: {
        marginHorizontal: wp(8),
        color: '#0E134F',
        fontFamily: Constants.NiveauGroteskRegular,
        fontSize: wp(3.8),
    },
    profilePictureContainer: {
        alignSelf: 'center',
        marginVertical: hp(4),
        // backgroundColor: Colors.primaryLight,
        width: wp(40),
        height: wp(40),
        borderRadius: wp(40) / 2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    editButton: {
        position: 'absolute',
        right: 10,
        bottom: 10
    },
});

export default ProfileSettings;