import { Colors, Constants } from 'common';
import { ImageView } from 'components';
import _ from 'lodash';
import moment from 'moment';
import * as React from 'react';
import { Text, View, StyleSheet, Image, Pressable } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { InvitationItemModel } from 'reduxStore/models/invitation';
import { NotificationsItemModel } from 'reduxStore/models/users';

interface NotificationItemProps {
    item: NotificationsItemModel,
    index: number,
    onPress: () => void
}

const NotificationItem = (props: NotificationItemProps) => {
    let imageUrl = Constants.logo;
    let user = props.item.Meta.User || {};
    if (!_.isEmpty(user.Media)) {
        imageUrl = {
            uri: user.Media.BasePath + user.Media.ThumbPath
        };
    };

    return (
        <Pressable onPress={props.onPress} style={[styles.container]}>
            <ImageView
                style={styles.image} source={imageUrl} />
            <View style={{ flex: 1, paddingHorizontal: wp(3) }}>
                <Text numberOfLines={1} style={styles.messageTime}>{moment(props.item.CreatedAt).fromNow()}</Text>
                <Text numberOfLines={1} style={styles.userDescription}>{user.FullName} {props.item.Title}</Text>
            </View>
        </Pressable>
    );
};

export default NotificationItem;

const styles = StyleSheet.create({
    image: {
        width: wp(12),
        height: wp(12),
        resizeMode: 'cover',
        borderRadius: 10
    },
    container: {
        borderWidth: 0.5,
        borderColor: 'transparent',
        backgroundColor: Colors.white,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: wp(2),
        paddingVertical: wp(3),
        marginHorizontal: wp(3),
        borderRadius: 6,
        ...Constants.boxShadow
    },
    reviewCount: {
        color: Colors.primary,
        fontFamily: Constants.NiveauGroteskMedium,
        fontSize: wp(3)
    },
    userTitle: {
        color: '#0E134F',
        fontFamily: Constants.NiveauGroteskMedium,
        fontSize: wp(3.6),
        paddingVertical: wp(1),
        paddingRight: wp(2)
    },
    messageTime: {
        color: Colors.primary,
        fontFamily: Constants.NiveauGroteskMedium,
        fontSize: wp(2.8),
        paddingBottom: 5
    },
    userDescription: {
        color: '#42526EB2',
        fontFamily: Constants.NiveauGroteskRegular,
        fontSize: wp(3.3)
    },
});
