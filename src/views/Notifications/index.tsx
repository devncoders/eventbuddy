
import React, { useEffect, useState } from 'react';
import {
    View,
    StyleSheet,
    KeyboardAvoidingView,
    FlatList,
    Modal
} from 'react-native';
import { Appbar, } from 'react-native-paper';
import { Colors, Constants, Utils } from 'common';

import NotificationItem from './NotificationItem';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import _ from 'lodash';
import { UserModel } from 'reduxStore/models';
import { useSelector } from 'react-redux';
import { RootState } from 'reduxStore/reducers';
import { getNotifications } from 'reduxStore/services/user';
import { NotificationsItemModel } from 'reduxStore/models/users';
import { getInvitationById } from 'reduxStore/services/invitations';
import { ErrorView, Spinner } from 'components';
import { InvitationItemModel, InvitationListResponse } from 'reduxStore/models/invitation';
import EventDetailView from 'views/MessageDetail/EventDetailView';
let initial = {
    notifications: [] as Array<NotificationsItemModel>,
    title: '',
    description: '',
    isLoading: false,
    showDetailView: false,
    isFetching: true,
    invitationDetails: {} as InvitationItemModel
}

const Notifications = (props: any) => {
    let navigation = props.navigation;
    let user: UserModel = useSelector((state: RootState) => state.main.userData);
    const [{
        notifications,
        isLoading,
        invitationDetails,
        isFetching,
        showDetailView
    }, setState] = useState(initial);

    const updateState = (state: {}) => {
        setState(prevState => ({ ...prevState, ...state }));
    }

    const _fetchNotifications = () => {
        getNotifications().then(res => {
            if (res.Status === 200 && !_.isEmpty(res.Data)) {
                console.log('res.Data.notifications = = > ', res.Data.notifications);

                updateState({
                    notifications: res.Data.notifications,
                    isLoading: false,
                    isFetching: false
                });
                // props.onPress();
            } else {
                Utils.showToast(res.Message)
                updateState({ isLoading: false, isFetching: false });
            }
        }).catch(err => {
            Utils.showToast(err.Message)
            updateState({ isLoading: false, isFetching: false });
        })
    }

    useEffect(() => {
        _fetchNotifications()
    }, []);

    const onNotificationPress = (item: NotificationsItemModel) => {
        if (item.Type === 1) {
            updateState({ isFetching: true });
            getInvitationById(item.Meta.InvitationId).then(res => {
                if (res.Status === 200 && !_.isEmpty(res.Data)) {
                    let _data = res.Data;
                    console.log('_data', _data);

                    updateState({ isFetching: false, showDetailView: true, invitationDetails: _data });
                }
            }).catch(err => {
                updateState({ isFetching: false });
            })
        }
    }


    return <KeyboardAvoidingView behavior={Constants.platform == 'ios' ? 'padding' : undefined} style={{ flex: 1, backgroundColor: 'white' }}>
        <Appbar.Header style={{ height: 90, elevation: 0, justifyContent: 'flex-start', backgroundColor: Colors.white }}>
            <View>
                <Appbar.Content style={{ flex: undefined, }} titleStyle={{ fontFamily: Constants.NiveauGroteskBold, fontWeight: '700', color: '#0E134F', fontSize: wp(5) }} title='Notifications' />
            </View>
            <View style={{ flex: 1 }} />
        </Appbar.Header>

        <View style={{ flex: 1 }}>
            {notifications.length !== 0 ? <FlatList
                refreshing={isLoading}
                onRefresh={() => _fetchNotifications()}
                data={notifications}
                ListHeaderComponent={() => <View style={{ height: hp(2) }} />}
                ListFooterComponent={() => <View style={{ height: hp(1.5) }} />}
                ItemSeparatorComponent={() => <View style={{ height: hp(1.5) }} />}
                keyExtractor={(item) => item.Id.toString()}
                extraData={notifications}
                renderItem={(props) => <NotificationItem onPress={() => onNotificationPress(props.item)} {...props} />}
            /> : !isFetching && <ErrorView onRefresh={() => {
                updateState({ isFetching: true });
                _fetchNotifications()
            }} errorMessage='No Notifications!' />}
        </View>
        <Modal
            transparent
            animationType='slide'
            onRequestClose={() => updateState({ showDetailView: false, invitationDetails: {} })}
            visible={!_.isEmpty(invitationDetails) && showDetailView}
        >
            <EventDetailView
                showActionButtons={invitationDetails.Status === 'pending'}
                onPress={() => updateState({ showDetailView: false, invitationDetails: {} })}
                invitationDetails={invitationDetails} />
        </Modal>

        {isFetching && <Spinner />}
    </KeyboardAvoidingView>
}

export default Notifications;

const styles = StyleSheet.create({
    spacer: {
        height: 12
    },
    locationView: {
        backgroundColor: '#FCEDE4',//Colors.primary,
        width: wp(40),
        borderTopLeftRadius: 25,
        borderBottomLeftRadius: 25,
        height: 40,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    locationIcon: {
        width: wp(4),
        marginHorizontal: wp(2),
        height: '80%',
        resizeMode: 'contain'
    },
    locationLabel: {
        color: '#42526E',
        fontSize: wp(3),
        fontFamily: Constants.NiveauGroteskBold
    },
    errorText: {
        color: '#ff5252',
        paddingTop: 5,
        fontSize: 12
    },
    textInput: {
        marginTop: 10,
        height: 35,
        backgroundColor: '#eee',
        borderRadius: 5,
        paddingHorizontal: 10,
        color: Colors.black
    },
    add_photoIcon: {
        width: 100,
        height: 100,
        resizeMode: 'contain',
    }
});
