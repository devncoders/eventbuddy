
import React, { useEffect, useState } from 'react';
import {
    SafeAreaView,
    Pressable,
    Text,
    View,
    Image,
    StyleSheet,
    FlatList
} from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

import geolocation from '@react-native-community/geolocation';
import { Constants, Colors, Utils } from 'common';
import messaging from '@react-native-firebase/messaging';
import { Button, Header, Spinner } from 'components';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { openSettings, PERMISSIONS } from 'react-native-permissions';

let initial = {
    loading: false,
    showLoginView: false,
    userPosition: null,
}
const LocationSearchView = (props: any) => {

    const [{
        loading
    }, setState] = useState(initial);

    const updateState = (state: {}) => {
        setState(prevState => ({ ...prevState, ...state }));
    }

    useEffect(() => {
    }, []);

    let backView = props.route.params.viewName;

    return <View style={{ flex: 1, backgroundColor: Colors.white }}>
        <Header title={'Search Location'} />
        <View style={{ flex: 1, backgroundColor: 'red' }}>
            <GooglePlacesAutocomplete
                placeholder='Search'
                onFail={(props) => {
                    console.log('onFail', props)
                }}
                onNotFound={() => {
                    console.log('onNotFound')
                }}
                onPress={(data, details = null) => {
                    // 'details' is provided when fetchDetails = true
                    // props.navigation(backView, { selectedLocation: item })
                    console.log(data, details);
                }}
                query={{
                    key: Constants.apiKey,
                    language: 'en',
                }}
            />
        </View>
    </View>
}

export default LocationSearchView;

const styles = StyleSheet.create({

    imageView: {
        width: wp(70),
        height: wp(70),
        resizeMode: 'contain'
    },
    description: {
        fontSize: wp(3.2),
        color: '#42526E',
        fontWeight: '400',
        fontFamily: Constants.NiveauGroteskRegular,
        paddingVertical: hp(2.2),
        textAlign: 'center',
        paddingHorizontal: wp(6)
    },
});
