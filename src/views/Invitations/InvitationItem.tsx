import { Colors, Constants } from 'common';
import { ImageView } from 'components';
import _ from 'lodash';
import * as React from 'react';
import { Text, View, StyleSheet, Image, Pressable } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { InvitationItemModel } from 'reduxStore/models/invitation';

interface InvitationItemProps {
    item: InvitationItemModel,
    index: number,
    onPress: () => void
}

const InvitationItem = (props: InvitationItemProps) => {
    let imageUrl = Constants.logo;
    let userItem = props?.item?.User || props?.item?.CreatedBy;
    if (!_.isEmpty(userItem.Media)) {
        imageUrl = {
            uri: userItem.Media.BasePath + userItem.Media.ThumbPath
        };
    };
    return (
        <Pressable onPress={props.onPress} style={styles.container}>
            <ImageView
                style={styles.image}
                source={imageUrl} />
            <View style={{ flex: 1, paddingHorizontal: wp(3) }}>
                <Text numberOfLines={1} style={styles.messageTime}>{userItem.FullName}</Text>
                <Text numberOfLines={1} style={styles.userDescription}>{props.item?.Title}</Text>
            </View>
        </Pressable>
    );
};

export default InvitationItem;

const styles = StyleSheet.create({
    image: {
        width: wp(12),
        height: wp(12),
        resizeMode: 'cover',
        borderRadius: 10,
        overflow: 'hidden',
    },
    container: {
        backgroundColor: Colors.white,
        flexDirection: 'row',
        alignItems: 'center',
        padding: wp(2),
        marginHorizontal: wp(8),
        // marginVertical: wp(2),
        borderRadius: 6,
        ...Constants.boxShadow
    },
    reviewCount: {
        color: Colors.primary,
        fontFamily: Constants.NiveauGroteskMedium,
        fontSize: wp(3)
    },
    userTitle: {
        color: '#0E134F',
        fontFamily: Constants.NiveauGroteskMedium,
        fontSize: wp(3.6),
        paddingVertical: wp(1),
        paddingRight: wp(2)
    },
    messageTime: {
        color: Colors.primary,
        fontFamily: Constants.NiveauGroteskMedium,
        fontSize: wp(2.5),
    },
    userDescription: {
        color: '#42526EB2',
        fontFamily: Constants.NiveauGroteskRegular,
        fontSize: wp(3)
    },
});
