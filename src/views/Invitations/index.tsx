import * as React from 'react';
import { Text, View, StyleSheet, Pressable, ScrollView, KeyboardAvoidingView, Image, Alert, FlatList, Modal } from 'react-native';
import { NavigationScreenProp, NavigationState } from "react-navigation";
import { Header, Input, Button, Spinner } from 'components';
import { Colors, Constants, Utils } from 'common';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import InvitationItem from './InvitationItem';
import { InvitationItemModel } from 'reduxStore/models/invitation';
import { getInvitationById, getInvitations } from 'reduxStore/services/invitations';
import _ from 'lodash';
import EventDetailView from 'views/MessageDetail/EventDetailView';
import { useSelector } from 'react-redux';
import { RootState } from 'reduxStore/reducers';
import { UserModel } from 'reduxStore/models';



interface InvitationsProps {
    navigation: NavigationScreenProp<NavigationState>
}
let initialState = {
    canPerformActions: true,
    isFetching: true,
    isLoading: false,
    selectedTab: 1,
    selectedSubTab: 1,
    invitaions: [] as Array<InvitationItemModel>,
    invitationDetails: {} as InvitationItemModel,
    showDetailView: false
}
const Invitations = (props: InvitationsProps) => {
    const [
        {
            canPerformActions,
            showDetailView,
            invitationDetails,
            isFetching,
            isLoading,
            selectedTab,
            selectedSubTab,
            invitaions
        }, setState
    ] = React.useState(initialState);
    let userData: UserModel = useSelector((state: RootState) => state.main.userData);
    let isVerified = userData.IsVerified;
    const updateState = (newState: {}) => {
        setState(prevState => ({ ...prevState, ...newState }))
    }

    const _getInvitations = () => {
        let invitationType = selectedTab === 1 ? 'sent' : 'received';
        getInvitations(invitationType)
            .then(res => {
                if (res.Status === 200 && !_.isEmpty(res.Data)) {
                    updateState({
                        invitaions: res.Data,
                        isLoading: false,
                        isFetching: false
                    });
                } else {
                    updateState({
                        isLoading: false,
                        isFetching: false
                    });
                }
            })
            .catch(err => {
                updateState({
                    isLoading: false,
                    isFetching: false
                });
            })
    }

    React.useEffect(() => {
        _getInvitations()
    }, [selectedTab]);

    const _getInvitationsByType = (data: Array<InvitationItemModel>) => {
        return data.filter(i => {
            if (selectedSubTab === 1) return i.Status.toLowerCase() === 'accepted'
            if (selectedSubTab === 2) return i.Status.toLowerCase() === 'pending'
            if (selectedSubTab === 3) return i.Status.toLowerCase() === 'rejected'
        })
    }

    const onInvitationPress = (item: InvitationItemModel) => {
        updateState({ isFetching: true, });
        getInvitationById(item.Id).then(res => {
            if (res.Status === 200 && !_.isEmpty(res.Data)) {
                let _data = res.Data;
                updateState({ isFetching: false, invitationDetails: _data });
            }
        }).catch(err => {
            updateState({ isFetching: false });
        })
        //receieved Invitations
        if (selectedTab === 2) {
            if (selectedSubTab === 2) {
                updateState({
                    canPerformActions: true,
                    showDetailView: true,
                })
            } else {
                updateState({
                    canPerformActions: false,
                    showDetailView: true,
                })
            }
        }
        //Sent Invitations
        else {
            updateState({
                canPerformActions: false,
                showDetailView: true,
            })
        }
    }

    return (
        <KeyboardAvoidingView behavior={Constants.platform === 'ios' ? 'padding' : undefined} style={{ flex: 1, backgroundColor: Colors.white }}>
            <Header
                title='Invitations' />
            {/* <View>
            </View> */}
            <View style={styles.subContainer}>

                <View style={styles.mainTabs}>
                    <Pressable onPress={() => updateState({ selectedTab: 1 })} style={{ flex: 1, paddingVertical: 15 }}>
                        <Text style={[styles.tabText, selectedTab === 1 && { color: Colors.primary }]}>Sent Invitations</Text>
                    </Pressable>
                    <View style={styles.seperator} />
                    <Pressable onPress={() => updateState({ selectedTab: 2 })} style={{ flex: 1, paddingVertical: 15, }}>
                        <Text style={[styles.tabText, selectedTab === 2 && { color: Colors.primary }]}>Received Invitations</Text>
                    </Pressable>
                </View>
                <View style={styles.subTabs}>
                    <Pressable onPress={() => updateState({ selectedSubTab: 1 })} style={[styles.subTabButton, selectedSubTab === 1 && { backgroundColor: Colors.primary }]}>
                        <Text style={[styles.tabText, selectedSubTab === 1 && { color: Colors.white }]}>Accepted</Text>
                    </Pressable>
                    <Pressable onPress={() => updateState({ selectedSubTab: 2 })} style={[styles.subTabButton, selectedSubTab === 2 && { backgroundColor: Colors.primary }]}>
                        <Text style={[styles.tabText, selectedSubTab === 2 && { color: Colors.white }]}>Pending</Text>
                    </Pressable>
                    <Pressable onPress={() => updateState({ selectedSubTab: 3 })} style={[styles.subTabButton, selectedSubTab === 3 && { backgroundColor: Colors.primary }]}>
                        <Text style={[styles.tabText, selectedSubTab === 3 && { color: Colors.white }]}>Rejected</Text>
                    </Pressable>
                </View>
                <View style={{ flex: 1 }}>
                    <FlatList
                        refreshing={isLoading}
                        onRefresh={() => _getInvitations()}
                        data={_getInvitationsByType(invitaions)}
                        extraData={_getInvitationsByType(invitaions)}
                        ListHeaderComponent={() => <View style={{ height: hp(3) }} />}
                        ListFooterComponent={() => <View style={{ height: hp(1.5) }} />}
                        ListEmptyComponent={() => <Text style={styles.errorText}>
                            No Invitations
                        </Text>}
                        ItemSeparatorComponent={() => <View style={{ height: hp(1.5) }} />}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={(props) => <InvitationItem
                            onPress={() => onInvitationPress(props.item)} {...props} />}
                    />
                </View>

            </View>
            {isFetching && <Spinner />}
            <Modal
                transparent
                animationType='slide'
                onRequestClose={() => updateState({ showDetailView: false, invitationDetails: {} })}
                visible={!_.isEmpty(invitationDetails) && showDetailView}
            >
                <EventDetailView
                    showActionButtons={canPerformActions}
                    onSuccess={() => {
                        updateState({ isFetching: true });
                        _getInvitations()
                    }}
                    onPress={() => updateState({ showDetailView: false, invitationDetails: {} })}
                    invitationDetails={invitationDetails} />
            </Modal>
        </KeyboardAvoidingView>
    );
};

export default Invitations;

const styles = StyleSheet.create({
    errorText: {
        textAlign: 'center',
        fontSize: wp(3),
        color: Colors.textDark,
        fontFamily: Constants.NiveauGroteskMedium

    },
    tabText: {
        textAlign: 'center',
        fontSize: wp(3),
        color: Colors.textDark,
        fontFamily: Constants.NiveauGroteskMedium
    },
    subContainer: {
        flex: 1
    },
    seperator: {
        width: 1,
        height: '60%',
        backgroundColor: Colors.borderColor
    },
    mainTabs: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: hp(2),
        backgroundColor: Colors.white,
        ...Constants.boxShadow
    },
    subTabs: {
        zIndex: 9999999,
        marginHorizontal: wp(8),
        padding: 5,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: hp(3),
        backgroundColor: '#42526E0F',
        height: 45,
        borderRadius: 8,
    },
    subTabButton: {
        flex: 1,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8
    }
});
