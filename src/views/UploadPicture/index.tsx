
import React, { useEffect, useState } from 'react';
import {
    SafeAreaView,
    Pressable,
    Text,
    View,
    Dimensions,
    StyleSheet,
    Image
} from 'react-native';

import * as Animated from 'react-native-animatable';
import { Constants, Colors, Utils } from 'common';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Button, Header, Spinner } from 'components';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import *  as ImagePicker from 'react-native-image-picker';
import { MediaType, PhotoQuality, } from 'react-native-image-picker';
import { RouteProp, useNavigation } from '@react-navigation/native';
import { uploadMedia } from 'reduxStore/services/user';
import _ from 'lodash';
interface CategoryItem {
    title: string,
    image: any
}
let initial = {
    picture: '',
    fileName: '',
    imageType: '',
    isLoading: false,
    showLoginView: false,
    imageUploaded: null,
    mediaId: null
}
const { width } = Dimensions.get('window')
let cameraOptions = {
    mediaType: 'photo' as MediaType,
    quality: __DEV__ ? 0.1 : 0.6 as PhotoQuality,

}
interface UploadPictureProps {
    route: RouteProp<{ params: { userData: {} } }, 'params'>
}
const UploadPicture = (props: UploadPictureProps) => {
    let navigation = useNavigation();
    const [{
        imageUploaded,
        picture,
        fileName,
        imageType,
        mediaId,
        isLoading
    }, setState] = useState(initial);

    const verifyPicture = () => {
        if (picture === '') return;
        let userData = props.route.params.userData
        navigation.reset({
            index: 0,
            routes: [
                { name: 'verifyPicture', params: { mediaId, picture, fileName, imageType, userData } }
            ]
        })
    }

    const _uploadPicture = () => {
        updateState({ isLoading: true })
        let body = new FormData();
        body.append('file',
            {
                uri: picture,
                name: fileName,
                type: imageType
            });
        uploadMedia(body).then(res => {
            if (res.Status === 200 && !_.isEmpty(res.Data)) {
                updateState({ imageUploaded: true, mediaId: res.Data.Id });
                // verifyPicture()
            }
        }).catch(err => {
            updateState({ isLoading: false });
        })
    }

    useEffect(() => {
        if (mediaId) {
            verifyPicture();
        }
    }, [mediaId]);

    const onImageSuccess = (response: ImagePicker.ImagePickerResponse) => {
        let assets = response.assets || [];
        let uri = assets[0] && assets[0].uri || '';
        let imageType = assets[0] && assets[0].type || '';
        let fileName = assets[0] && assets[0].fileName || '';
        const realPath = Constants.platform === 'ios' ? uri.replace('file://', '') : uri;
        Utils.getResolvedPath(realPath).then(pathRes => {
            var str1 = "file://";
            var str2 = pathRes.path;
            var correctpath = str1.concat(str2);
            updateState({
                imageType,
                fileName,
                isLoading: false,
                picture: correctpath,
                imageUploaded: null
            });
        }).catch(err => {
            console.log('=====> ', err)
        })
    }

    const openCamera = () => {
        if (__DEV__ && Constants.platform === 'ios') {
            ImagePicker.launchImageLibrary(
                cameraOptions,
                async response => {

                    if (!response.errorCode && !response.didCancel) {
                        onImageSuccess(response);
                        // updateState({ picture: response.assets[0].uri })
                    } else {
                        updateState({ isLoading: false });
                    }
                },
            );
            return
        }
        Utils.checkPermissionForUsage('camera')
            .then(res => {
                updateState({ isLoading: true });
                ImagePicker.launchCamera(
                    cameraOptions,
                    async response => {

                        if (!response.errorCode && !response.didCancel) {
                            onImageSuccess(response);
                            // updateState({ picture: response.assets[0].uri })
                        } else {
                            updateState({ isLoading: false });
                        }
                    },
                );
            })
            .catch(error => {
                Utils.showToast('Camera Permssion required');
                updateState({ isLoading: false });
            })

    }
    const updateState = (state: {}) => {
        setState(prevState => ({ ...prevState, ...state }));
    }

    useEffect(() => {
    }, [])

    return <View style={{ flex: 1, backgroundColor: Colors.white }}>
        <Header title={'Upload Profile Picture'} />
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingHorizontal: wp(10) }}>
            <View>
                <View style={styles.cameraIconContainer}>
                    <Image style={styles.cameraImage} source={picture === '' ? require('assets/upload-image.png') : { uri: picture }} />
                    <Pressable onPress={() => openCamera()} style={styles.editButton}>
                        <Image source={Constants.edit} style={styles.editIcon} />
                    </Pressable>
                </View>
            </View>
            <Text style={styles.description}>Please make sure to be in a spot with sufficient light on your face.</Text>
        </View>
        <View style={{ paddingVertical: hp(2), paddingHorizontal: wp(5) }}>
            <Button
                disabled={picture === ''}
                onPress={() => _uploadPicture()} text='Next' />
        </View>
        {isLoading && <Spinner />}
    </View>
}

export default UploadPicture;

const styles = StyleSheet.create({
    editIcon: {
        width: wp(4),
        height: wp(4),
        resizeMode: 'contain'
    },
    editButton: {
        position: 'absolute',
        width: wp(8),
        height: wp(8),
        borderRadius: wp(8) / 2,
        borderColor: Colors.borderColor,
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.white,
        right: 20,
        bottom: 10
    },
    cameraImage: {

        width: wp(50),
        height: wp(50),
        borderRadius: wp(50) / 2,
    },
    cameraIconContainer: {
        width: wp(50),
        height: wp(50),
        // backgroundColor: '#F9F9FA',
        // borderRadius: wp(50) / 2,
        justifyContent: 'center',
        alignItems: 'center',
        /* borderWidth: 1,
        b */orderColor: Colors.borderColor
    },
    categoryItem: {
        elevation: 4,
        borderRadius: 8,
        padding: 12,
        margin: 10,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    description: {
        fontSize: 14,
        textAlign: 'center',
        color: '#42526E',
        fontFamily: Constants.NiveauGroteskMedium,
        paddingVertical: hp(3),
        lineHeight: 20,
    }
});
