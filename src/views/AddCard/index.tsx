import * as React from 'react';
import { Text, TouchableOpacity, View, StyleSheet, Pressable, ScrollView, KeyboardAvoidingView, Image, Alert } from 'react-native';
import { NavigationScreenProp, NavigationState } from "react-navigation";
import { Header, Input, Button, Label } from 'components';
import { Colors, Constants, Utils } from 'common';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { Avatar, IconButton } from 'react-native-paper';



interface AddCardProps {
    navigation: NavigationScreenProp<NavigationState>
}
let initialState = {
    first_name: '',
    last_name: '',
    email: '',
    call_code: '71',
    phone_number: '',
    address: '',
    isLoading: false,
}
const AddCard = (props: AddCardProps) => {
    const [
        {
            isLoading,
            first_name,
            last_name,
            email,
            call_code,
            phone_number,
            address
        }, setState
    ] = React.useState(initialState);

    const updateState = (newState: {}) => {
        setState(prevState => ({ ...prevState, ...newState }))
    }

    const isDisabled = () => {
        return first_name === '' || last_name === ''
            || !Utils.validateEmail(email) || phone_number === '' || address === '';
    }

    return (
        <KeyboardAvoidingView behavior={Constants.platform === 'ios' ? 'padding' : undefined} style={{ flex: 1, backgroundColor: Colors.white }}>
            <Header
                title='Add Card' />
            {/* <View>
            </View> */}
            <View style={styles.subContainer}>
                <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
                    <View style={{ paddingVertical: hp(6) }}>
                        <View style={{ alignSelf: 'center', }}>
                            <Avatar.Image source={Constants.placeHolder} size={wp(25)} />
                            <TouchableOpacity
                                activeOpacity={.7}
                                onPress={() => { }}
                                style={styles.cameraButton}>
                                <Image source={Constants.add_image}
                                    style={styles.cameraButtonImage}
                                />
                            </TouchableOpacity>
                        </View>
                        <Pressable style={styles.uploadCoverButton}>
                            <Image style={styles.uploadCoverButtonIcon} source={Constants.upload} />
                            <Text style={styles.uploadCoverButtonText}>Upload Cover</Text>
                        </Pressable>
                    </View>
                    <View style={{ backgroundColor: Colors.lightBg }}>
                        <View style={styles.otherInfoBox}>
                            <Text style={styles.otherInfoText}>Mark Albert</Text>
                        </View>
                        <View style={styles.otherInfoBox}>
                            <Text style={styles.otherInfoText}>Creative Design</Text>
                        </View>
                        <View style={styles.otherInfoBox}>
                            <Text style={styles.otherInfoText}>Advertising</Text>
                        </View>
                    </View>
                    <View style={{ paddingHorizontal: wp(5), backgroundColor: Colors.white }}>
                        <View style={styles.spacer} />
                        <Label textEng='Email' />
                        <Input
                            rightIcon={Constants.email}
                            value={first_name}
                            returnKeyType='next'
                            onChangeText={(first_name) => updateState({ first_name })}
                            placeholder='First Name' />
                        <View style={styles.spacer} />
                        <Label textEng='Phone' />
                        <Input
                            value={phone_number}
                            isPhone
                            rightIcon={Constants.call}
                            call_code={call_code}
                            selectCallCode={(call_code) => updateState({ call_code })}
                            onChangeText={(phone_number) => updateState({ phone_number })}
                            placeholder='Phone Number'
                            returnKeyType='done'
                            keyboardType='phone-pad'

                        />
                        <View style={styles.spacer} />
                        <Label textEng='Address' />
                        <Input
                            value={email}
                            rightIcon={Constants.location}
                            onChangeText={(email) => updateState({ email })}
                            placeholder='Email'
                            autoCapitalize='none'
                            autoCorrect={false}
                            returnKeyType='next'
                            keyboardType='email-address'
                        />

                        <View style={styles.spacer} />
                        <Label textEng="Add URL's:" />
                        <Input
                            value={address}
                            onChangeText={(address) => updateState({ address })}
                            placeholder='Address'
                            returnKeyType='done'
                        />
                        <View style={styles.spacer} />
                        <View style={styles.spacer} />
                        <Button
                            isDisabled={isDisabled()}
                            onPress={() => { }}
                            text='Submit' />
                        <View style={styles.spacer} />
                        <View style={styles.spacer} />
                    </View>
                </ScrollView>
            </View>
        </KeyboardAvoidingView>
    );
};

export default AddCard;

const styles = StyleSheet.create({
    cameraButton: {
        borderRadius: 50,
        backgroundColor: Colors.primary,
        position: 'absolute',
        right: -5,
        bottom: -5,
        zIndex: 99
    },
    cameraButtonImage: {
        width: wp(8),
        height: wp(8),
        resizeMode: 'contain'
    },
    spacer: {
        height: hp(3)
    },
    headerTitle: {
        color: Colors.black
    },
    subContainer: {
        flex: 1,
        backgroundColor: Colors.greyBg,
    },
    uploadCoverButton: {
        position: 'absolute',
        right: 0,
        bottom: 0,
        flexDirection: 'row',
        alignItems: 'center',
        borderTopLeftRadius: 20,
        backgroundColor: Colors.white,
        paddingVertical: hp(1),
        paddingHorizontal: wp(4)
    },
    uploadCoverButtonIcon: {
        width: wp(4),
        height: wp(4),
        resizeMode: 'contain'
    },
    uploadCoverButtonText: {
        fontFamily: Constants.popins_regular,
        color: Colors.primary,
        paddingLeft: wp(3)
    },
    otherInfoBox: {
        paddingVertical: hp(2.5),
        alignItems: 'center',
        borderBottomWidth: .5,
        borderBottomColor: Colors.borderColor
    },
    otherInfoText: {
        fontFamily: Constants.popins,
        color: Colors.primary,
        fontSize: wp(4)
    }
});
