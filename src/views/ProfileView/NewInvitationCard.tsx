import { Colors, Constants, Utils } from 'common';
import { AuthInput, Button, DateSelector, Input } from 'components';
import _ from 'lodash';
import moment from 'moment';
import * as React from 'react';
import { Text, View, StyleSheet, KeyboardAvoidingView, ScrollView, Pressable } from 'react-native';
import { IconButton } from 'react-native-paper';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { useSelector } from 'react-redux';
import { UserModel } from 'reduxStore/models';
import { CreateInvitationDataModel } from 'reduxStore/models/invitation';
import { UsersItemModel } from 'reduxStore/models/users';
import { RootState } from 'reduxStore/reducers';
import { createInvitation } from 'reduxStore/services/invitations';

interface NewInvitationCardProps {
    onPress: () => void,
    onLocationTap: () => void,
    userItem: UsersItemModel
}

let initialState = {
    eventName: '',
    date: '',
    time: '',
    location: '',
    description: '',
    isLoading: false
}

const NewInvitationCard = (props: NewInvitationCardProps) => {
    const [{

        isLoading,
        eventName,
        date,
        time,
        location,
        description,
    }, setState] = React.useState(initialState);
    let userLatLng = useSelector((state: RootState) => state.main.userLatLng);

    const onSubmit = () => {
        let data: CreateInvitationDataModel = {
            "Title": eventName,
            "Description": description,
            "Time": moment(date + ' ' + time).toISOString(),
            "Lat": userLatLng.latitude,
            "Lng": userLatLng.longitude,
            "Address": location,
            "UserId": props.userItem.Id
        }
        updateState({ isLoading: true });
        console.log(data);

        createInvitation(data).then(res => {
            if (res.Status === 200 && !_.isEmpty(res.Data)) {
                Utils.showToast('Event Created Successfully!', 'Success');
                props.onPress();
            } else {
                Utils.showToast(res.Message)
                updateState({ isLoading: false });
            }
        }).catch(err => {
            Utils.showToast(err.Message)
            updateState({ isLoading: false });
        })
    }

    const updateState = (newState = {}) => {
        setState(prevState => ({ ...prevState, ...newState }));
    }

    const isDisabled = () => {
        return eventName === '' ||
            date === '' ||
            time === '' ||
            location === '' ||
            description === ''
    }

    return (
        <KeyboardAvoidingView style={{ flex: 1 }} behavior={Constants.platform === 'ios' ? 'padding' : undefined}>
            <View style={styles.container}>
                <View style={styles.subContainer}>
                    <ScrollView>
                        <View style={{ paddingHorizontal: wp(2), flexDirection: 'row', alignItems: 'center', }}>
                            <IconButton
                                icon={'close'}
                                style={{ opacity: 0, }}
                                color={Colors.primary}
                            />
                            <Text style={[styles.title, { flex: 1 }]}>Invitation Message</Text>

                            <IconButton
                                onPress={() => props.onPress()}
                                icon={'close'}
                                style={styles.cancelButton}
                                color={Colors.primary}
                            />
                        </View>
                        <View style={{ height: hp(2) }} />
                        <View style={styles.formContainer}>
                            <Input
                                value={eventName}
                                onChangeText={(eventName) => updateState({ eventName })}
                                placeholder={'Event Name'} />
                            <View style={{ height: hp(2) }} />
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: 1 }}>
                                    <DateSelector
                                        onDateSelect={(date) => updateState({ date })}
                                        type='date'
                                        iconName='calendar'
                                        placeholder={'Pick Date'}
                                    />
                                </View>
                                <View style={{ width: wp(3) }} />
                                <View style={{ flex: 1 }}>
                                    <DateSelector
                                        type='time'
                                        onDateSelect={(time) => updateState({ time })}
                                        iconName='clock'
                                        placeholder={'Pick Time'}
                                    />
                                </View>
                            </View>
                            <View style={{ height: hp(2) }} />
                            <Pressable onPress={() => props.onLocationTap()}>

                                <Input

                                    // isButton
                                    leftIcon={true}
                                    value={location}
                                    onChangeText={(location) => updateState({ location })}
                                    placeholder={'Event Location'}
                                />
                            </Pressable>
                            <View style={{ height: hp(2) }} />
                            <Input
                                multiline
                                value={description}
                                onChangeText={(description) => updateState({ description })}
                                placeholder={'Event Description'}
                            />
                            <View style={{ height: hp(2) }} />
                            <View style={{ height: hp(2) }} />
                            <Button
                                isLoading={isLoading}
                                // disabled={isDisabled()}
                                onPress={() => onSubmit()} text='Send' />
                        </View>
                    </ScrollView>
                </View>
            </View>
        </KeyboardAvoidingView>
    );
};

export default NewInvitationCard;

const styles = StyleSheet.create({
    cancelButton: {
        /* position: 'absolute',
        top: 10,
        right: 10,
        zIndex: 999, */
    },
    container: {
        backgroundColor: 'rgba(255,255,255,0.8)',
        flex: 1,
        paddingHorizontal: wp(5),
        justifyContent: 'center',
        alignItems: 'center',
    },
    subContainer: {
        width: wp(90),
        backgroundColor: Colors.white,
        height: hp(80),
        borderRadius: 10,
        ...Constants.boxShadow
    },
    title: {
        fontFamily: Constants.NiveauGroteskBold,
        fontWeight: '700',
        paddingVertical: hp(3),
        textAlign: 'center',
        color: '#0E134F',
        fontSize: wp(5)
    },
    formContainer: {
        paddingHorizontal: wp(5),
        paddingVertical: hp(3),
    }
});
