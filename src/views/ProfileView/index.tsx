
import React, { useEffect, useState } from 'react';
import {
    Image,
    View,
    StyleSheet,
    KeyboardAvoidingView,
    Text,
    ScrollView,
    Modal
} from 'react-native';
import { Colors, Constants, Utils } from 'common';
import { Rating, AirbnbRating } from 'react-native-ratings';

import { Header, Button, Spinner, ImageView, ActionSheet } from 'components';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/FontAwesome5';
import ReviewItem from 'views/Reviews/ReviewItem';
import NewInvitationCard from './NewInvitationCard';
import { RouteProp, useNavigation } from '@react-navigation/native';
import { UsersItemModel } from 'reduxStore/models/users';
import _ from 'lodash';
import { getProfile, getUserProfile, getUserReviews } from 'reduxStore/services/user';
import { InvitationItemModel } from 'reduxStore/models/invitation';
import { UserModel } from 'reduxStore/models';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'reduxStore/reducers';
import { updateUser } from 'reduxStore/actions/auth';
import ErrorHandler from 'common/errorHandler';
let initial = {
    title: '',
    description: '',
    isLoading: true,
    showCard: false,
    showLocationView: false,
    showCameraPicker: false,
    userReviews: [],
    AvgRating: 0
}

interface ProfileViewProps {
    route: RouteProp<{ params: { user: UsersItemModel, fromEvent?: boolean, eventId?: number } }, 'params'>
}

const ProfileView = (props: ProfileViewProps) => {
    let userData: UserModel = useSelector((state: RootState) => state.main.userData);
    let user = props.route.params.user;
    const navigation = useNavigation();
    const [{
        AvgRating,
        showCameraPicker,
        isLoading,
        showCard,
        userReviews,
    }, setState] = useState(initial);

    const updateState = (state: {}) => {
        setState(prevState => ({ ...prevState, ...state }));
    }

    const _fetchUserDetails = () => {
        getUserProfile(user.Id).then(res => {
            updateState({ userDetails: res.Data });
        }).catch(err => { });
    }
    const _fetchUserReviews = () => {
        getUserReviews(user.Id).then(res => {
            updateState({ isViewloaded: true, isLoading: false, isFetching: false })
            if (res.Status === 200) {
                updateState({ userReviews: res.Data.Reviews, AvgRating: res.Data.AvgRating });
            }
        }).catch(err => {
            updateState({ isViewloaded: true, isLoading: false, isFetching: false });
        }).finally(() => {
            updateState({ isLoading: false });
        })
    }

    const dispatch = useDispatch();

    const _fetchUserProfile = () => {
        getProfile()
            .then(res => {
                if (res.Status === 200 && !_.isEmpty(res.Data)) {
                    dispatch(updateUser(res.Data.User));
                }
            })
    }

    const getUserDetails = () => {
        Promise.all([
            _fetchUserProfile(),
            _fetchUserDetails(),
            _fetchUserReviews()
        ])
    }

    useEffect(() => {
        getUserDetails();
    }, []);

    const renderReview = () => {
        if (userReviews)
            return userReviews.filter((i, index) => index < 2).map((item, index) => <ReviewItem hideBorder={index === 1} key={index} item={item} />)
    }

    let image = Constants.logo;
    if (!_.isEmpty(user.Media)) {
        image = {
            uri: user.Media.BasePath + user.Media.ThumbPath
        }
    }
    let rating = 5;

    const renderRatingStars = () => {
        let data = [];
        for (let index = 0; index < 5; index++) {
            data.push(<View>
                <Image style={{ marginHorizontal: 1, width: 13, height: 13, resizeMode: 'contain' }} source={index < AvgRating ? Constants.star_icon : Constants.star} />
            </View>);
        }
        return data;
    }
    return <KeyboardAvoidingView behavior={Constants.platform == 'ios' ? 'padding' : undefined} style={{ flex: 1, backgroundColor: 'white' }}>
        <Header
            title={user.FullName}
        />
        <View style={{ flex: 1 }}>
            <ScrollView style={{ paddingHorizontal: wp(7) }}>
                <ImageView source={image} style={styles.userImage} />
                <Text style={styles.title}>About</Text>
                <Text style={styles.description}>{user.About || 'Not Available'}</Text>
                <View style={styles.reviewTitleContainer}>

                    <Text style={styles.title}>Reviews</Text>
                    <View style={styles.ratingContainer}>
                        {renderRatingStars()}
                    </View>
                </View>
                {renderReview()}
                <View style={{ height: hp(2) }} />
                <View style={{ height: hp(2) }} />
                <Button
                    style={{ borderWidth: 0, backgroundColor: Colors.primaryLight }}
                    titleColor={Colors.primary}
                    emptyButton
                    onPress={() => navigation.navigate('reviews', { user })}
                    text='More Reviews' />
                <View style={{ height: hp(2) }} />
                <Button
                    onPress={() => {
                        if (!userData.IsVerified) {
                            Utils.showToast(Constants.unverifiedMessage);
                            return
                        } else {
                            if (props.route.params.eventId && props.route.params.fromEvent) {
                                navigation.navigate('messageDetail', { user, eventId: props.route.params.eventId })
                            } else {
                                updateState({ showCard: true })
                            }
                        }
                    }}
                    leftIcon={Constants.tab_2}
                    text='Send Message' />
                <View style={{ height: hp(4) }} />
            </ScrollView>
        </View>
        {showCameraPicker && <ActionSheet onImageSuccess={(mediaId) => { }}
            onCancel={() => updateState({ showCameraPicker: false })} />}
        <Modal
            transparent
            animationType='slide'
            visible={showCard}
            onRequestClose={() => updateState({ showCard: false })}>
            <NewInvitationCard
                onLocationTap={() => navigation.navigate('locationSearchView', { viewName: 'profileView' })}
                userItem={user}
                onPress={() => updateState({ showCard: false })}
            />
        </Modal>
        {isLoading && <Spinner />}
    </KeyboardAvoidingView>
}

export default ProfileView;

const styles = StyleSheet.create({
    ratingContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    userImage: {
        alignSelf: 'center',
        width: '100%',
        height: hp(35),
        marginVertical: hp(2),
        borderRadius: 15,
        resizeMode: 'cover',
        overflow: 'hidden',
    },
    locationView: {
        backgroundColor: '#FCEDE4',//Colors.primary,
        width: wp(40),
        borderTopLeftRadius: 25,
        borderBottomLeftRadius: 25,
        height: 40,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    locationIcon: {
        width: wp(4),
        marginHorizontal: wp(2),
        height: '80%',
        resizeMode: 'contain'
    },
    locationLabel: {
        color: '#42526E',
        fontSize: wp(3),
        fontFamily: Constants.NiveauGroteskBold
    },
    errorText: {
        color: '#ff5252',
        paddingTop: 5,
        fontSize: 12
    },
    reviewTitleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: hp(1),
        marginBottom: hp(2)
    },
    description: {
        fontSize: wp(3.5),
        color: '#42526E',
        fontFamily: Constants.NiveauGroteskRegular,
        paddingVertical: hp(2.5)
    },
    title: {
        fontFamily: Constants.NiveauGroteskBold,
        fontWeight: '700',
        color: '#0E134F',
        fontSize: wp(5)
    },
});
