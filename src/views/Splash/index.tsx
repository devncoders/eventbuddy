
import React, { useEffect, useState } from 'react';
import {
    Dimensions,
    View,
    StyleSheet,
    ImageBackground,
    AsyncStorage
} from 'react-native';
import * as Animated from 'react-native-animatable';
import { Colors, Constants, Utils } from 'common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { ProgressBar, Subheading, Title } from 'react-native-paper';
import SplashScreen from 'react-native-splash-screen';
import { NavigationActions } from 'react-navigation';
import { setClientToken } from 'reduxStore/services';
import { getProfile, updateProfile } from 'reduxStore/services/user';
import _ from 'lodash';
import Geolocation, { GeolocationResponse } from '@react-native-community/geolocation';
import { openSettings } from 'react-native-permissions';
import { updateUser, updateUserLatlng, updateUserToken } from 'reduxStore/actions/auth';
import Geocoder from '@timwangdev/react-native-geocoder';
import { useDispatch } from 'react-redux';

interface SplashProps {
    navigation: any
};

const Splash = ({ navigation }: SplashProps) => {
    const [isLoading, setLoading] = useState(true);
    useEffect(() => {
        SplashScreen.hide();
        // onFinishAnimation()
    }, []);
    const dispatch = useDispatch();

    const getLocationAddress = async (position: GeolocationResponse['coords']) => {
        try {
            const latLng = { lat: position.latitude, lng: position.longitude };
            let address = await Geocoder.geocodePosition(latLng);
            setLoading(false);
            dispatch(updateUserLatlng({ latitude: position.latitude, longitude: position.longitude }))
            let formattedAddress = address[0].formattedAddress || '';
            navigation.reset({
                index: 0,
                routes: [
                    {
                        name: 'main',
                        state: {
                            routes: [
                                {
                                    name: 'home', params: { userLocation: formattedAddress, userPosition: position }
                                }
                            ]
                        }

                    }
                ]
            })

        } catch (err) {
            console.log(err);
            Utils.showToast('Please check & enable your GPS to continue using the application');
            setLoading(false);
        }
    }

    const onFinishAnimation = async () => {
        let userData = await AsyncStorage.getItem('userData') || '';
        if (userData) {
            setTimeout(() => {
                let _userData = JSON.parse(userData);
                setClientToken(_userData.token);
                dispatch(updateUserToken(_userData.token));
                /* navigation.replace('locationView');
                return; */
                getProfile().then(res => {
                    dispatch(updateUser(res.Data.User));
                    if (!res.Data.User.MediaId || _.isEmpty(res.Data.User.Media)) {
                        navigation.replace('uploadPicture', { userData: _userData });
                    } else {
                        Geolocation.getCurrentPosition(
                            (position) => {
                                let data = {
                                    Lat: position.coords.latitude,
                                    Lng: position.coords.longitude,
                                }
                                updateProfile(data).then(resp => {
                                    getLocationAddress(position.coords);
                                    // navigation.replace('searchView', { userPosition: position.coords });
                                });
                            },
                            (error) => {
                                console.log('error = = = >', error);
                                navigation.replace('locationView');
                                Utils.showToast('Please check & enable your GPS to continue using the application', '', (status) => {
                                    openSettings()
                                        .then(() => console.warn('open settings'))
                                        .catch(() => console.warn('cannot open settings'));
                                });
                            },
                            { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 }
                        );
                    }
                }).catch(err => {
                    navigation.replace('login')
                });
            }, 1000);
        } else {
            setTimeout(() => {
                navigation.replace('login')
            }, 1000);
        }
    }

    return <ImageBackground
        source={Constants.splash_bg}
        resizeMode={'stretch'}
        style={{ backgroundColor: Colors.white, flex: 1 }}>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
            <Animated.Image
                animation="zoomIn"
                easing="ease-out"
                duration={1200}
                onAnimationEnd={() => onFinishAnimation()}
                source={Constants.logo}
                style={{ tintColor: '#fff', overflow: 'hidden', alignSelf: 'center', width: wp(55), height: wp(55), resizeMode: 'contain' }}
            />
        </View>
        <View style={{ paddingHorizontal: wp(20), paddingVertical: hp(2) }}>
            <Subheading style={{ color: Colors.white, alignSelf: 'center' }}>
                Loading
            </Subheading>
            <ProgressBar color={Colors.white} indeterminate />
        </View>
    </ImageBackground>
}

export default Splash;

const styles = StyleSheet.create({
    container: {}
});
