
import React, { useEffect } from 'react';
import {
  View,
  Text,
  StatusBar,
} from 'react-native';
import _ from 'lodash'

import { NavigationScreenProp, NavigationState } from 'react-navigation';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import NetworkLogger from 'react-native-network-logger';
import { Header } from 'components';


interface NetworkLogProps {
  navigation: NavigationScreenProp<NavigationState>;
}

const NetworkLogScreen = (props: NetworkLogProps) => {

  useEffect(() => {
    // if(!_.isEmpty(userData)){
    //   props.navigation.navigate('home')
    // }else{
    //   props.navigation.navigate('auth')
    // }

  }, [])
  return (
    <View style={{ flex: 1, backgroundColor: 'white' }}>
      <Header title={'Network Logger'} />
      <StatusBar barStyle='light-content' />

      <NetworkLogger />

    </View>
  );
};

export default NetworkLogScreen;
