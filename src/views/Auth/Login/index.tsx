
import React, { useEffect, useState } from 'react';
import {
    Image,
    Text,
    Dimensions,
    View,
    StyleSheet,
    Pressable,
    KeyboardAvoidingView,
    ScrollView,
    AsyncStorage,
    Alert
} from 'react-native';
import { Appbar, Button as ButtonPaper, Surface, TextInput } from 'react-native-paper';
import { Input, Button, Heading, AuthInput, Spinner } from 'components';
import * as Animated from 'react-native-animatable';

import { Colors, Constants, Utils } from 'common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { useDispatch } from 'react-redux';
import { loginUserService } from 'reduxStore/services/user';
import { updateUser, updateUserToken } from 'reduxStore/actions/auth';
import { UserModel } from 'reduxStore/models';
import _ from 'lodash';
import { openSettings, PERMISSIONS, request } from 'react-native-permissions';
import Geolocation from '@react-native-community/geolocation';
const { width } = Dimensions.get('window');
interface LoginProps {
    navigation: any
};

let initial = {
    email: __DEV__ ? Constants.platform === 'android' ? 'river@cc.com' : 'testnn@yopmail.com' : '',
    password: __DEV__ ? 'password' : '',
    isLoading: false,
    showLoginView: false,
    selected: 1,
    isEmailValid: false
}

const Login = ({ navigation }: LoginProps) => {

    const [{
        email,
        password,
        isEmailValid,
        isLoading,
        selected
    }, setState] = useState(initial);

    const updateState = (state: {}) => {
        setState(prevState => ({ ...prevState, ...state }));
    }

    const [isSecure, setSecure] = useState(true);
    const dispatch = useDispatch();

    useEffect(() => {
        updateState({ isLoading: false, isEmailValid: Utils.validateEmail(email) });
    }, []);

    const onLogin = () => {
        if (isLoading) return;
        updateState({ isLoading: true });
        loginUserService(email, password)
            .then(res => {
                if (res.Status === 200 && !_.isEmpty(res.Data)) {
                    let {
                        User,
                        Token
                    } = res.Data;
                    let user: UserModel = User;
                    dispatch(updateUser(user));
                    dispatch(updateUserToken(Token));

                    AsyncStorage.setItem('userData', JSON.stringify({
                        ...user,
                        token: Token
                    }));
                    if (!res.Data.User.MediaId || _.isEmpty(res.Data.User.Media)) {
                        navigation.reset({
                            index: 0,
                            routes: [
                                { name: 'uploadPicture', params: { userData: res.Data.User } }
                            ]
                        })
                        // navigation.replace('uploadPicture', { userData: res.Data.User });
                    } else {
                        Geolocation.getCurrentPosition(
                            (position) => {
                                navigation.reset({
                                    index: 0,
                                    routes: [
                                        { name: 'searchView', params: { userPosition: position.coords } }
                                    ]
                                })
                                // navigation.replace('searchView', { userPosition: position.coords });
                            },
                            (error) => {
                                updateState({ isLoading: false });
                                console.log('error = = = >', error);
                                Utils.showToast('Please check & enable your GPS to continue using the application', '', (status) => {
                                    openSettings()
                                        .then(() => console.warn('open settings'))
                                        .catch(() => console.warn('cannot open settings'));
                                });
                                navigation.reset({
                                    index: 0,
                                    routes: [
                                        { name: 'locationView' }
                                    ]
                                })
                            },
                            { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 }
                        );
                    }
                    // navigation.replace('uploadPicture')
                } else {

                    updateState({ isLoading: false });
                    Utils.showToast(res.Message);
                }
            })
            .catch(err => {
                console.warn(err);
                Utils.showToast(err);
                /* let msg = ErrorHandler(err)
                if (err?.errors?.verify_phone) {

                } */
                updateState({ isLoading: false })
            }).finally(() => {
                /* if (__DEV__) {
                    navigation.navigate('uploadPicture', { userData: {} });
                } */
            })
    }

    const isButtonDisabled = () => {
        return email === '' || !isEmailValid || password === '';
    }

    return <KeyboardAvoidingView behavior={Constants.platform === 'ios' ? 'padding' : undefined} style={{ flex: 1, backgroundColor: Colors.white }}>
        <Image source={Constants.mainBg} style={styles.bgImage} />
        <View style={{ flex: 1 }}>
            <ScrollView>
                <View style={{ paddingHorizontal: wp(7) }}>
                    <View style={styles.spacerView} />
                    <View>
                        <Text style={styles.title}>Hey, {'\n'}Login Now</Text>
                    </View>
                    <View style={styles.spacer} />
                    <View style={styles.spacer} />
                    <AuthInput
                        placeholder={`Enter Email here...`}
                        returnKeyType='next'
                        leftIcon={Constants.auth_email}
                        label={'Email'}
                        keyboardType='email-address'
                        onChangeText={(email) => {
                            let isEmailValid = Utils.validateEmail(email);
                            updateState({ email, isEmailValid })
                        }}
                        value={email} />

                    <View style={styles.spacer} />
                    <AuthInput
                        placeholder='Password'
                        label={'Password'}
                        leftIcon={Constants.auth_password}
                        onChangeText={(password) => updateState({ password })}
                        isPassword
                        value={password} />
                    {email !== '' && !isEmailValid && <Text style={styles.errorText}>Invalid Email address</Text>}
                    <View style={{ height: hp(4) }} />
                    <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                        <Text style={styles.resetButtonText}>Forgot Password? </Text>
                        <Pressable onPress={() => navigation.navigate('forgotPass')}>
                            <Text style={[styles.resetButtonText, { color: Colors.primary }]}>Reset Now</Text>
                        </Pressable>

                    </View>
                    <View style={{ height: hp(4) }} />
                    <View style={{ alignItems: 'flex-start', }}>
                        <Button
                            disabled={isButtonDisabled()}
                            onPress={() => onLogin()}
                            text="Login" />
                    </View>
                    <View style={styles.spacer} />
                    <View style={styles.spacer} />

                </View>
            </ScrollView>
        </View>
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>
            <Text style={styles.registerText}>If you are new / </Text>
            <Pressable onPress={() => navigation.navigate('register')}>
                <Text style={[styles.registerText, { color: Colors.primary }]}>Create new Account </Text>
            </Pressable>
        </View>
        {isLoading && <Spinner />}
    </KeyboardAvoidingView>
}

export default Login;

const styles = StyleSheet.create({
    errorText: {
        paddingVertical: 8,
        fontFamily: Constants.NiveauGroteskRegular,
        fontSize: wp(2.5),
        color: Colors.red,
    },
    container: {},
    registerText: {
        fontSize: wp(3.5),
        color: Colors.textDark,
        fontFamily: Constants.NiveauGroteskRegular,
        paddingVertical: hp(2)
    },
    tabContainer: {
        marginVertical: 15,
        flexDirection: 'row',
    },
    tabButtonSelected: {
        backgroundColor: Colors.primary
    },
    tabSelected: { color: Colors.white },
    tabText: { color: Colors.primary, fontSize: wp(3) },
    tabButton: {
        borderRadius: 6,
        elevation: 3,
        flex: 1,
        backgroundColor: Colors.white,
    },
    tabButtonInner: {
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    bgImage: {
        position: 'absolute',
        zIndex: -1,
        width: wp(100),
        height: hp(100),
        resizeMode: 'cover'
    },
    spacer: {
        height: hp(2)
    },
    spacerView: {
        height: hp(20)
    },
    title: {
        fontSize: wp(8),
        color: Colors.title,
        fontFamily: Constants.NiveauGroteskBold
    },
    image_watermark: {
        width: wp(30),
        height: hp(10),
        resizeMode: 'cover'
    },
    resetButtonText: {
        fontSize: wp(3.5),
        color: Colors.textDark,
        fontFamily: Constants.NiveauGroteskMedium
    }
});
