
import React, { useState } from 'react';
import {
    Image,
    Text,
    Dimensions,
    View,
    StyleSheet,
    Pressable,
    KeyboardAvoidingView,
    ScrollView,
    Keyboard
} from 'react-native';
import { Appbar, Button as ButtonPaper, Surface, TextInput } from 'react-native-paper';
import { Input, Button, Heading, AuthInput, Spinner } from 'components';
import * as Animated from 'react-native-animatable';

import { Colors, Constants, Utils } from 'common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import ErrorHandler from 'common/errorHandler';
import { forgotPasswordService } from 'reduxStore/services/user';
import _ from 'lodash';
// import Icon from 'react-native-vector-icons/FontAwesome5';
const { width } = Dimensions.get('window');
interface ForgotPassProps {
    navigation: any
};
let initial = {
    email: '',
    isLoading: false,
    isEmailValid: false,
}
const ForgotPass = ({ navigation }: ForgotPassProps) => {

    const [{
        email,
        isLoading,
        isEmailValid,
    }, setState] = useState(initial);

    const updateState = (state: {}) => {
        setState(prevState => ({ ...prevState, ...state }));
    }

    const isButtonDisabled = () => {
        return email == '' || !isEmailValid;
    }


    const submitForm = () => {
        if (isLoading) return;
        Keyboard.dismiss()
        updateState({ isLoading: true })
        forgotPasswordService(email)
            .then(res => {
                updateState({ isLoading: false })
                if (res.Status === 200 && !_.isEmpty(res.Data)) {
                    navigation.navigate('login')
                } else {
                    Utils.showToast('Email not found!');
                }
            })
            .catch(err => {
                let msg = ErrorHandler(err)
                Utils.showToast(msg);
                updateState({ isLoading: false })
            })

        // navigation.replace('main', { screen: 'dashboard' })
        /* updateState({ loading: true });
        setTimeout(() => {
            updateState({ loading: false });
        }, 400); */
    }

    return <KeyboardAvoidingView behavior={Constants.platform === 'ios' ? 'padding' : undefined} style={{ flex: 1, backgroundColor: Colors.white }}>
        <Image source={Constants.mainBg} style={styles.bgImage} />
        <View style={{ flex: 1 }}>
            <ScrollView>
                <View style={{ paddingHorizontal: wp(7) }}>

                    <View style={styles.spacerView} />
                    <View>
                        <Text style={styles.title}>Reset Password</Text>
                    </View>
                    <View style={styles.spacer} />
                    <View style={styles.spacer} />
                    <AuthInput
                        placeholder={`Enter Email here...`}
                        returnKeyType='next'
                        leftIcon={Constants.auth_email}
                        label={'Email/Phone Number'}
                        onChangeText={(email) => {
                            let isEmailValid = Utils.validateEmail(email);
                            updateState({ email, isEmailValid })
                        }}
                        value={email} />
                    <View style={styles.spacer} />
                    <View style={styles.spacer} />
                    <View style={{ alignItems: 'flex-start', }}>
                        <Button
                            disabled={isButtonDisabled()}
                            onPress={() => submitForm()}
                            text="Submit" />
                    </View>
                    <View style={styles.spacer} />
                    <View style={styles.spacer} />

                </View>
            </ScrollView>
        </View>
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>
            <Text style={styles.registerText}>If already have account / </Text>
            <Pressable onPress={() => navigation.navigate('login')}>
                <Text style={[styles.registerText, { color: Colors.primary }]}>Login now </Text>
            </Pressable>
        </View>
        {isLoading && <Spinner />}
    </KeyboardAvoidingView>
}

export default ForgotPass;

const styles = StyleSheet.create({
    container: {},
    registerText: {
        fontSize: wp(3.5),
        color: Colors.textDark,
        fontFamily: Constants.NiveauGroteskRegular,
        paddingVertical: hp(2)
    },
    tabContainer: {
        marginVertical: 15,
        flexDirection: 'row',
    },
    tabButtonSelected: {
        backgroundColor: Colors.primary
    },
    tabSelected: { color: Colors.white },
    tabText: { color: Colors.primary, fontSize: wp(3) },
    tabButton: {
        borderRadius: 6,
        elevation: 3,
        flex: 1,
        backgroundColor: Colors.white,
    },
    tabButtonInner: {
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    bgImage: {
        position: 'absolute',
        zIndex: -1,
        width: wp(100),
        height: hp(100),
        resizeMode: 'cover'
    },
    spacer: {
        height: hp(2)
    },
    spacerView: {
        height: hp(20)
    },
    title: {
        fontSize: wp(8),
        color: Colors.title,
        fontFamily: Constants.NiveauGroteskBold
    },
    image_watermark: {
        width: wp(30),
        height: hp(10),
        resizeMode: 'cover'
    },
    resetButtonText: {
        fontSize: wp(3.5),
        color: Colors.textDark,
        fontFamily: Constants.NiveauGroteskMedium
    }
});
