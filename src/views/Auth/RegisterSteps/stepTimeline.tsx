import * as React from 'react';
import { Text, View, StyleSheet, Pressable } from 'react-native';
import { Colors } from 'common';
import Icon from 'react-native-vector-icons/FontAwesome5'
interface StepsTimelineProps {

    currentStep: number
}

const StepsTimeline = (props: StepsTimelineProps) => {
    let {
        currentStep
    } = props
    return (
        <View style={styles.container}>
            <Pressable style={[styles.stepContainer, styles.stepContainerDone]}>
                {currentStep > 1 ? <Icon name='check' color={Colors.white} /> : <Text style={[styles.stepText, styles.stepTextDone]}>1</Text>}
            </Pressable>
            <View style={[styles.stepSeperator, currentStep > 1 && { borderColor: Colors.primary }]} />
            <Pressable style={[styles.stepContainer, currentStep >= 2 && styles.stepContainerDone]}>
                {currentStep > 2 ? <Icon name='check' color={Colors.white} /> : <Text style={[styles.stepText, currentStep >= 2 && styles.stepTextDone]}>2</Text>}
            </Pressable>
            <View style={[styles.stepSeperator, currentStep > 2 && { borderColor: Colors.primary }]} />
            <Pressable style={[styles.stepContainer, currentStep == 3 && styles.stepContainerDone]}>
                <Text style={[styles.stepText, currentStep == 3 && styles.stepTextDone]}>3</Text>
            </Pressable>
        </View>
    );
};

StepsTimeline.defaultProps = {
    currentStep: 1,
}

export default StepsTimeline;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    stepSeperator: {
        borderStyle: 'dashed',
        borderWidth: 1,
        flex: 1,
        height: 1,
        borderRadius: 1,
        marginHorizontal: 10,
        borderColor: Colors.borderColor
    },
    stepContainerDone: {
        backgroundColor: Colors.primary
    },
    stepTextDone: {
        color: Colors.white
    },
    stepContainer: {
        width: 35,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5
    },
    stepText: {
        color: Colors.primary

    },
});
