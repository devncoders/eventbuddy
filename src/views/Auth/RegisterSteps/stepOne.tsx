import * as React from 'react';
import { Text, View, StyleSheet, Pressable } from 'react-native';
import { Colors, Constants } from 'common';
import { DateSelector, Input, Label } from 'components';

interface StepOneProps {
    updateState: ({ }) => void;
    selected: number
    dateOfBirth: string
    fullname: string
}

const StepOne = (props: StepOneProps) => {
    let {
        updateState,
        selected,
        dateOfBirth,
        fullname,
    } = props;
    return (
        <>
            <Input returnKeyType='next' lableEng='Full Name'
                lableUrdu='پورا نام'
                placeholder='Enter full name' onChangeText={(fullname) => updateState({ fullname })} value={fullname} />
            <View style={{ height: 10 }} />
            <View>
                <Label textEng={'Gender'} textUrdu={'صنف'} />
                <View style={styles.tabContainer}>
                    <View style={{ flexDirection: 'row', borderRadius: Constants.borderRadius, overflow: 'hidden' }}>
                        <Pressable onPress={() => updateState({ selected: 1 })} style={[styles.tabButton, selected == 1 && styles.tabButtonSelected]}>
                            <Text style={[styles.tabText, selected == 1 && styles.tabSelected]}>MALE</Text>
                        </Pressable>
                        <View style={styles.tabSeparator} />
                        <Pressable onPress={() => updateState({ selected: 2 })} style={[styles.tabButton, selected == 2 && styles.tabButtonSelected]}>
                            <Text style={[styles.tabText, selected == 2 && styles.tabSelected]}>FEMALE</Text>
                        </Pressable>
                        <View style={styles.tabSeparator} />
                        <Pressable onPress={() => updateState({ selected: 3 })} style={[styles.tabButton, selected == 3 && styles.tabButtonSelected]}>
                            <Text style={[styles.tabText, selected == 3 && styles.tabSelected]}>OTHER</Text>
                        </Pressable>
                    </View>
                </View>
            </View>
            <View style={{ height: 10 }} />
            <DateSelector
                dateOfBirth={dateOfBirth}
                lableEng={'Date of Birth'}
                lableUrdu={'پیدائش کی تاریخ'}
            />
        </>
    );
};

export default StepOne;

const styles = StyleSheet.create({
    tabSeparator: {
        width: 1,
        backgroundColor: Colors.borderColor
    },
    tabContainer: {
        borderRadius: Constants.borderRadius,
        borderWidth: 1,
        borderColor: Colors.borderColor,
        marginVertical: 15,
        backgroundColor: Colors.white,
    },
    tabButtonSelected: {
        backgroundColor: Colors.primary
    },
    tabSelected: { color: Colors.white },
    tabText: { color: Colors.primary },
    tabButton: {
        paddingVertical: 10,
        backgroundColor: Colors.white,
        flex: 1,
        alignItems: 'center',
    }
});
