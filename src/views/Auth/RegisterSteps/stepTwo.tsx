import * as React from 'react';
import { Text, View, StyleSheet, Pressable } from 'react-native';
import { Colors, Constants } from 'common';
import { DateSelector, Input, Label } from 'components';

interface StepTwoProps {
    updateState: ({ }) => void;
    email: string
    password: string
    confirmPassword: string
}

const StepTwo = (props: StepTwoProps) => {
    let {
        updateState,
        email,
        password,
        confirmPassword,
    } = props;
    return (
        <>
            <Input returnKeyType='next' lableUrdu='ای میل' lableEng='Email Address' placeholder='Enter Email Address here...' onChangeText={(email) => updateState({ email })} value={email} />
            <View style={{ height: 10 }} />
            <Input returnKeyType='next' lableUrdu='پاس ورڈ' isPassword lableEng='Password' placeholder='Enter Password here...' onChangeText={(password) => updateState({ password })} value={password} />
            <View style={{ height: 10 }} />
            <Input returnKeyType='next' lableUrdu='پاس ورڈ' isPassword lableEng='Confirm Password' placeholder='Enter Password here...' onChangeText={(confirmPassword) => updateState({ confirmPassword })} value={confirmPassword} />
        </>
    );
};

export default StepTwo;