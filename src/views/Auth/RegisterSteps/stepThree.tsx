import * as React from 'react';
import { Text, View, StyleSheet, Pressable } from 'react-native';
import { Colors, Constants } from 'common';
import { DateSelector, Input, InputSelector, Label } from 'components';

interface StepThreeProps {
    updateState: ({ }) => void;
    cnic: string
    tehsil: string
    district: string
}

const StepThree = (props: StepThreeProps) => {
    let {
        updateState,
        cnic,
        tehsil,
        district,
    } = props;
    return (
        <>
            <Input returnKeyType='done' lableEng='CNIC #' placeholder='Enter CNIC Number here...' keyboardType='number-pad' onChangeText={(cnic) => updateState({ cnic })} value={cnic} />
            <View style={{ height: 10 }} />
            <InputSelector lableEng='District' lableUrdu='ضلع' placeholder='Select District' onValueChange={(val) => updateState({ district: val })} value={district} />
            <View style={{ height: 10 }} />
            <InputSelector lableEng='Tehsil' lableUrdu='تحصیل' placeholder='Select Tehsil' onValueChange={(val) => updateState({ tehsil: val })} value={tehsil} />
        </>
    );
};

export default StepThree;