
import React, { useState } from 'react';
import {
    Pressable,
    Text,
    Dimensions,
    View,
    StyleSheet,
    KeyboardAvoidingView
} from 'react-native';
import { Appbar, Button } from 'react-native-paper';

let initial = {
    currentStep: 1,
    fullname: '',
    loading: false,
    showLoginView: false,
    selected: 0,
    dateOfBirth: '',
    email: '',
    password: '',
    confirmPassword: '',
}
import { Constants, Colors } from 'common';
import { Header } from 'components';
import { ScrollView } from 'react-native-gesture-handler';
import StepOne from './stepOne';
import StepsTimeline from './stepTimeline';
import StepTwo from './stepTwo';
import StepThree from './stepThree';

const { width } = Dimensions.get('window');
interface LoginProps { };

const RegisterSteps = ({ navigation }) => {

    const [{
        currentStep,
        dateOfBirth,
        selected,
        fullname,
        loading,
        email,
        password,
        confirmPassword,
    }, setState] = useState(initial);

    const updateState = (state: {}) => {
        setState(prevState => ({ ...prevState, ...state }));
    }

    const submitForm = () => {
        if (loading) return;
        updateState({ loading: true });
        setTimeout(() => {
            updateState({ loading: false });
            navigation.navigate('otp')
        }, 400);
    }

    return <KeyboardAvoidingView behavior={Constants.platform == 'ios' ? 'padding' : undefined} style={{ flex: 1, backgroundColor: 'white' }}>
        <Header title={Constants.title.register} />
        <View style={{ padding: 25, flex: 1 }}>
            <View style={{ flex: 1 }}>
                <ScrollView bounces={false}>
                    <Text style={{ color: Colors.textGrey, textAlign: 'center' }}><Text style={{ fontWeight: '600' }}>Create</Text> {'\n'} your account in few simple steps</Text>
                    <View style={{ height: 20 }} />
                    <StepsTimeline
                        currentStep={currentStep}
                    />
                    <View style={{ height: 20 }} />
                    {currentStep == 1 && <StepOne
                        fullname={fullname}
                        dateOfBirth={dateOfBirth}
                        updateState={(obj) => updateState(obj)}
                        selected={selected}
                    />}
                    {currentStep == 2 && <StepTwo
                        email={email}
                        password={password}
                        confirmPassword={confirmPassword}
                        updateState={(obj) => updateState(obj)}
                    />}
                    {currentStep == 3 && <StepThree
                        email={email}
                        password={password}
                        confirmPassword={confirmPassword}
                        updateState={(obj) => updateState(obj)}
                    />}
                    <View style={{ height: 20 }} />
                </ScrollView>
            </View>
            <Button
                mode='contained' loading={loading}
                contentStyle={{ paddingVertical: 5 }}
                onPress={() => currentStep == 3 ? submitForm() : updateState({ currentStep: currentStep + 1 })}>
                Next
            </Button>
            {currentStep != 1 && <>
                <View style={{ height: 5 }} /><Button
                    contentStyle={{ paddingVertical: 5 }}
                    onPress={() => {
                        updateState({ currentStep: currentStep - 1 })
                    }}>
                    Back
                </Button></>}
            <View style={{ height: 10 }} />
        </View>
    </KeyboardAvoidingView>
}

export default RegisterSteps;

const styles = StyleSheet.create({
    container: {},
    registerText: {
        color: Colors.textGrey
    },
});
