
import React, { useState } from 'react';
import {
    Image,
    Text,
    Dimensions,
    View,
    StyleSheet,
    Pressable,
    KeyboardAvoidingView,
    ScrollView,
    AsyncStorage
} from 'react-native';
import { Appbar, Button as ButtonPaper, Surface, TextInput } from 'react-native-paper';
import { Input, Button, Heading, AuthInput, Spinner } from 'components';
import * as Animated from 'react-native-animatable';

import { Colors, Constants, Utils } from 'common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { signUpUserService } from 'reduxStore/services/user';
import ErrorHandler from 'common/errorHandler';
import _ from 'lodash';
import { useNavigation } from '@react-navigation/native';
import { UserModel } from 'reduxStore/models';
import { updateUser, updateUserToken } from 'reduxStore/actions/auth';
import { useDispatch } from 'react-redux';
// import Icon from 'react-native-vector-icons/FontAwesome5';
const { width } = Dimensions.get('window');
interface RegisterProps {
    navigation: any
};
let initial = {
    name: '',
    isSubmitted: false,
    email: '',
    password: '',
    confirm_password: '',
    isLoading: false,
    isEmailValid: false,
    selected: 1
}
const Register = (props: RegisterProps) => {
    let navigation = useNavigation();
    const [{
        isSubmitted,
        name,
        email,
        password,
        confirm_password,
        isLoading,
        isEmailValid
    }, setState] = useState(initial);
    const dispatch = useDispatch();
    const updateState = (state: {}) => {
        setState(prevState => ({ ...prevState, ...state }));
    }

    const isButtonDisabled = () => {
        return name == '' ||
            email == '' ||
            !isEmailValid ||
            password === '' ||
            password !== confirm_password
    }

    const submitForm = () => {
        if (isLoading) return;
        setState(prevState => ({ ...prevState, isSubmitted: true }));

        updateState({ isLoading: true });
        let data = {
            Email: email,
            UserName: email,
            FullName: name,
            Password: password
        }

        try {
            signUpUserService(data)
                .then(res => {
                    if (res.Status === 200 && !_.isEmpty(res.Data)) {
                        updateState({ isLoading: false });
                        let {
                            User,
                            Token
                        } = res.Data;
                        let user: UserModel = User;
                        dispatch(updateUser(user));
                        dispatch(updateUserToken(Token));
                        AsyncStorage.setItem('userData', JSON.stringify({
                            ...user,
                            token: Token
                        }));
                        navigation.reset({
                            index: 0,
                            routes: [
                                {
                                    name: 'uploadPicture', params: { userData: res.Data }
                                }
                            ]
                        });
                    } else {
                        updateState({ isLoading: false })
                        Utils.showToast(res.Message);
                    }
                })
                .catch(err => {
                    let msg = ErrorHandler(err)
                    Utils.showToast(msg);
                    updateState({ isLoading: false })
                })
        } catch (error) {

            updateState({ isLoading: false })
        }

        // navigation.navigate('login')
        // navigation.replace('main', { screen: 'dashboard' })
        /* 
        setTimeout(() => {
            updateState({ loading: false });
        }, 400); */
    }

    return <KeyboardAvoidingView behavior={Constants.platform === 'ios' ? 'padding' : undefined} style={{ flex: 1, backgroundColor: Colors.white }}>
        <Image source={Constants.mainBg} style={styles.bgImage} />
        <View style={{ flex: 1 }}>
            <ScrollView>
                <View style={{ paddingHorizontal: wp(7) }}>

                    <View style={styles.spacerView} />
                    <View>
                        <Text style={styles.title}>Welcome, {'\n'}Signup Now</Text>
                    </View>
                    <View style={styles.spacer} />
                    <View style={styles.spacer} />
                    <AuthInput
                        placeholder={`First Name`}
                        returnKeyType='next'
                        leftIcon={Constants.auth_user}
                        label={'First Name'}
                        onChangeText={(name) => updateState({ name })}
                        value={name} />
                    <View style={styles.spacer} />
                    <AuthInput
                        placeholder={`Enter Email here...`}
                        returnKeyType='next'
                        leftIcon={Constants.auth_email}
                        keyboardType='email-address'
                        label={'Email'}
                        onChangeText={(email) => {
                            let isEmailValid = Utils.validateEmail(email);
                            updateState({ email, isEmailValid })
                        }}
                        value={email} />
                    {email !== '' && !isEmailValid && <Text style={styles.errorText}>Invalid Email address</Text>}
                    <View style={styles.spacer} />
                    <AuthInput
                        placeholder='Password'
                        label={'Password'}

                        leftIcon={Constants.auth_password}
                        onChangeText={(password) => updateState({ password })}
                        isPassword
                        value={password} />
                    <View style={styles.spacer} />
                    <AuthInput
                        placeholder='Re-type Password'
                        label={'Password'}
                        leftIcon={Constants.auth_password}
                        onChangeText={(confirm_password) => updateState({ confirm_password })}
                        isPassword
                        value={confirm_password} />
                    {password !== confirm_password && <Text style={styles.errorText}>Confirm Password Does not match</Text>}
                    <View style={{ height: hp(4) }} />
                    {<Text style={styles.errorText}>*All fields are mandatory</Text>}
                    <View style={{ alignItems: 'flex-start', }}>
                        <Button
                            disabled={isButtonDisabled()}
                            onPress={() => submitForm()}
                            text="Signup" />
                    </View>
                    <View style={styles.spacer} />

                </View>
            </ScrollView>
        </View>
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>
            <Text style={styles.registerText}>If already have account / </Text>
            <Pressable onPress={() => navigation.navigate('login')}>
                <Text style={[styles.registerText, { color: Colors.primary }]}>Login now </Text>
            </Pressable>
        </View>
        {isLoading && <Spinner />}
    </KeyboardAvoidingView>
}

export default Register;

const styles = StyleSheet.create({
    errorText: {
        paddingVertical: 8,
        fontFamily: Constants.NiveauGroteskRegular,
        fontSize: wp(2.5),
        color: Colors.red,
    },
    container: {},
    registerText: {
        fontSize: wp(3.5),
        color: Colors.textDark,
        fontFamily: Constants.NiveauGroteskRegular,
        paddingVertical: hp(2)
    },
    tabContainer: {
        marginVertical: 15,
        flexDirection: 'row',
    },
    tabButtonSelected: {
        backgroundColor: Colors.primary
    },
    tabSelected: { color: Colors.white },
    tabText: { color: Colors.primary, fontSize: wp(3) },
    tabButton: {
        borderRadius: 6,
        elevation: 3,
        flex: 1,
        backgroundColor: Colors.white,
    },
    tabButtonInner: {
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    bgImage: {
        position: 'absolute',
        zIndex: -1,
        width: wp(100),
        height: hp(100),
        resizeMode: 'cover'
    },
    spacer: {
        height: hp(2)
    },
    spacerView: {
        height: hp(20)
    },
    title: {
        fontSize: wp(8),
        color: Colors.title,
        fontFamily: Constants.NiveauGroteskBold
    },
    image_watermark: {
        width: wp(30),
        height: hp(10),
        resizeMode: 'cover'
    },
    resetButtonText: {
        fontSize: wp(3.5),
        color: Colors.textDark,
        fontFamily: Constants.NiveauGroteskMedium
    }
});
