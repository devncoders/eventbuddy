
import React, { useState, useEffect } from 'react';
import {
    TextInput,
    Text,
    Pressable,
    View,
    StyleSheet,
    TextInputProps
} from 'react-native';
import { Appbar, Button } from 'react-native-paper';

import * as Animated from 'react-native-animatable';
// import { useSelector, useDispatch } from 'react-redux';
import { NavigationScreenProp, NavigationState } from 'react-navigation';

import { Constants, Colors, Utils } from '../../../common';
import moment from 'moment';
import { CommonActions } from '@react-navigation/native';
import { Header } from 'components';
interface OtpProps {
    navigation: NavigationScreenProp<NavigationState>;
    route: any
}

interface OTPInputProps extends TextInputProps {
    ref?: any
}

const OTPInput = React.forwardRef((props: OTPInputProps, ref) => {
    const [height, setHeight] = useState(0);
    return <View
        onLayout={(e) => setHeight(e.nativeEvent.layout.width)}
        style={{ height: height + 10, borderRadius: 7, flex: 1, ...Constants.boxShadow, backgroundColor: Colors.white }}>
        <TextInput
            {...props}
            ref={ref}
            placeholder={'-'}
            placeholderTextColor={Colors.textDark}
            maxLength={1}
            style={{
                fontSize: 30,
                color: Colors.textDark,
                height: '100%', flex: 1, textAlign: 'center'
            }}
        />
    </View>
})

let initialState = {
    c1: '',
    c2: '',
    c3: '',
    c4: '',
    c5: '',
    c6: '',
    isLoading: false,
    currentTime: '',
    timer: '30',
    hideTimer: true,
    resendLoad: false
}


const OTP = (props: any) => {
    const [{
        c1,
        c2,
        c3,
        c4,
        c5,
        c6,
        timer,
        hideTimer,
        isLoading,
        resendLoad,
        currentTime
    }, setState] = useState(initialState)
    const [isSecure, setSecure] = useState(true);
    // const dispatch = useDispatch();

    const updateState = (newState: any) => {
        setState(prevState => ({ ...prevState, ...newState }))
    }

    const otp2Ref = React.createRef<TextInput>();
    const otp3Ref = React.createRef<TextInput>();
    const otp4Ref = React.createRef<TextInput>();
    const otp5Ref = React.createRef<TextInput>();

    /**
     * 
     * 
     * {
            name,
            cogname,
            email,
            phone,
            address,
            province,
            city,
            postalCode,
            websiteLink,
        }
     */
    let regData = props.route.params?.data || {}
    let resendInterval: any;

    const startTimer = () => {
        clearInterval(resendInterval)
        updateState({ hideTimer: false });
        let endTime = moment().add(30, 'second');
        // updateState({ timer: endTime });
        resendInterval = setInterval(() => {
            let _timer = moment();
            var duration = moment.duration(endTime.diff(_timer));
            var newTime = Math.round(duration.asSeconds()) < 10 ? '0' + Math.round(duration.asSeconds()) : Math.round(duration.asSeconds())
            updateState({ timer: newTime });
            if (duration.asSeconds() <= 0) {
                updateState({ hideTimer: true, timer: null });
                clearInterval(resendInterval)
            }
        }, 1000)
    }

    useEffect(() => {
        updateState({ timer: '00', hideTimer: true })
        return (() => {
            if (resendInterval) {
                clearInterval(resendInterval)
            }
        })
    }, []);


    const verifyOtp = () => {
        let code = c1 + c2 + c3 + c4 + c5;
        let resetAction = CommonActions.reset({
            index: 0,
            routes: [
                { name: 'login' }
            ],
        })
        if (isLoading) return;
        updateState({ isLoading: true });
        setTimeout(() => {
            Utils.showToast('OTP verified successfully!')
            updateState({ isLoading: false });
            props.navigation.dispatch(resetAction);
        }, 400);
        /* verifyOTP(regData.phone_number, code)
            .then(res => {
                console.log(res);
                updateState({ isLoading: false })
                Utils.showToast('OTP Verified successfully!');
                props.navigation.navigate('createpass', { regData })
            })
            .catch(err => {
                console.log(err.errors?.message[0]);
                let msg = err.errors?.message[0] || 'User with this phone number not found'
                updateState({ isLoading: false })
                Utils.showToast(msg);
            }) */
    }

    const onPressResend = () => {
        updateState({ ...initialState, resendLoad: true });
        updateState({ isLoading: false })
        startTimer();
        /* resendOtp(regData.phone_number)
            .then(res => {
                updateState({ isLoading: false })
                startTimer();
                console.log(res);
                Utils.showToast('Verification code sent successfully!');
            })
            .catch(err => {
                updateState({ hideTimer: true, isLoading: false })
                let msg = err.errors?.message[0] || 'User with this phone number not found'
                Utils.showToast(msg);
            }); */
    }

    const isDisabled = () => {
        return c1 == '' || c2 == '' || c3 == '' || c4 == '' || c5 == ''
    };


    return <View style={{ flex: 1, backgroundColor: 'white' }}>
        <Header title={Constants.title.main} />
        <View style={{ padding: 25, }}>
            <Animated.View animation='fadeInUp'>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', }}>

                    <Text style={styles.title}>Verify OTP</Text>
                    <Text style={styles.title}>او ٹی پی کی تصدیق کریں</Text>
                </View>
                <Text style={styles.description}>We have sent an OTP at your email address/phone Number,
                    please enter the code here:</Text>
                <View style={styles.separator} />
                <View style={styles.separator} />
                <View style={{ flexDirection: 'row' }}>
                    <OTPInput
                        returnKeyType='next'
                        onSubmitEditing={() => {
                            otp2Ref?.current?.focus()
                        }}
                        maxLength={1} value={c1} onChangeText={(c1) => {
                            if (c1.length > 0) otp2Ref?.current?.focus()
                            updateState({ c1 })
                        }} />
                    <View style={{ flex: .2 }} />
                    <OTPInput
                        returnKeyType='next'
                        onSubmitEditing={() => {
                            otp3Ref?.current?.focus()
                        }}
                        ref={otp2Ref} maxLength={1} value={c2} onChangeText={(c2) => {
                            if (c2.length > 0) otp3Ref?.current?.focus()
                            updateState({ c2 })
                        }} />
                    <View style={{ flex: .2 }} />
                    <OTPInput
                        returnKeyType='next'
                        onSubmitEditing={() => {
                            otp4Ref?.current?.focus()
                        }}
                        ref={otp3Ref} maxLength={1} value={c3} onChangeText={(c3) => {
                            if (c3.length > 0) otp4Ref?.current?.focus()
                            updateState({ c3 })
                        }} />
                    <View style={{ flex: .2 }} />
                    <OTPInput
                        returnKeyType='next'
                        onSubmitEditing={() => {
                            otp5Ref?.current?.focus()
                        }}
                        ref={otp4Ref} maxLength={1} value={c4} onChangeText={(c4) => {
                            if (c4.length > 0) otp5Ref?.current?.focus()
                            updateState({ c4 })
                        }} />
                    <View style={{ flex: .2 }} />
                    <OTPInput
                        returnKeyType='next'
                        onSubmitEditing={() => {
                            otp5Ref?.current?.blur()
                        }}
                        ref={otp5Ref} maxLength={1} value={c5} onChangeText={(c5) => {
                            if (c5.length > 0) otp5Ref?.current?.blur()
                            updateState({ c5 })
                        }} />
                    <View style={{ flex: .2 }} />
                </View>
                <View style={{ height: 50 }} />
                <View
                    style={[styles.buttonOption]}>
                    <Text style={[styles.buttonText, { marginRight: 0 }]}>
                        Didn't receive otp yet?</Text>
                    {!hideTimer ? <Text style={styles.timerText}>00:{timer}</Text> : <Pressable
                        disabled={!hideTimer}
                        style={!hideTimer ? { opacity: .3 } : null}
                        onPress={() => onPressResend()}>
                        <Text style={{
                            textDecorationLine: 'underline',
                            color: Colors.primary
                        }}>Resend</Text>
                    </Pressable>}
                </View>
                <View style={{ height: 50 }} />
                <Button mode='contained' loading={isLoading}
                    contentStyle={{ paddingVertical: 5 }}
                    /* disabled={isDisabled()}
                    style={isDisabled() && { opacity: .3 }} */
                    onPress={() => verifyOtp()}>
                    Verify
                </Button>
                <View style={{ height: 20 }} />
                <View style={{ height: 20 }} />
            </Animated.View>
        </View>
    </View>
}

export default OTP;

const styles = StyleSheet.create({
    title: {
        fontSize: 20,
        paddingVertical: 10,
        color: Colors.primary
    },
    description: {
        fontSize: 13,
        color: Colors.textGrey
    },
    buttonOption: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 45,
        // flexDirection: 'row'
    },
    buttonText: {
        fontSize: 15,
        color: Colors.textGrey,
        marginRight: 20
    },
    timerText: {
        fontSize: 20,
        color: Colors.primary,
    },
    separator: {
        height: 25
    }
});
