
import React, { useEffect, useRef, useState } from 'react';
import {
    Image,
    TextInput,
    View,
    Dimensions,
    StyleSheet,
    KeyboardAvoidingView,
    Pressable,
    ToastAndroid,
    Alert,
    FlatList,
    SectionList,
    Modal,
    Keyboard
} from 'react-native';
import { Appbar, Avatar, Text, Button, Card, Subheading, Title } from 'react-native-paper';
import * as Animated from 'react-native-animatable';
import { Colors, Constants, Utils } from 'common';

import { ActionSheet, Header, Spinner } from 'components';
import MessageListItem from './MessageItem';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/FontAwesome5';
import moment from 'moment';
import EventDetailView from './EventDetailView';
import { createChat, getMessageDetails } from 'reduxStore/services/messages';
import { RouteProp, useNavigation } from '@react-navigation/native';
import _ from 'lodash';
import { getInvitationById } from 'reduxStore/services/invitations';
import { InvitationItemModel } from 'reduxStore/models/invitation';
import { UsersItemModel } from 'reduxStore/models/users';
import { MessageItem, MessageItemModel } from 'reduxStore/models/message';
import { uploadMedia } from 'reduxStore/services/user';
import { Socket, SocketTypes } from 'common/socketUtils';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'reduxStore/reducers';
import { UserModel } from 'reduxStore/models';
import ImageView from "react-native-image-viewing";
import WriteReviewCard from 'views/Reviews/WriteReviewCard';
import { Bubble, GiftedChat } from 'react-native-gifted-chat'
interface MessageModel {
    title: string,
    data: Array<MessageItem>
}
let initial = {
    otherUser: {} as UserModel,
    title: '',
    messageList: [] as Array<MessageModel>,
    allMessages: [] as Array<MessageItem>,
    message: '',
    newMessage: {},
    isLoading: true,
    loadingImage: false,
    showKeyboard: false,
    showCard: false,
    showCameraOptions: false,
    showWriteReview: false,
    invitationDetails: {} as InvitationItemModel
}

let chatMessage = {
    "Id": "",
    "CreatedAt": "",
    "Message": "",
    "InvitationId": 0,
    "CreatedById": 0,
}

interface MessageDetailProps {
    route: RouteProp<{
        params: {
            eventId: number,
            user: UsersItemModel,
            messageItem: MessageItemModel
        }
    }, 'params'>
}
const MessageDetail = (props: MessageDetailProps) => {
    let listRef = useRef<SectionList>(undefined);
    let chatUser = props?.route?.params?.user || {};
    let currentUser = useSelector((state: RootState) => state.main.userData);
    if (props.route.params.messageItem) {
        let message_item = props.route.params.messageItem;
        chatUser = message_item.UserId === currentUser.Id ? message_item.CreatedBy : message_item.User
    }
    let navigation = useNavigation();
    let invitationId = props?.route?.params?.eventId; //InvitationID
    const [{
        otherUser,
        newMessage,
        allMessages,
        loadingImage,
        isLoading,
        invitationDetails,
        messageList,
        message,
        showKeyboard,
        showCard,
        showCameraOptions,
        showWriteReview
    }, setState] = useState(initial);

    const [showImageView, setImageViewState] = React.useState(false);
    const [imageUriMessage, setImageImageUri] = React.useState('');
    const images = [
        {
            uri: imageUriMessage,
        }
    ];

    const updateState = (state: {}) => {
        setState(prevState => ({ ...prevState, ...state }));
    }

    const getInvitationItemDetails = () => {

        return new Promise((resolve, reject) => {
            getInvitationById(invitationId).then(res => {
                if (res.Status === 200 && !_.isEmpty(res.Data)) {
                    let invitationDetails = res.Data || {}
                    let otherUser = res.Data.User;
                    console.log('invitationDetails == = > ', invitationDetails);

                    if (res.Data.CreatedById !== currentUser.Id) {
                        otherUser = res.Data.CreatedBy
                    }
                    chatUser = otherUser
                    updateState({
                        otherUser,
                        invitationDetails
                    });
                    return resolve(invitationDetails)
                } else {
                    return reject('error')
                }
            }).catch(err => {
                updateState({
                    invitationDetails: {}
                });
                return reject('error')
            })
        });
    }

    const getMessageDataList = (data: Array<MessageItem>) => {
        let res: Array<MessageModel> = data.reduce((re: any, o: MessageItem) => {
            let existObj: MessageModel = re.find(
                (obj: any) => obj.title === moment(o?.CreatedAt).format('MMMM DD'),
            );

            if (existObj) {
                existObj?.data.push(o);
            } else {
                re.push({
                    title: moment(o.CreatedAt).format('MMMM DD'),
                    data: [o],
                });
            }
            return re;
        }, []);
        return res;
    };

    useEffect(() => {
        if (listRef && messageList.length > 0) {
            let length = messageList.length;
        }
    }, [messageList])

    const _getMessagesById = () => {
        return new Promise((resolve, reject) => {
            getMessageDetails(invitationId).then(res => {
                if (res.Status === 200 && !_.isEmpty(res.Data)) {
                    return resolve(res.Data)
                } else {
                    return reject('error')

                }
            }).catch(err => {
                /* updateState({
                    isLoading: false
                }) */
                return reject('success')
            })

        })
    }
    const onMessageSend = () => {

        Socket.emitMessage(
            {
                InvitationId: invitationId,
                Content: message
            },
            async (data: any) => {
                console.log('data');

            }
        );

        let chatMessage = {
            "_id": Math.random(),
            "createdAt": moment(),
            "text": message,
            user: {
                _id: currentUser.Id,
                name: currentUser.FullName,
                avatar: currentUser.Media.BasePath + currentUser.Media.ThumbPath,
            },
        }
        let _allMessages: Array<any> = [...allMessages, chatMessage];
        updateState({
            allMessages: _allMessages,
            newMessage: {},
            message: ''
        })
    }

    const sendMediaFile = ({
        type,
        name,
        uri
    }: {
        type: string,
        name: string,
        uri: string
    }) => {
        const body = new FormData();
        updateState({ loadingImage: true });
        body.append('file',
            {
                uri,
                name,
                type
            });
        uploadMedia(body).then(res => {
            if (res.Status === 200 && !_.isEmpty(res.Data)) {
                Socket.emitMessage(
                    {
                        InvitationId: invitationId,
                        MediaId: res.Data.Id
                    },
                    async (data: any) => {
                        console.log('data');

                    }
                );
                let chatMessage = {
                    "_id": Math.random(),
                    "createdAt": moment(),
                    image: uri,
                    user: {
                        _id: currentUser.Id,
                        name: currentUser.FullName,
                        avatar: currentUser.Media.BasePath + currentUser.Media.ThumbPath,
                    },
                }
                let _allMessages: Array<any> = [...allMessages, chatMessage];
                updateState({
                    allMessages: _allMessages,
                    newMessage: {},
                    message: ''
                });
            } else {
                Utils.showToast(res.Message);
                updateState({ isLoading: false, loadingImage: false, });
            }
        }).catch(err => {
            Utils.showToast(err.Message);
            updateState({ isLoading: false, loadingImage: false, });
        })
    }

    const _fetchDetails = () => {
        Promise.all([_getMessagesById(), getInvitationItemDetails()])
            .then(res => {
                // let messages = getMessageDataList(res[0] as Array<MessageItem>);
                let _allMessages = res[0] as Array<MessageItem>;
                let allMessages = _allMessages.map((i) => {
                    return {
                        _id: i.Id,
                        image: i.MediaId && i.Media.BasePath + i.Media.ThumbPath,
                        text: i.Message,
                        createdAt: i.CreatedAt,
                        user: {
                            _id: i.CreatedBy.Id,
                            name: i.CreatedBy.FullName,
                            avatar: i.CreatedBy.Media.BasePath + i.CreatedBy.Media.ThumbPath,
                        },
                    }
                });
                updateState({
                    // allMessages: res[0],
                    allMessages,
                    isLoading: false,
                    // messageList: messages
                });
                setTimeout(() => {
                    // scrollToBottom(messages);
                }, 500);
            }).catch(err => {
                updateState({
                    isLoading: false
                })
            })
    }

    const scrollToBottom = (messages: any) => {
        let length = messages.length;

        setTimeout(() => {
            listRef && listRef.current.scrollToLocation({
                animated: true,
                sectionIndex: length - 1,
                itemIndex: messages[length - 1].data.length - 1,
                viewPosition: 0
            });
        }, 1000)
    }

    const dispatch = useDispatch();
    useEffect(() => {
        if (!_.isEmpty(newMessage)) {
            let chatMessage = {
                "_id": Math.random(),
                "createdAt": moment(),
                image: newMessage.MediaId && newMessage.Media.BasePath + newMessage.Media.ThumbPath,
                text: newMessage.Message,
                user: {
                    _id: newMessage.CreatedById,
                    name: otherUser.FullName,
                    avatar: otherUser?.Media?.BasePath + otherUser.Media.ThumbPath,
                },
            }
            let _allMessages = [...allMessages, chatMessage];
            updateState({
                allMessages: _allMessages,
                newMessage: {}
            });
        }
    }, [newMessage])
    const addMessageToList = (Chat: any) => {
        updateState({
            newMessage: Chat
        });
    }

    const handleMessageRecieved = (data: any) => {
        // if (!data.MediaId) {
        addMessageToList(data.Chat);
        //message received
        // }
    }
    useEffect(() => {
        Socket.init(); //initiate Socket
        _fetchDetails();

        Socket.onMessageRecieved(handleMessageRecieved);
        return () => {
            Socket.remove(SocketTypes.MESSAGE, handleMessageRecieved);
            Socket.disconnect();
        };
    }, []);

    const getRightIcon = () => {
        let firstChatItem = allMessages[0] || [];
        if (!_.isEmpty(firstChatItem)) {
            let hours = moment().diff(moment(firstChatItem.createdAt), 'hours');//actual
            let minutes = moment().diff(moment(firstChatItem.createdAt), 'minutes');//for testing
            if (minutes >= 2) {
                return Constants.edit
            }
            return null
        }
        return null
    }

    const renderBubble = props => {
        return (
            <Bubble
                {...props}
                textStyle={{
                    left: {
                        color: '#42526EB2',
                        fontFamily: Constants.NiveauGroteskRegular,
                        fontSize: wp(3.5)
                    },
                    right: {
                        color: '#42526EB2',
                        fontFamily: Constants.NiveauGroteskRegular,
                        fontSize: wp(3.5)
                    },
                }}
                timeTextStyle={{
                    left: {
                        color: '#aaa',
                        fontFamily: Constants.NiveauGroteskMedium,
                        fontSize: wp(2.5),
                    },
                    right: {
                        color: '#aaa',
                        fontFamily: Constants.NiveauGroteskMedium,
                        fontSize: wp(2.5),
                    },
                }}
                wrapperStyle={{
                    left: {
                        marginVertical: 4,
                        backgroundColor: Colors.primaryLight,
                    },
                    right: {
                        marginVertical: 4,
                        backgroundColor: '#F2F2F2',
                    },
                }}
            />
        );
    }

    return <KeyboardAvoidingView behavior={Constants.platform == 'ios' ? 'padding' : undefined} style={{ flex: 1, backgroundColor: 'white' }}>
        <Header
            rightIcon={getRightIcon()}
            onRightPress={() => updateState({ showWriteReview: true })}
            title={chatUser.FullName} />

        <View style={{ flex: 1 }}>
            {!_.isEmpty(invitationDetails) && !isLoading && <Pressable
                onPress={() => updateState({ showCard: true })}
                style={styles.eventDetailContainer}>
                <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                    <Image source={Constants.setting_3} style={{ width: 18, height: 18, resizeMode: 'contain' }} />
                    <Text style={styles.dateTitle}>{moment(invitationDetails.Time).format('DD MMMM, YYYY')}</Text>
                </View>
                <Text style={styles.eventTitle}>{invitationDetails.Title}</Text>
                <Text style={styles.eventDescription}>{invitationDetails.Description}</Text>
            </Pressable>}
            <View style={{ flex: 1 }}>
                <GiftedChat
                    inverted={false}
                    messages={allMessages}
                    minInputToolbarHeight={0}
                    renderInputToolbar={() => null}
                    // renderInputToolbar={() => <View style={styles.messageInputContainer}>
                    //     <View style={{ flex: 1 }}>
                    //         <TextInput
                    //             placeholderTextColor={Colors.placeholderColor}
                    //             onFocus={() => updateState({ showKeyboard: true })}
                    //             onBlur={() => updateState({ showKeyboard: false })}
                    //             autoCorrect={false}
                    //             value={message}
                    //             onChangeText={(message) => updateState({ message })}
                    //             placeholder={'Enter Message here...'}
                    //             style={styles.messageInput} />
                    //     </View>
                    //     <Pressable onPress={() => {
                    //         Keyboard.dismiss();
                    //         updateState({ showCameraOptions: true })
                    //     }} style={{ paddingHorizontal: wp(2) }}>
                    //         <Image source={Constants.camera} style={{ width: 22, height: 25, resizeMode: 'contain' }} />
                    //     </Pressable>
                    // </View>}
                    // renderSend={() => <Pressable onPress={() => onMessageSend()}
                    //     disabled={message === ''}
                    //     style={[styles.sendButton, message === '' && { opacity: 0.5 }]}>
                    //     <Image source={Constants.send} style={{ height: 45, width: 45, resizeMode: 'contain' }} />
                    // </Pressable>}
                    // alwaysShowSend
                    renderBubble={renderBubble}
                    scrollToBottom={true}
                    isKeyboardInternallyHandled={true}
                    // onSend={messages => onSend(messages)}
                    user={{
                        _id: currentUser.Id,
                    }}
                />
            </View>
            {/* <SectionList
                ref={listRef}
                onScrollToIndexFailed={(error) => { console.log(error) }}
                sections={messageList}
                keyExtractor={(item, index) => index.toString()}
                extraData={messageList}
                renderItem={(props) => (
                    <MessageListItem
                        onMediaPress={(uri: string) => {
                            setImageViewState(true);
                            setImageImageUri(uri)
                        }}
                        otherUser={otherUser}
                        {...props}
                    />
                )}
                stickySectionHeadersEnabled={true}
                ItemSeparatorComponent={() => <View style={{ height: hp(1.5) }} />}
                ListFooterComponent={() => <View style={{ height: hp(1.5) }} />}
                showsVerticalScrollIndicator={false}
                renderSectionHeader={({ section: { title } }) => (
                    <Text
                        style={{
                            backgroundColor: Colors.white,
                            fontFamily: Constants.NiveauGroteskRegular,
                            paddingVertical: hp(2),
                            color: Colors.textGrey,
                            textAlign: 'center',
                            fontSize: wp(4),
                        }}>
                        {title}
                    </Text>
                )}
            /> */}
        </View>
        <View style={{ backgroundColor: Colors.white, paddingVertical: hp(1), paddingHorizontal: wp(2), flexDirection: 'row', alignItems: 'center', }}>
            <View style={styles.messageInputContainer}>
                <View style={{ flex: 1 }}>
                    <TextInput
                        placeholderTextColor={Colors.placeholderColor}
                        onFocus={() => updateState({ showKeyboard: true })}
                        onBlur={() => updateState({ showKeyboard: false })}
                        autoCorrect={false}
                        value={message}
                        onChangeText={(message) => updateState({ message })}
                        placeholder={'Enter Message here...'}
                        style={styles.messageInput} />
                </View>
                {/*  <Pressable style={{ paddingHorizontal: wp(2) }}>
                    <Image source={Constants.emoji} style={{ width: 22, height: 22, resizeMode: 'contain' }} />
                </Pressable> */}
                <Pressable onPress={() => {
                    Keyboard.dismiss();
                    updateState({ showCameraOptions: true })
                }} style={{ paddingHorizontal: wp(2) }}>
                    <Image source={Constants.camera} style={{ width: 22, height: 25, resizeMode: 'contain' }} />
                </Pressable>
            </View>
            <Pressable onPress={() => onMessageSend()}
                disabled={message === ''}
                style={[styles.sendButton, message === '' && { opacity: 0.5 }]}>
                <Image source={Constants.send} style={{ height: 45, width: 45, resizeMode: 'contain' }} />
            </Pressable>
        </View>
        <Modal
            transparent
            animationType='slide'
            visible={showCard}
            onRequestClose={() => updateState({ showCard: false })}>
            <EventDetailView
                isMessageDetails={true}
                otherUser={otherUser}
                invitationDetails={invitationDetails}
                onPress={() => updateState({ showCard: false })}
            />
        </Modal>
        {showKeyboard && Constants.platform === 'ios' && <View style={{ height: 60 }} />}
        {isLoading && <Spinner />}
        {showCameraOptions && <ActionSheet
            onCancel={() => updateState({ showCameraOptions: false })}
            onImageSuccess={(props) => sendMediaFile(props)}
        />}
        <ImageView
            images={images}
            imageIndex={0}
            visible={showImageView}
            onRequestClose={() => {
                setImageViewState(false);
                setImageImageUri('')
            }}
        />
        <Modal
            transparent
            animationType='slide'
            visible={showWriteReview}
            onRequestClose={() => updateState({ showWriteReview: false })}>
            <WriteReviewCard
                invitationId={invitationDetails.Id}
                userId={chatUser.Id}
                onSuccess={() => Utils.showToast('Review sent successfully!', 'Success')}
                onPress={() => updateState({ showWriteReview: false })}
            />
        </Modal>
    </KeyboardAvoidingView>
}

export default MessageDetail;

const styles = StyleSheet.create({
    sendButton: {
        borderRadius: 7,
        backgroundColor: Colors.primary,
        // justifyContent: 'center',
        // alignItems: 'center',
        marginLeft: 10
    },
    messageInput: {
        paddingHorizontal: wp(2),
        height: 45,
        color: Colors.black
    },
    messageInputContainer: {
        height: 45,
        flexDirection: 'row',
        backgroundColor: '#F2F2F2',
        flex: 1,
        alignItems: 'center',
    },
    spacer: {
        height: 12
    },
    locationView: {
        backgroundColor: '#FCEDE4',
        width: wp(40),
        borderTopLeftRadius: 25,
        borderBottomLeftRadius: 25,
        height: 40,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    locationIcon: {
        width: wp(4),
        marginHorizontal: wp(2),
        height: '80%',
        resizeMode: 'contain'
    },
    locationLabel: {
        color: '#42526E',
        fontSize: wp(3),
        fontFamily: Constants.NiveauGroteskBold
    },
    errorText: {
        color: '#ff5252',
        paddingTop: 5,
        fontSize: 12
    },
    textInput: {
        marginTop: 10,
        height: 35,
        backgroundColor: '#eee',
        borderRadius: 5,
        paddingHorizontal: 10,
        color: Colors.black
    },
    dateTitle: {
        fontSize: wp(4),
        color: '#42526EB2',
        fontFamily: Constants.NiveauGroteskRegular,
        paddingHorizontal: wp(3)
    },
    eventTitle: {
        fontSize: wp(5),
        paddingVertical: hp(.8),
        fontWeight: '500',
        fontFamily: Constants.NiveauGroteskBold,
        color: '#0E134F'
    },
    eventDescription: {
        fontSize: wp(3.2),
        fontFamily: Constants.NiveauGroteskRegular,
        color: '#42526EB2'
    },
    eventDetailContainer: {
        backgroundColor: Colors.white,
        padding: wp(4),
        margin: wp(4),
        borderWidth: 2,
        borderColor: Colors.primary,
        borderRadius: 7
    }
});
