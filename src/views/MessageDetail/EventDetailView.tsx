import { useNavigation } from '@react-navigation/native';
import { Colors, Constants, Utils } from 'common';
import { AuthInput, Button, DateSelector, ImageView, Input } from 'components';
import _ from 'lodash';
import moment from 'moment';
import * as React from 'react';
import { Text, View, StyleSheet, KeyboardAvoidingView, ScrollView, Image } from 'react-native';
import { IconButton, Surface } from 'react-native-paper';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { UserModel } from 'reduxStore/models';
import { InvitationItemModel } from 'reduxStore/models/invitation';
import { updateInvitationRequest } from 'reduxStore/services/invitations';

interface EventDetailViewProps {
    showActionButtons?: boolean
    isMessageDetails?: boolean
    onPress: () => void
    onSuccess?: () => void
    invitationDetails: InvitationItemModel,
    otherUser?: UserModel
}

const EventDetailView = (props: EventDetailViewProps) => {
    let navigation = useNavigation();
    let details = props.invitationDetails || {}
    let showActionButtons = props.showActionButtons ? 1 : 0;
    let isMessageDetails = props.isMessageDetails ? 1 : 0;
    const onSubmit = (type: number) => {
        updateInvitationRequest(details.Id, type === 0 ? 'reject' : 'accept')
            .then(res => {
                if (res.Status === 200) {
                    props.onPress();
                    props.onSuccess && props.onSuccess();
                } else {
                    Utils.showToast(res.Message, 'Error');
                }
            }).catch(err => {
                props.onPress();
                Utils.showToast(err.Message);
            })
    }

    let imageUrl = Constants.logo;
    if (!_.isEmpty(details.CreatedBy.Media)) {
        imageUrl = {
            uri: details.CreatedBy.Media.BasePath + details.CreatedBy.Media.ThumbPath
        }
    }
    if (props.otherUser) {
        if (!_.isEmpty(props.otherUser.Media)) {
            imageUrl = {
                uri: props.otherUser.Media.BasePath + props.otherUser.Media.ThumbPath
            }
        }
    }

    const goToProfile = () => {
        props.onPress();
        navigation.navigate('profileView', { user: details.CreatedBy, eventId: details.Id, fromEvent: true })
    }

    return (
        <KeyboardAvoidingView style={{ flex: 1 }} behavior={Constants.platform === 'ios' ? 'padding' : undefined}>
            <View style={styles.container}>
                <Surface style={styles.subContainer}>
                    <View style={styles.userImageContainer}>
                        <ImageView style={styles.userImage} source={imageUrl} />
                    </View>
                    <IconButton
                        icon={'close'}
                        onPress={() => props.onPress()}
                        style={styles.closeButton}
                        color={Colors.primary}
                    />
                    <ScrollView bounces={false}>
                        <Text style={styles.title}>{details.Title}</Text>
                        <Text style={styles.invitationBy}>Invite By: <Text style={{ textDecorationLine: 'underline', color: Colors.primary, fontFamily: Constants.NiveauGroteskBold }}>{details.CreatedBy.FullName}</Text></Text>
                        <View style={{ height: hp(2) }} />
                        <View style={styles.formContainer}>
                            <Text style={styles.eventSubtitle}>Event Message</Text>
                            <Text style={styles.description}>{details.Description}</Text>
                            <View style={{ height: hp(2) }} />
                            <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                                <Image source={Constants.setting_3} style={{ width: 22, height: 25, resizeMode: 'contain' }} />
                                <Text style={styles.eventDate}>{moment(details.Time).format('DD MMM, YYYY')}</Text>
                            </View>
                            <View style={{ height: hp(2) }} />
                            <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                                <Image source={Constants.location} style={{ width: 22, height: 25, resizeMode: 'contain' }} />
                                <Text style={styles.eventDate}>{details.Address}</Text>
                            </View>
                            <View style={{ height: hp(2) }} />
                            <View style={{ height: hp(2) }} />
                            {showActionButtons === 1 && <View style={{ paddingHorizontal: wp(2), flexDirection: 'row' }}>
                                <Button
                                    emptyButton
                                    style={{ borderColor: '#42526EB2' }}
                                    titleColor={'#42526EB2'}
                                    onPress={() => onSubmit(0)} text='Reject' >
                                    <Image source={Constants.cross} style={{ width: 15, height: 15, marginHorizontal: 6, resizeMode: 'contain' }} />
                                </Button>
                                <View style={{ flex: 1 }} />
                                <Button onPress={() => onSubmit(1)} text='Accept'>
                                    <Image source={Constants.check} style={{ width: 15, height: 18, marginHorizontal: 6, resizeMode: 'contain' }} />
                                </Button>
                            </View>}
                            {showActionButtons !== 1 && !props.isMessageDetails && <Button onPress={() => goToProfile()} text='Go To Profile' />}
                        </View>
                    </ScrollView>
                </Surface>
            </View>
        </KeyboardAvoidingView>
    );
};

EventDetailView.defaulProps = {
    showActionButtons: true,
    isMessageDetails: false,
    invitationDetails: {}
};
export default EventDetailView;

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(255,255,255,0.8)',
        flex: 1,
        paddingHorizontal: wp(5),
        justifyContent: 'center',
        alignItems: 'center',
    },
    subContainer: {
        width: wp(90),
        backgroundColor: Colors.white,
        height: hp(70),
        borderRadius: 10,
        ...Constants.boxShadow
    },
    title: {
        fontFamily: Constants.NiveauGroteskBold,
        fontWeight: '700',
        paddingTop: hp(3),
        textAlign: 'center',
        color: '#0E134F',
        fontSize: wp(5)
    },
    invitationBy: {
        fontFamily: Constants.NiveauGroteskRegular,
        paddingVertical: hp(1),
        textAlign: 'center',
        color: 'rgba(66, 82, 110, 0.7)',
        fontSize: wp(3)
    },
    eventSubtitle: {
        fontFamily: Constants.NiveauGroteskBold,
        paddingVertical: hp(1),
        color: '#0E134F',
        fontSize: wp(3.5)
    },
    description: {
        fontFamily: Constants.NiveauGroteskRegular,
        color: '#42526EB2',
        fontSize: wp(3),
        lineHeight: 23
    },
    eventDate: {
        fontFamily: Constants.NiveauGroteskRegular,
        color: '#42526EB2',
        fontSize: wp(3),
        paddingHorizontal: wp(3)
    },
    formContainer: {
        paddingHorizontal: wp(5),
        paddingVertical: hp(3),
    },
    userImage: {
        width: 100,
        height: 100,
        borderRadius: 100 / 2,
        overflow: 'hidden',
        zIndex: 9999,
        borderColor: Colors.white,
        resizeMode: 'cover',
    },
    userImageContainer: {
        top: -100 / 2,
        alignItems: 'center',
    },
    closeButton: {
        position: 'absolute',
        right: 10,
        top: 10
    }
});
