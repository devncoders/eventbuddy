import { Colors, Constants } from 'common';
import { ImageView as ImageComp } from 'components';
import _ from 'lodash';
import moment from 'moment';
import * as React from 'react';
import { Text, View, StyleSheet, Image, Pressable } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { useSelector } from 'react-redux';
import { UserModel } from 'reduxStore/models';
import { MessageItem as MessageItemResp, MessageItemModel } from 'reduxStore/models/message';
import { RootState } from 'reduxStore/reducers';
// import ImageView from 'react-native-image-view';

interface MessageItemProps {
    otherUser: UserModel,
    item: MessageItemResp,
    index: number,
    onMediaPress: (url: string) => void

}

const MessageItem = (props: MessageItemProps) => {
    let user: UserModel = useSelector((state: RootState) => state.main.userData);
    let {
        Message = '',
        CreatedAt,
        Id = 1,
        MediaId,
        Media
    } = props.item;
    let mediaUrl = '';
    let imageUrl = Constants.logo;

    if (!_.isEmpty(props.otherUser.Media)) {
        imageUrl = {
            uri: props.otherUser.Media.BasePath + props.otherUser.Media.ThumbPath
        }
    }
    if (!_.isEmpty(Media)) {
        mediaUrl = Media.BasePath + Media.ThumbPath;
    }
    const _getMessageItem = () => {
        if (props.item.CreatedById !== user.Id) {
            return (
                <View style={styles.container}>
                    <View style={styles.subContainer}>
                        <ImageComp style={styles.image} source={imageUrl} />
                        <View style={styles.senderContent}>
                            {!MediaId && <Text style={styles.userDescription}>{Message}</Text>}
                            {MediaId && <Pressable onPress={() => {
                                props.onMediaPress && props.onMediaPress(mediaUrl)
                            }}>
                                <Image style={styles.messageImage} source={{ uri: mediaUrl }} />
                            </Pressable>}
                        </View>
                    </View>
                    <Text style={styles.createdAtText}>{moment(CreatedAt).fromNow()}</Text>
                    <View style={{ width: wp(20) }} />
                </View>
            );
        }
        /* else {
            return (
                <View style={styles.container}>
                    <View style={styles.subContainer}>
                        <View style={{ minWidth: wp(20) }} />
                        <View style={styles.receiverContent}>
                            {!MediaId && <Text style={[styles.userDescription, { textAlign: 'right' }]}>{Message}</Text>}
                            {MediaId && <Pressable onPress={() => {
                                props.onMediaPress && props.onMediaPress(mediaUrl)
                            }}>
                                <Image style={styles.messageImage} source={{ uri: mediaUrl }} />
                            </Pressable>}
                        </View>
                    </View>
                    <Text style={styles.createdAtText}>{moment(CreatedAt).fromNow()}</Text>
                </View>
            );
        } */
    }

    return (
        <>
            {_getMessageItem()}
        </>
    )

};

export default MessageItem;

const styles = StyleSheet.create({
    messageImage: {
        width: '100%',
        height: hp(15),
        resizeMode: 'cover'
    },
    senderContent: {
        backgroundColor: Colors.primaryLight,
        marginLeft: wp(2),
        // flex: 1,
        borderRadius: 10,
        borderTopLeftRadius: 0,
        paddingVertical: wp(2),
        paddingHorizontal: wp(3)
    },
    receiverContent: {
        flexWrap: 'wrap',
        paddingVertical: wp(2),
        borderRadius: 10,
        borderTopRightRadius: 0,
        backgroundColor: '#F2F2F2',
        marginLeft: wp(2),
        paddingHorizontal: wp(3)
    },
    image: {
        width: wp(12),
        height: wp(12),
        resizeMode: 'cover',
        borderRadius: wp(12) / 2,
        overflow: 'hidden'
    },
    subContainer: {
        flexDirection: 'row',
    },
    container: {
        padding: wp(2),
        marginHorizontal: wp(3),
    },
    createdAtText: {
        paddingTop: 3,
        color: Colors.textDark,
        fontFamily: Constants.NiveauGroteskMedium,
        fontSize: wp(2),
        textAlign: 'right'
    },
    reviewCount: {
        color: Colors.primary,
        fontFamily: Constants.NiveauGroteskMedium,
        fontSize: wp(3)
    },
    userTitle: {
        color: '#0E134F',
        fontFamily: Constants.NiveauGroteskMedium,
        fontSize: wp(3.6),
        paddingVertical: wp(1),
        paddingRight: wp(2)
    },
    messageTime: {
        color: Colors.primary,
        fontFamily: Constants.NiveauGroteskMedium,
        fontSize: wp(2.5),
    },
    userDescription: {
        flexWrap: 'wrap',
        color: '#42526EB2',
        fontFamily: Constants.NiveauGroteskRegular,
        fontSize: wp(3)
    },
});
