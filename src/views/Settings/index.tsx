import React, { Component, useEffect, useRef, useState } from "react";

import { AsyncStorage, View, Dimensions, Text, StyleSheet, Linking, Pressable, Image, ScrollView } from "react-native";

import * as Animatable from "react-native-animatable";
import { Header, Spinner } from "components";
import { Colors, Constants, Utils } from "common";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import Icon from "react-native-vector-icons/FontAwesome5";
import { Appbar, Surface, Switch } from "react-native-paper";
import { CommonActions } from "@react-navigation/routers";
import { useDispatch, useSelector } from "react-redux";
import { removeUser, updateUser, updateUserToken } from "reduxStore/actions/auth";
import { UserModel } from "reduxStore/models";
import { RootState } from "reduxStore/reducers";
import { getProfile, updateProfile } from "reduxStore/services/user";
import _ from "lodash";

console.disableYellowBox = true;

let validURL = (str: string) => {
    var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
        '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return !!pattern.test(str);
}

interface SettingsProps {
    navigation: any
}
let initialState = {
    isBack: true,
    isOpen: true,
    isLoading: false,
    isVerified: false
}
let resetAction: any = CommonActions.reset({
    index: 0,
    routes: [
        { name: 'login' },
    ],
});
const Settings = (props: SettingsProps) => {
    let userData: UserModel = useSelector((state: RootState) => state.main.userData);
    const dispatch = useDispatch();
    // let isVerified = userData.IsVerified;
    const [isVerified, setVerified] = useState(userData.IsVerified);
    const [
        { isLoading }, setState
    ] = useState(initialState);
    const updateState = (newState = {}) => {
        setState(prevState => ({ ...prevState, ...newState }));
    }

    const onChangeInvitationStatus = (value: boolean) => {
        updateProfile({ AvailableForInvitation: value }).then(res => {
            if (res.Status === 200 && !_.isEmpty(res.Data)) {
                dispatch(updateUser(res.Data.User));
            }
        })
    }

    useEffect(() => {

    }, [])

    const onLogout = () => {
        Utils.confirmAlert('Exit?', 'are you sure you want to exit?', (status) => {
            if (status === 'success') {
                updateState({ isLoading: true });
                updateProfile({ FCMToken: '' }).then(res => {
                    if (res.Status === 200 && !_.isEmpty(res.Data)) {

                    }
                }).catch(err => {

                }).finally(() => {
                    dispatch(removeUser());
                    AsyncStorage.removeItem('userData');
                    dispatch(updateUserToken(''));
                    props.navigation.dispatch(resetAction);
                })

            }
        })
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.white }}>
            <Appbar.Header style={{ height: 90, elevation: 0, justifyContent: 'flex-start', backgroundColor: Colors.white }}>
                <View>
                    <Appbar.Content style={{ flex: undefined, }} titleStyle={{ fontFamily: Constants.NiveauGroteskBold, fontWeight: '700', color: '#0E134F', fontSize: wp(5) }} title='Settings' />
                </View>
                <View style={{ flex: 1 }} />
                {<View style={[styles.locationView, !userData.IsVerified && styles.locationViewUnVerified]}>
                    <Image style={[styles.locationIcon, !userData.IsVerified && { tintColor: Colors.unverfiedText }]} source={!userData.IsVerified ? Constants.unverified : Constants.check} />
                    <Text style={[styles.locationLabel, !userData.IsVerified && { color: Colors.unverfiedText }]}>{userData.IsVerified ? 'Verified' : 'Unverified'}</Text>
                </View>}
            </Appbar.Header>
            <View style={{ flex: 1 }}>
                <ScrollView>
                    <View style={styles.spacer} />
                    <Pressable onPress={() => props.navigation.navigate('profileSettings')}>
                        <View style={styles.settingButton}>
                            <Image style={styles.buttonIcon} source={Constants.setting_1} />
                            <Text style={styles.buttonTitle}>Profile Setting</Text>
                            <Icon color={'#42526E70'} size={wp(4)} name='chevron-right' />
                        </View>
                    </Pressable>
                    <View style={styles.spacer} />
                    <Pressable onPress={() => props.navigation.navigate('changePassword')}>

                        <View style={styles.settingButton}>
                            <Image style={styles.buttonIcon} source={Constants.setting_2} />
                            <Text style={styles.buttonTitle}>Change Password</Text>
                            <Icon color={'#42526E70'} size={wp(4)} name='chevron-right' />
                        </View>
                    </Pressable>
                    <View style={styles.spacer} />
                    <Pressable onPress={() => props.navigation.navigate('invitations')}>
                        <View style={styles.settingButton}>
                            <Image style={styles.buttonIcon} source={Constants.setting_3} />
                            <Text style={styles.buttonTitle}>Invitations</Text>
                            <Icon color={'#42526E70'} size={wp(4)} name='chevron-right' />
                        </View>
                    </Pressable>
                    <View style={styles.spacer} />
                    <Pressable>
                        <View style={styles.settingButton}>
                            <Image style={styles.buttonIcon} source={Constants.setting_4} />
                            <Text style={styles.buttonTitle}>Available for Invitation</Text>
                            <Switch
                                value={userData.AvailableForInvitation}
                                trackColor={{ true: Colors.primary, false: Colors.textLight }}
                                // thumbColor={Colors.borderColor}
                                onValueChange={(value) => {
                                    setVerified(value)
                                    onChangeInvitationStatus(value)
                                }}
                                color={Colors.primary}
                            />
                        </View>
                    </Pressable>
                    <View style={styles.spacer} />
                    <Pressable>
                        <View style={styles.settingButton}>
                            <Image style={styles.buttonIcon} source={Constants.setting_5} />
                            <Text style={styles.buttonTitle}>Verify Your Account</Text>
                            <Icon color={'#42526E70'} size={wp(4)} name='chevron-right' />
                        </View>
                    </Pressable>
                    <View style={styles.spacer} />
                    <Pressable onPress={() => props.navigation.navigate('privacyPolicy')}>
                        <View style={styles.settingButton}>
                            <Image style={styles.buttonIcon} source={Constants.setting_6} />
                            <Text style={styles.buttonTitle}>Privacy Policy</Text>
                            <Icon color={'#42526E70'} size={wp(4)} name='chevron-right' />
                        </View>
                    </Pressable>
                    <View style={styles.spacer} />
                    <Pressable onPress={() => onLogout()}>
                        <View style={styles.settingButton}>
                            <Image style={styles.buttonIcon} source={Constants.setting_7} />
                            <Text style={styles.buttonTitle}>Log Out</Text>
                            <Icon color={'#42526E70'} size={wp(4)} name='chevron-right' />
                        </View>
                    </Pressable>
                    <View style={styles.spacer} />
                </ScrollView>
            </View>
            {isLoading && <Spinner />}
        </View>
    );

}

const styles = StyleSheet.create({
    spacer: {
        height: hp(2)
    },
    locationViewUnVerified: {
        backgroundColor: Colors.unverified,
    },
    locationView: {
        backgroundColor: Colors.verified,
        minWidth: wp(22),
        paddingHorizontal: wp(2),
        borderTopLeftRadius: 25,
        borderBottomLeftRadius: 25,
        height: 40,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    locationIcon: {
        tintColor: Colors.verfiedText,
        width: wp(4),
        marginHorizontal: wp(2),
        height: '80%',
        resizeMode: 'contain'
    },
    locationLabel: {
        color: Colors.verfiedText,
        fontSize: wp(3),
        fontFamily: Constants.NiveauGroteskRegular
    },
    settingButton: {
        flexDirection: 'row',
        height: hp(7),
        alignItems: 'center',
        borderRadius: 5,
        marginHorizontal: wp(4),
        paddingHorizontal: wp(5),
        ...Constants.boxShadow,
        backgroundColor: Colors.white
    },
    buttonIcon: {
        width: wp(5),
        height: wp(5),
        resizeMode: 'contain'
    },
    buttonTitle: {
        paddingHorizontal: wp(4),
        color: '#0E134F',
        fontFamily: Constants.NiveauGroteskMedium,
        fontSize: wp(3.8),
        flex: 1
    },
});

export default Settings;