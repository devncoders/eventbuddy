import { Colors, Constants } from 'common';
import _ from 'lodash';
import * as React from 'react';
import { Text, View, StyleSheet, Image, Pressable } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { UsersItemModel } from 'reduxStore/models/users';

interface UserItemProps {
    item: UsersItemModel,
    index?: number,
    onPress?: () => void
}

const UserItem = (props: UserItemProps) => {
    let image = Constants.logo;
    if (!_.isEmpty(props.item.Media)) {
        image = {
            uri: props.item.Media.BasePath + props.item.Media.ThumbPath
        }
    }
    return (
        <Pressable onPress={props.onPress} style={styles.container}>
            <Image style={styles.image} source={image} />
            <View style={{ flex: 1, paddingHorizontal: wp(3) }}>
                <Text style={styles.reviewCount}>{props.item.ReviewCount} Reviews</Text>
                <Text numberOfLines={1} style={styles.userTitle}>{props.item.FullName}</Text>
                <Text numberOfLines={3} style={styles.userDescription}>{props.item.About}</Text>
            </View>
        </Pressable>
    );
};

export default UserItem;

const styles = StyleSheet.create({
    image: {
        width: 100,
        height: 100,
        resizeMode: 'cover',
        borderRadius: 10
    },
    container: {
        width: wp(90),
        backgroundColor: Colors.white,
        flexDirection: 'row',
        paddingHorizontal: wp(3),
        alignItems: 'center',
        height: 130,
        marginVertical: 10,
        borderRadius: 6,
        ...Constants.boxShadow
    },
    reviewCount: {
        color: Colors.primary,
        fontFamily: Constants.NiveauGroteskMedium,
        fontSize: wp(3)
    },
    userTitle: {
        color: '#0E134F',
        fontFamily: Constants.NiveauGroteskMedium,
        fontSize: wp(4.2),
        paddingVertical: wp(1)
    },
    userDescription: {
        color: '#42526EB2',
        fontFamily: Constants.NiveauGroteskMedium,
        fontSize: wp(3.5)
    },
});
