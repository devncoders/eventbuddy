import * as React from 'react';
import { Text, View, StyleSheet, Image, FlatList, Pressable, KeyboardAvoidingView } from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import { Appbar, Surface } from 'react-native-paper';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Carousel from 'react-native-snap-carousel';
import { UsersItemModel } from 'reduxStore/models/users';
import UserItem from './UserItem';
import Geolocation, { GeolocationResponse } from '@react-native-community/geolocation';
import { Colors, Constants, Utils } from 'common';
import { Input, Spinner } from 'components';
import _ from 'lodash';
import { useNavigation } from '@react-navigation/native';

interface MainMapProps {
    users: Array<UsersItemModel>
    userPosition: GeolocationResponse['coords']
}

const MainMap = (props: MainMapProps) => {
    let [showUsers, setUserState] = React.useState(false);
    let [isLoading, setLoading] = React.useState(false);
    let mapViewRef = React.useRef<MapView>(null);
    const onMapReady = () => {
        let coordniates: Array<{
            latitude: number,
            longitude: number
        }> = props.users.filter(i => !_.isEmpty(i.Location)).map(i => ({ latitude: i.Location.Lat, longitude: i.Location.Lng, }));
        coordniates.push({
            latitude: props.userPosition.latitude,
            longitude: props.userPosition.longitude,
        })
        mapViewRef.current?.fitToCoordinates(coordniates, {
            edgePadding: {
                top: 20,
                left: 20,
                bottom: 300,
                right: 20
            }
        })
    }

    const onShowMap = () => {
        setLoading(true);
        setTimeout(() => {
            setUserState(true);
            setLoading(false);
        }, 2500);
    }

    const _currentUser = () => {
        return <Marker
            coordinate={{
                latitude: props.userPosition.latitude,
                longitude: props.userPosition.longitude,
            }}
            title="I'm here!"
            pinColor={Colors.primary}
        />
    }

    const getFilteredUsers = () => {
        return props.users.filter(i => !_.isEmpty(i.Location));
    }

    React.useEffect(() => {
    }, [])
    React.useEffect(() => {
        if (showUsers) {
            onMapReady();
        }
    }, [showUsers]);
    let navigation = useNavigation();
    console.log('getFilteredUsers', getFilteredUsers());

    return (
        <>
            <KeyboardAvoidingView behavior={Constants.platform === 'ios' ? 'padding' : undefined} style={styles.container}>
                <MapView
                    onMapReady={onMapReady}
                    ref={mapViewRef}
                    showsMyLocationButton
                    style={{ flex: 1 }}>
                    {getFilteredUsers().map((i) => {
                        if (_.isEmpty(i.Location)) return;
                        let image = Constants.logo;
                        if (!_.isEmpty(i.Media)) {
                            image = {
                                uri: i.Media.BasePath + i.Media.ThumbPath
                            }
                        }
                        return <Marker
                            onPress={() => navigation.navigate('profileView', { user: i })}
                            coordinate={{
                                latitude: i.Location.Lat,
                                longitude: i.Location.Lng,
                            }}
                        >
                            <View>
                                <View style={styles.imageContainer}>
                                    <Image style={styles.image} source={image} />
                                </View>
                                <View style={styles.triangle} />
                            </View>
                        </Marker>
                    })}
                    {_currentUser()}
                </MapView>
                <View style={styles.listContainer}>
                    <Carousel
                        //   ref={(c) => { this._carousel = c; }}
                        data={getFilteredUsers()}
                        renderItem={(props) => <UserItem
                            onPress={() => navigation.navigate('profileView', { user: props.item })}
                            {...props} />}
                        sliderWidth={wp(100)}
                        onSnapToItem={(index) => {

                            let item = getFilteredUsers()[index];
                            console.log('item = = => ', item);
                            if (_.isEmpty(item.Location)) return
                            mapViewRef?.current?.animateToRegion(
                                {
                                    latitude: item.Location.Lat,
                                    longitude: item.Location.Lng,
                                    latitudeDelta: 0.01,
                                    longitudeDelta: 0.01,
                                },
                                1000,
                            )
                        }}
                        itemWidth={wp(90)}
                    />
                </View>
            </KeyboardAvoidingView>
            {isLoading && <Spinner />}
        </>
    );
};

MainMap.defaultProps = {
    users: []
}

export default MainMap;

const styles = StyleSheet.create({
    currentLocationButton: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10
    },
    currentLocationButtonText: {
        fontSize: 14,
        color: '#42526E',
        fontWeight: '400',
        fontFamily: Constants.NiveauGroteskRegular,
    },
    gpsIcon: {
        width: 25,
        height: 25,
        resizeMode: 'contain',
        marginRight: 12
    },
    title: {
        fontSize: 22,
        color: '#0E134F',
        fontFamily: Constants.NiveauGroteskBold,

    },
    skipButtonText: {
        color: Colors.primary,
        fontFamily: Constants.NiveauGroteskMedium,
        fontSize: 15,
        paddingVertical: 5,
        paddingHorizontal: 7
    },
    searchContainer: {
        paddingHorizontal: wp(7),
        paddingVertical: 30,
        borderTopLeftRadius: 25,
        borderTopRightRadius: 25,
        overflow: 'hidden',
        elevation: 4,
        width: '100%',
        position: 'absolute',
        bottom: 0,
        left: 0,
        backgroundColor: Colors.white,
        minHeight: hp(25)
    },
    triangle: {
        alignSelf: 'center',
        width: 0,
        top: -1,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderTopWidth: 7,
        borderBottomWidth: 0,
        borderRightWidth: 15 / 2,
        borderLeftWidth: 15 / 2,
        borderTopColor: '#ccc',
        borderRightColor: 'transparent',
        borderBottomColor: 'transparent',
        borderLeftColor: 'transparent',
    },
    imageContainer: {
        width: 50,
        height: 50,
        borderRadius: 50 / 2,
        borderWidth: 5, borderColor: '#ccc',
    },
    image: {
        width: 40,
        height: 40,
        borderRadius: 40 / 2,
    },
    container: {
        flex: 1
    },
    listContainer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        // backgroundColor: '#fff',//Colors.primary,
    },
    locationDetailView: {
        paddingVertical: 7,
        flexDirection: 'row',
        alignItems: 'center',
    },
    locationTextDetail: {
        fontFamily: Constants.NiveauGroteskLight,
        color: '#0E134F',
        fontSize: 13
    },
    locationIcon: {
        width: 20,
        height: wp(4),
        marginRight: wp(1),
        resizeMode: 'contain'
    },
    mapIcon: {
        width: wp(4),
        marginHorizontal: wp(1),
        height: '80%',
        resizeMode: 'contain'
    },
    locationLabel: {
        color: '#42526E',
        fontSize: wp(3),
        fontFamily: Constants.NiveauGroteskBold
    },
});
