
import React, { useEffect, useState } from 'react';
import {
    Image,
    TextInput,
    View,
    Dimensions,
    StyleSheet,
    KeyboardAvoidingView,
    Pressable,
    ToastAndroid,
    Alert,
    FlatList
} from 'react-native';
import { Appbar, Avatar, Text, Button, Card, Subheading, Title } from 'react-native-paper';
import * as Animated from 'react-native-animatable';
import { Colors, Constants, Utils } from 'common';

import { ErrorView, Header, Spinner } from 'components';
import MessageItem from './MessageItem';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { getMessages } from 'reduxStore/services/messages';
import { MessageItemModel } from 'reduxStore/models/message';
import { useFocusEffect } from '@react-navigation/native';
let initial = {
    title: '',
    description: '',
    isLoading: false,
    isFetching: true,
    messages: [] as Array<MessageItemModel>
}
const { width } = Dimensions.get('window');

const Messages = (props: any) => {
    let navigation = props.navigation;
    const [{
        isFetching,
        isLoading, messages
    }, setState] = useState(initial);

    const updateState = (state: {}) => {
        setState(prevState => ({ ...prevState, ...state }));
    }

    const _getMessagesList = () => {
        getMessages()
            .then(res => {
                updateState({ isLoading: false, isFetching: false })
                if (res.Status === 200) {
                    updateState({
                        messages: res.Data
                    })
                }
            }).catch(err => {
                updateState({ isLoading: false, isFetching: false })
            })
    }
    useFocusEffect(
        React.useCallback(() => {
            _getMessagesList();

            // const unsubscribe = API.subscribe(userId, user => setUser(user));

            // return () => unsubscribe();
        }, [])
    );

    /* useEffect(() => {
        _getMessagesList();
    }, []); */


    return <KeyboardAvoidingView behavior={Constants.platform == 'ios' ? 'padding' : undefined} style={{ flex: 1, backgroundColor: 'white' }}>
        <Appbar.Header style={{ height: 90, elevation: 0, justifyContent: 'flex-start', backgroundColor: Colors.white }}>
            <View>
                <Appbar.Content style={{ flex: undefined, }} titleStyle={{ fontFamily: Constants.NiveauGroteskBold, fontWeight: '700', color: '#0E134F', fontSize: wp(5) }} title='Messages' />
            </View>
            <View style={{ flex: 1 }} />
        </Appbar.Header>

        <View style={{ flex: 1 }}>
            {messages.length !== 0 ? <FlatList
                refreshing={isLoading}
                onRefresh={() => _getMessagesList()}
                data={messages}
                extraData={messages}
                ListHeaderComponent={() => <View style={{ height: hp(2) }} />}
                ListFooterComponent={() => <View style={{ height: hp(1.5) }} />}
                ItemSeparatorComponent={() => <View style={{ height: hp(1.5) }} />}
                keyExtractor={(item) => item.Id.toString()}
                renderItem={(props) => <MessageItem onPress={() => navigation.navigate('messageDetail', { messageItem: props.item, eventId: props.item.Id, userDetail: props.item })} {...props} />}
            /> : !isFetching && <ErrorView
                onRefresh={() => {
                    updateState({ isFetching: true });
                    _getMessagesList()
                }}
                errorMessage='No Messages!' />}
        </View>
        {isFetching && <Spinner />}
    </KeyboardAvoidingView>
}

export default Messages;

const styles = StyleSheet.create({
    spacer: {
        height: 12
    },
    locationView: {
        backgroundColor: '#FCEDE4',//Colors.primary,
        width: wp(40),
        borderTopLeftRadius: 25,
        borderBottomLeftRadius: 25,
        height: 40,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    locationIcon: {
        width: wp(4),
        marginHorizontal: wp(2),
        height: '80%',
        resizeMode: 'contain'
    },
    locationLabel: {
        color: '#42526E',
        fontSize: wp(3),
        fontFamily: Constants.NiveauGroteskBold
    },
    errorText: {
        textAlign: 'center',
        fontSize: wp(4),
        color: Colors.textDark,
        fontFamily: Constants.NiveauGroteskMedium
    },
    textInput: {
        marginTop: 10,
        height: 35,
        backgroundColor: '#eee',
        borderRadius: 5,
        paddingHorizontal: 10,
        color: Colors.black
    },
    add_photoIcon: {
        width: 100,
        height: 100,
        resizeMode: 'contain',
    }
});
