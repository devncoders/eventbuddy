import { Colors, Constants } from 'common';
import { ImageView } from 'components';
import _ from 'lodash';
import moment from 'moment';
import * as React from 'react';
import { Text, View, StyleSheet, Image, Pressable } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { useSelector } from 'react-redux';
import { UserModel } from 'reduxStore/models';
import { MessageItemModel } from 'reduxStore/models/message';
import { RootState } from 'reduxStore/reducers';

interface MessageItemProps {
    item: MessageItemModel,
    index: number,
    onPress: () => void
}

const MessageItem = (props: MessageItemProps) => {
    let messagesList = props.item.Chats || [];
    let user: UserModel = useSelector((state: RootState) => state.main.userData);
    let otherUser = props.item.UserId === user.Id ? props.item.CreatedBy : props.item.User
    let imageUrl = Constants.logo;
    if (!_.isEmpty(otherUser.Media)) {
        imageUrl = {
            uri: otherUser.Media.BasePath + otherUser.Media.ThumbPath
        }
    }
    let updatedAt = props.item.UpdatedAt;
    let message = '';
    if (messagesList.length > 0) {
        message = messagesList[0].MediaId ? '<Image>' : messagesList[0].Message;
        updatedAt = messagesList[0].CreatedAt
    }
    return (
        <Pressable onPress={props.onPress} style={styles.container}>
            <ImageView style={styles.image} source={imageUrl} />
            <View style={{ flex: 1, paddingHorizontal: wp(3) }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                    <Text numberOfLines={1} style={styles.userTitle}>{otherUser.FullName}</Text>
                    <Text numberOfLines={1} style={styles.messageTime}>{moment(updatedAt).fromNow()}</Text>
                </View>
                <Text numberOfLines={1} style={styles.userDescription}>{message}</Text>
            </View>
        </Pressable>
    );
};

export default MessageItem;

const styles = StyleSheet.create({
    image: {
        width: wp(12),
        height: wp(12),
        resizeMode: 'cover',
        borderRadius: 10,
        overflow: 'hidden',
    },
    container: {
        backgroundColor: Colors.white,
        flexDirection: 'row',
        paddingHorizontal: wp(2),
        paddingVertical: wp(3),
        marginHorizontal: wp(3),
        borderRadius: 6,
        ...Constants.boxShadow
    },
    reviewCount: {
        color: Colors.primary,
        fontFamily: Constants.NiveauGroteskMedium,
        fontSize: wp(3)
    },
    userTitle: {
        color: '#0E134F',
        fontFamily: Constants.NiveauGroteskMedium,
        fontSize: wp(3.6),
        paddingVertical: wp(1),
        paddingRight: wp(2)
    },
    messageTime: {
        color: Colors.primary,
        fontFamily: Constants.NiveauGroteskMedium,
        fontSize: wp(2.5),
    },
    userDescription: {
        color: '#42526EB2',
        fontFamily: Constants.NiveauGroteskRegular,
        fontSize: wp(3)
    },
});
