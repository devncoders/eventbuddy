
import React, { useEffect, useState } from 'react';
import {
    SafeAreaView,
    Pressable,
    Text,
    View,
    Image,
    StyleSheet,
    PermissionsAndroid
} from 'react-native';

import geolocation, { GeolocationResponse } from '@react-native-community/geolocation';
import { Constants, Colors, Utils } from 'common';
import messaging from '@react-native-firebase/messaging';
import { Button, Header, Spinner } from 'components';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { openSettings, PERMISSIONS } from 'react-native-permissions';
import { updateProfile } from 'reduxStore/services/user';
import { useDispatch } from 'react-redux';
import { updateUserToken } from 'reduxStore/actions/auth';

let initial = {
    loading: false,
    showLoginView: false,
    userPosition: null,
}
const LocationView = (props: any) => {

    let [{
        userPosition,
        loading
    }, setState] = useState(initial);

    const askPermission = () => {
        updateState({ loading: true })
        askLocationPermission().then(res => {
            updateState({ loading: true });
            let userPosition = res as GeolocationResponse['coords'];
            let data = {
                Lat: userPosition.latitude,
                Lng: userPosition.longitude,
                FCMToken: res
            }
            updateProfile(data).then(resp => {
                if (resp.Status === 200) {
                    props.navigation.replace('searchView', { userPosition });
                } else {
                    props.navigation.replace('searchView', { userPosition });
                }
            }).finally(() => {
                updateState({ loading: false });
                props.navigation.replace('searchView', { userPosition });
            })
        }).catch(err => {
            updateState({ loading: false })
            console.log('error', err);
            // Utils.showToast('Please enable location to continue using the application');
        });
    }

    const updateState = (state: {}) => {
        setState(prevState => ({ ...prevState, ...state }));
    }

    const getNotificationPermission = () => {
        return new Promise(async (resolve, reject) => {
            const authStatus = await messaging().requestPermission();
            const enabled =
                authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
                authStatus === messaging.AuthorizationStatus.PROVISIONAL;

            if (enabled) {
                var token = await messaging().getToken();
                console.log('token = = = > ', token);
                let dispatch = useDispatch();
                dispatch(updateUserToken(token));
                return resolve(token);
            } else {
                return reject('error')
            }
        });
    };
    const askLocationPermission = () => {
        return new Promise(async (resolve, reject) => {
            if (Constants.platform === 'android') {
                await PermissionsAndroid.request('android.permission.ACCESS_FINE_LOCATION')
                Utils.checkPermissionForUsage('location')
                    .then(res => {
                        geolocation.getCurrentPosition(
                            (position) => {
                                updateState({ userPosition: position.coords });
                                return resolve(position.coords);
                            },
                            (error) => {
                                console.log('error = = = >', error);
                                Utils.showToast('Please check & enable your GPS to continue using the application', '', (status) => {
                                    openSettings()
                                        .then(() => console.warn('open settings'))
                                        .catch(() => console.warn('cannot open settings'));
                                });
                                return reject(error);
                            },
                            { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 }
                        );
                    }).catch(err => {
                        Utils.showToast('Please check & enable your GPS to continue using the application', '', (status) => { });
                        openSettings()
                            .then(() => console.warn('open settings'))
                            .catch(() => console.warn('cannot open settings'));
                        return reject('error')
                    })
            } else {
                await geolocation.requestAuthorization();
                geolocation.getCurrentPosition(
                    (position) => {
                        console.log('userPosition = = => ', userPosition);
                        updateState({ userPosition: position.coords });
                        return resolve(position.coords);
                    },
                    (error) => {
                        console.log('error = = = >', error);
                        Utils.showToast('Please check & enable your GPS to continue using the application', '', (status) => {
                            openSettings()
                                .then(() => console.warn('open settings'))
                                .catch(() => console.warn('cannot open settings'));
                        });
                        return reject(error);
                    },
                    { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 }
                );
            }

            /* Utils.checkPermissionForUsage('location')
                .then(res => {
                    geolocation.getCurrentPosition(
                        (position) => {
                            updateState({ userPosition: position });
                            return resolve(position);
                        },
                        (error) => {
                            console.log('error = = = >', error);
                            Utils.showToast('Please check & enable your GPS to continue using the application', '', (status) => {
                                openSettings()
                                    .then(() => console.warn('open settings'))
                                    .catch(() => console.warn('cannot open settings'));
                            });
                            return reject(res);
                        },
                        { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 }
                    );

                })
                .catch(error => {
                    console.log('error = = = > ', error);
                    Utils.showToast('Please enable location to continue using the application', '', (status) => {
                        openSettings()
                            .then(() => console.warn('open settings'))
                            .catch(() => console.warn('cannot open settings'));
                    });
                    return reject(error);
                }) */
        });
    };



    useEffect(() => {
    }, [])

    return <View style={{ flex: 1, backgroundColor: Colors.white }}>
        <Header title={'Enable Location'} />
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingHorizontal: wp(10) }}>
            <Image source={Constants.verify_location} style={styles.imageView} />
            <Text style={styles.description}>Please enable your location to access the app</Text>
        </View>
        <View style={{ paddingVertical: hp(2), paddingHorizontal: wp(5) }}>
            <Button onPress={() => askPermission()} text='Enable' />
        </View>
        {loading && <Spinner />}
    </View>
}

export default LocationView;

const styles = StyleSheet.create({

    imageView: {
        width: wp(70),
        height: wp(70),
        resizeMode: 'contain'
    },
    description: {
        fontSize: wp(3.2),
        color: '#42526E',
        fontWeight: '400',
        fontFamily: Constants.NiveauGroteskRegular,
        paddingVertical: hp(2.2),
        textAlign: 'center',
        paddingHorizontal: wp(6)
    },
});
