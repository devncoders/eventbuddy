import { Colors, Constants, Utils } from 'common';
import { AuthInput, Button, DateSelector, Input } from 'components';
import * as React from 'react';
import { Text, View, StyleSheet, KeyboardAvoidingView, ScrollView, Pressable, Image } from 'react-native';
import { IconButton } from 'react-native-paper';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { InvitationItemModel } from 'reduxStore/models/invitation';
import { UsersItemModel } from 'reduxStore/models/users';
import { createReview } from 'reduxStore/services/user';

interface WriteReviewCardProps {
    onPress: () => void
    onSuccess: () => void
    userId: number
    invitationId?: number
}

const WriteReviewCard = (props: WriteReviewCardProps) => {
    const [rating, setRating] = React.useState(1);
    const [Description, setDescription] = React.useState('');
    const [EventName, setEventName] = React.useState('');
    const [isLoading, setLoading] = React.useState(false);
    // const [Description,setDescription]= React.useState('');
    const onSubmit = () => {
        props.onPress();
    }
    const renderRating = () => {
        let data = [];
        for (let index = 0; index < 5; index++) {
            data.push(<Pressable onPress={() => setRating(index)}>
                <Image style={{ marginHorizontal: 4, width: 30, height: 30, resizeMode: 'contain' }} source={index <= rating ? Constants.star_icon : Constants.star} />
            </Pressable>)

        }
        return data;
    }

    const onWriteReview = () => {
        let data = {
            "Description": Description,
            "UserId": props.userId,
            "Rating": rating + 1,
            "Title": EventName,
            "InvitaionId": props.invitationId
        }
        setLoading(true)
        createReview(data).then(res => {
            setLoading(false);
            if (res.Status === 200) {
                props.onPress();
                props.onSuccess();
            } else {
                Utils.showToast(res.Message)
            }
        }).catch(err => {
            setLoading(false);
            Utils.showToast(err.Message)

        })
    }


    return (
        <KeyboardAvoidingView style={{ flex: 1 }} behavior={Constants.platform === 'ios' ? 'padding' : undefined}>
            <View style={styles.container}>
                <View style={styles.subContainer}>
                    <ScrollView>
                        <View style={{ paddingHorizontal: wp(2), flexDirection: 'row', alignItems: 'center', }}>
                            <IconButton
                                disabled={isLoading}
                                icon={'close'}
                                style={{ opacity: 0, }}
                                color={Colors.primary}
                            />
                            <Text style={[styles.title, { flex: 1 }]}>Write Review</Text>
                            <IconButton
                                onPress={() => props.onPress()}
                                icon={'close'}
                                color={Colors.primary}
                            />
                        </View>
                        <View style={styles.formContainer}>
                            <View style={styles.ratingContainer}>{renderRating()}
                            </View>
                            <View style={{ height: hp(2) }} />
                            <View style={{ height: hp(2) }} />
                            <Input
                                onChangeText={(val) => setEventName(val)}
                                placeholder={'Event Name'}
                            />
                            <View style={{ height: hp(2) }} />
                            <Input
                                multiline
                                onChangeText={(val) => setDescription(val)}
                                placeholder={'Event Description'}
                            />
                            <View style={{ height: hp(2) }} />
                            <View style={{ height: hp(2) }} />
                            <Button
                                isLoading={isLoading}
                                disabled={Description === '' || EventName === ''}
                                onPress={() => onWriteReview()} text='Send' />
                        </View>
                    </ScrollView>
                </View>
            </View>
        </KeyboardAvoidingView>
    );
};

export default WriteReviewCard;

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(255,255,255,0.8)',
        flex: 1,
        paddingHorizontal: wp(5),
        justifyContent: 'center',
        alignItems: 'center',
    },
    subContainer: {
        width: wp(90),
        backgroundColor: Colors.white,
        height: hp(80),
        borderRadius: 10,
        ...Constants.boxShadow
    },
    title: {
        fontFamily: Constants.NiveauGroteskBold,
        fontWeight: '700',
        paddingVertical: hp(3),
        textAlign: 'center',
        color: '#0E134F',
        fontSize: wp(5)
    },
    formContainer: {
        paddingHorizontal: wp(5),
        paddingVertical: hp(3),
    },
    ratingContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    }
});
