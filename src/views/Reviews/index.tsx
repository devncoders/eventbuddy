
import React, { useEffect, useState } from 'react';
import {
    View,
    Image,
    StyleSheet,
    FlatList,
    Modal
} from 'react-native';

import { Constants, Colors, Utils } from 'common';
import { ScrollView } from 'react-native-gesture-handler';
import { Button, ErrorView, Header, Spinner } from 'components';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import ReviewItem from './ReviewItem';
import WriteReviewCard from './WriteReviewCard';
import { RouteProp } from '@react-navigation/native';
import { ReviewsItemModel, UsersItemModel } from 'reduxStore/models/users';
import { getUserReviews } from 'reduxStore/services/user';
import { UserModel } from 'reduxStore/models';
import { useSelector } from 'react-redux';
import { RootState } from 'reduxStore/reducers';

let initial = {
    imageData: {} as any,
    loading: false,
    showWriteReview: false,
    searchText: '',
    isFetching: true,
    isLoading: false,
    reviews: [] as Array<ReviewsItemModel>,
    isViewloaded: false
}
interface ReviewsViewProps {
    route: RouteProp<{ params: { user: UsersItemModel, } }, 'params'>
}
const Reviews = (props: ReviewsViewProps) => {
    let user: UsersItemModel = props.route.params.user || {}
    let userData: UserModel = useSelector((state: RootState) => state.main.userData);
    const [{
        isViewloaded,
        isFetching,
        isLoading,
        reviews, showWriteReview
    }, setState] = useState(initial);

    const updateState = (state: {}) => {
        setState(prevState => ({ ...prevState, ...state }));
    }

    const fetchReviews = () => {
        getUserReviews(user.Id).then(res => {
            updateState({ isViewloaded: true, isLoading: false, isFetching: false })
            if (res.Status === 200) {
                updateState({ reviews: res.Data.Reviews });
            }
        }).catch(err => {
            updateState({ isViewloaded: true, isLoading: false, isFetching: false });
        })
    }

    useEffect(() => {
        fetchReviews();
    }, []);

    return <View style={{ flex: 1, backgroundColor: Colors.white }}>
        <Header title={"Reviews"} />
        <View style={{ flex: 1, paddingHorizontal: wp(5) }}>
            {reviews.length !== 0 ? <FlatList
                data={reviews}
                refreshing={isLoading}
                onRefresh={() => fetchReviews()}
                keyExtractor={(item, index) => index.toString()}
                extraData={reviews}
                showsVerticalScrollIndicator={false}
                ListFooterComponent={<View style={{ height: hp(2) }} />}
                ListHeaderComponent={<View style={{ height: hp(2) }} />}
                ItemSeparatorComponent={() => <View style={{ height: hp(2) }} />}
                renderItem={props => <ReviewItem {...props} />}
            /> : isViewloaded && <ErrorView errorMessage='No Reviews!' onRefresh={() => {
                updateState({ isFetching: true });
                fetchReviews();
            }} />}
            {/* isViewloaded && <><View style={{ height: hp(2) }} />
                <Button
                    onPress={() => {
                        if (!userData.IsVerified) {
                            Utils.showToast(Constants.unverifiedMessage);
                            return
                        } else {
                            updateState({ showWriteReview: true })
                        }
                    }}
                    text="Write Review"
                />
                <View style={{ height: hp(2) }} /></> */}
        </View>
        {isFetching && <Spinner />}
        <Modal
            transparent
            animationType='slide'
            visible={showWriteReview}
            onRequestClose={() => updateState({ showWriteReview: false })}>
            <WriteReviewCard
                user={user}
                onSuccess={() => fetchReviews()}
                onPress={() => updateState({ showWriteReview: false })}
            />
        </Modal>
    </View >
}

export default Reviews;

const styles = StyleSheet.create({
    imageView: {
        width: wp(90),
        height: hp(40),
        resizeMode: 'contain'
    },
    categoryItem: {
        elevation: 2,
        borderRadius: 8,
        paddingBottom: 12,
        paddingHorizontal: 12,
        marginHorizontal: 10,
        marginBottom: 10,
    }
});
