import { Colors, Constants } from 'common';
import { ImageView } from 'components';
import _ from 'lodash';
import moment from 'moment';
import * as React from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { ReviewsItemModel } from 'reduxStore/models/users';

interface ReviewItemProps {
    item: ReviewsItemModel,
    hideBorder?: boolean
}

const ReviewItem = (props: ReviewItemProps) => {
    let image = Constants.logo;
    if (!_.isEmpty(props.item.CreatedBy.Media)) {
        image = {
            uri: props.item.CreatedBy.Media.BasePath + props.item.CreatedBy.Media.ThumbPath
        }
    }
    return (
        <View style={styles.container}>
            <View style={styles.userImageContainer}>
                <ImageView style={styles.userImage} source={image} />
            </View>
            <View style={[styles.reviewContainer, props.hideBorder && { borderBottomColor: 'transparent' }]}>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                    <Text numberOfLines={1} style={styles.title}>{props.item.CreatedBy.FullName}</Text>
                    <Text style={styles.time}>{moment(props.item.CreatedAt).fromNow()}</Text>
                </View>
                <View style={{ marginVertical: hp(.5) }}>
                    <AirbnbRating
                        starImage={Constants.star_icon}
                        size={14}
                        defaultRating={props.item.Rating}
                        starContainerStyle={{ alignSelf: 'flex-start', }}
                        selectedColor={Colors.primary}
                        count={props.item.Rating}
                        ratingContainerStyle={{ marginHorizontal: 0 }}
                        isDisabled
                        showRating={false} />
                </View>
                <Text style={styles.description}>{props.item.Description}</Text>
            </View>
        </View>
    );
};

export default ReviewItem;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row'
    },
    userImageContainer: {
        width: wp(15),
        height: wp(18),
        justifyContent: 'center',
        // alignItems: 'center',
    },
    userImage: {
        width: wp(10),
        height: wp(10),
        resizeMode: 'cover',
        borderRadius: wp(10) / 2,
    },
    description: {
        fontSize: wp(3.2),
        color: '#42526E',
        fontFamily: Constants.NiveauGroteskRegular,
    },
    title: {
        fontFamily: Constants.NiveauGroteskBold,
        fontWeight: '700',
        color: '#0E134F',
        fontSize: wp(4.5),
        paddingRight: 8
    },
    time: {
        fontSize: wp(3),
        color: '#42526EB2'
    },
    reviewContainer: {
        flex: 1,
        paddingVertical: hp(1.5),
        borderBottomColor: Colors.borderColor,
        borderBottomWidth: 1,
    }
});
