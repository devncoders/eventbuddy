import axios from "axios";
import { Constants } from "common";
// import { logoutSuccess, updateUserState } from "../actions/main";
// const { store } = configureStore();
/* const { dispatch } = store;
const { keepMeLogin } = store.getState().main;
const userData = store.getState().main */
import _ from 'lodash'
// import { refreshTokenService } from "./user";

const instance = axios.create({
  baseURL: Constants.baseUrl,
  timeout: 20000,
});
/* 
instance.interceptors.response.use((response) => {
  return response
}, async function (error) {
  const originalRequest = error.config;
  if ((error.response.status === 403 || error.response.status === 401) && !originalRequest._retry) {
    const user = _.cloneDeep(store.getState().main.userData);
    originalRequest._retry = true;

    try {
      let newToken = await refreshTokenService(store.getState().main.userData.data.username, store.getState().main.userData.refreshToken);
      user.refreshToken = newToken.result.refreshToken;
      user.token = newToken.result.token;
      user.exp = newToken.result.exp;
      setClientToken(newToken.result.token)
      // store.dispatch(updateUserState(user))
    } catch (error) {
      console.log(error)
    }

    //   user.refreshToken=newToken.result.refreshToken;
    //     user.token=newToken.result.token;
    //  user.iat=newToken.result.iat;
    // user.exp=newToken.result.exp;      
    return instance(originalRequest);
  }
  return Promise.reject(error);
});
 */
export const setClientToken = (token: string) => {
  instance.defaults.headers.common['Authorization'] = `${token}`
};
export default instance;