import { Utils } from "common";
import axios from '.';
import { LoginResp, SignupResp, ProfileResp, UploadMediaResp } from "reduxStore/models/main";
import { NotificationsListResponse, ReviewsListResponse } from "reduxStore/models/users";
export function loginUserService(Email: string, Password: string): Promise<LoginResp> {
  return new Promise(async (resolve, reject) => {
    try {

      const response = await axios.post<LoginResp>(
        `user/login`,
        {
          Email,
          Password
        },
        {
          // headers: {},
        },
      );

      if (response) resolve(response.data);
      else {
        reject('Something went wrong');

      }
    } catch (error: any) {

      reject(error.response.data);
    }
  });
}


export function forgotPasswordService(username: string) {
  return new Promise<LoginResp>(async (resolve, reject) => {
    try {
      const response = await axios.post<LoginResp>('user/forgotpassword', {
        username,
      });
      if (response) resolve(response.data);
      else reject('Something went wrong');
    } catch (error: any) {
      reject(error.response.data);
    }
  });
}

interface RegisterData {
  FullName: string
  Email: string
  Password?: string
}
export function signUpUserService(data: RegisterData): Promise<SignupResp> {
  console.log(data)
  return new Promise(async (resolve, reject) => {

    try {
      const response = await axios.post<SignupResp>(
        `user/signup`,
        { ...data },
        {},
      );
      //console.log(response);
      if (response) resolve(response.data);
      else reject('Something went wrong');
    } catch (error: any) {
      reject(error?.response?.data);
    }
  });
}
export function getProfile(): Promise<ProfileResp> {
  return new Promise(async (resolve, reject) => {

    try {
      const response = await axios.get<ProfileResp>(
        `user/me`,
      );
      if (response) resolve(response.data);
      else reject('Something went wrong');
    } catch (error: any) {
      reject(error?.response?.data);
    }
  });
}
export function getUserProfile(id: number): Promise<ProfileResp> {
  return new Promise(async (resolve, reject) => {

    try {
      const response = await axios.get<ProfileResp>(
        `user/details/${id}`,
      );
      if (response) resolve(response.data);
      else reject('Something went wrong');
    } catch (error: any) {
      reject(error?.response?.data);
    }
  });
}
export function updateProfile(data = {}): Promise<ProfileResp> {
  return new Promise(async (resolve, reject) => {

    try {
      const response = await axios.put<ProfileResp>(
        `user`,
        data
      );
      if (response) resolve(response.data);
      else reject('Something went wrong');
    } catch (error: any) {
      reject(error?.response?.data);
    }
  });
}

export function uploadMedia(data = {}): Promise<UploadMediaResp> {
  return new Promise(async (resolve, reject) => {

    try {
      const response = await axios.post<UploadMediaResp>(
        `media/upload`,
        data
      );
      if (response) resolve(response.data);
      else reject('Something went wrong');
    } catch (error: any) {
      reject(error?.response?.data);
    }
  });
}

export function getNotifications(): Promise<NotificationsListResponse> {
  return new Promise(async (resolve, reject) => {
    try {
      const response = await axios.get<NotificationsListResponse>(
        `notification/`
      );
      if (response) resolve(response.data);
      else reject('Something went wrong');
    } catch (error: any) {
      reject(error?.response?.data);
    }
  });
}

export function getUserReviews(id: number): Promise<ReviewsListResponse> {
  return new Promise(async (resolve, reject) => {
    try {
      const response = await axios.get<ReviewsListResponse>(
        `review/user/${id}`
      );
      if (response) resolve(response.data);
      else reject('Something went wrong');
    } catch (error: any) {
      reject(error?.response?.data);
    }
  });
}
export function createReview(data: {}): Promise<ReviewsListResponse> {
  return new Promise(async (resolve, reject) => {
    try {
      const response = await axios.post<ReviewsListResponse>(
        `review`,
        data
      );
      if (response) resolve(response.data);
      else reject('Something went wrong');
    } catch (error: any) {
      reject(error?.response?.data);
    }
  });
}
