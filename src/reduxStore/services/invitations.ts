import axios from '.'
import { CreateInvitationResponse, CreateInvitationDataModel, InvitationListResponse } from "reduxStore/models/invitation";

export function createInvitation(data: CreateInvitationDataModel): Promise<CreateInvitationResponse> {
  return new Promise(async (resolve, reject) => {
    try {
      const response = await axios.post<CreateInvitationResponse>(
        `invitation`, data
      );
      if (response) resolve(response.data);
      else reject('Something went wrong');
    } catch (error: any) {
      reject(error?.response?.data);
    }
  });
}
export enum InvitationTypes {
  'sent',
  'received',
  'pending',
  'accept',
  'reject'
}

export function updateInvitationRequest(invitationId: number, type: string): Promise<CreateInvitationResponse> {
  return new Promise(async (resolve, reject) => {
    try {
      const response = await axios.put<CreateInvitationResponse>(
        `invitation/${invitationId}/${type}`
      );
      if (response) resolve(response.data);
      else reject('Something went wrong');
    } catch (error: any) {
      reject(error?.response?.data);
    }
  });
}

export function getInvitations(type: string): Promise<InvitationListResponse> {
  return new Promise(async (resolve, reject) => {
    try {
      const response = await axios.get<InvitationListResponse>(
        `invitation/${type}`
      );
      if (response) resolve(response.data);
      else reject('Something went wrong');
    } catch (error: any) {
      reject(error?.response?.data);
    }
  });
}

export function getInvitationById(invitationId: number): Promise<InvitationListResponse> {
  return new Promise(async (resolve, reject) => {
    try {
      const response = await axios.get<InvitationListResponse>(
        `invitation/details/${invitationId}`
      );
      if (response) resolve(response.data);
      else reject('Something went wrong');
    } catch (error: any) {
      reject(error?.response?.data);
    }
  });
}