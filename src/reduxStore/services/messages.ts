import axios from '.'
import { MessageCreateResp, MessageDetailsResp, MessagesListResp } from "reduxStore/models/message";
export function getMessages(): Promise<MessagesListResp> {
  return new Promise(async (resolve, reject) => {
    try {
      const response = await axios.get<MessagesListResp>(
        `invitation/messages/`
      );
      if (response) resolve(response.data);
      else reject('Something went wrong');
    } catch (error: any) {
      reject(error?.response?.data);
    }
  });
}
export function getMessageDetails(chatId: number): Promise<MessageDetailsResp> {
  return new Promise(async (resolve, reject) => {
    try {
      const response = await axios.get<MessageDetailsResp>(
        `chat/${chatId}`
      );
      if (response) resolve(response.data);
      else reject('Something went wrong');
    } catch (error: any) {
      reject(error?.response?.data);
    }
  });
}
export function createChat(data: { Message?: string, InvitationId: number, MediaId?: number }): Promise<MessageCreateResp> {
  return new Promise(async (resolve, reject) => {
    try {
      const response = await axios.post<MessageCreateResp>(
        `chat/`, data
      );
      if (response) resolve(response.data);
      else reject('Something went wrong');
    } catch (error: any) {
      reject(error?.response?.data);
    }
  });
}
/**
 *
 *
 * usman@yopmail.com
 * Usman123!@#
 *
 */