import { GetNearbyUsersReponse } from 'reduxStore/models/users';
import axios from 'reduxStore/services';

export function getNearbyUsers(data: { Lat: number, Lng: number }): Promise<GetNearbyUsersReponse> {
    return new Promise(async (resolve, reject) => {

        try {
            const response = await axios.get<GetNearbyUsersReponse>(
                `user/discover?Lat=${data.Lat}&Lng=${data.Lng}`,
            );
            if (response) resolve(response.data);
            else reject('Something went wrong');
        } catch (error: any) {
            reject(error?.response?.data);
        }
    });
}