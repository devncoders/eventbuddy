import { UserModel } from "reduxStore/models/main"
import { UPDATE_USER_LATLNG, USER_DATA_SUCCESS, USER_TOKEN_UPDATE } from "reduxStore/types"
import { setClientToken } from "reduxStore/services";

export const removeUser = () => {
    return {
        type: USER_DATA_SUCCESS,
        payload: {}
    }
}

export const updateUser = (data: UserModel) => {
    return {
        type: USER_DATA_SUCCESS,
        payload: data
    }
}

export const updateUserLatlng = (latlng: { latitude: number, longitude: number }) => {
    return {
        type: UPDATE_USER_LATLNG,
        payload: latlng
    }
}

export const updateUserToken = (token: string) => {
    setClientToken(token);
    return {
        type: USER_TOKEN_UPDATE,
        payload: token
    }
}
