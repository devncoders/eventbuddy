import { UserModel } from "reduxStore/models/main";
import { Constants } from "common";
import {
    APP_STARTED,
    LOGOUT_SUCCESS,
    USER_DATA_SUCCESS,
    USER_TOKEN_UPDATE,
    UPDATE_USER_LANGUAGE,
    UPDATE_USER_LATLNG,
    UPDATE_EMPLOYEE_CATEGORIES
} from "../../types";


const INITIAL_STATE = {
    userData: {} as UserModel,
    employeeCategories: [] as Array<any>,
    userLatLng: {} as { latitude: 0, longitude: 0 },
    userRadius: 30,
    userToken: '',
    app_started: 0,
    keepMeLogin: false,
};

export default (state = INITIAL_STATE, action: any) => {
    switch (action.type) {
        case APP_STARTED:
            return {
                ...state,
                app_started: 1,
            }
        case UPDATE_EMPLOYEE_CATEGORIES:
            return {
                ...state,
                employeeCategories: action.payload,
            }
        case UPDATE_USER_LANGUAGE:
            return {
                ...state,
                userLanguage: action.payload,
            }
        case UPDATE_USER_LATLNG:
            return {
                ...state,
                userLatLng: action.payload,
            }
        case USER_DATA_SUCCESS:
            return {
                ...state,
                userData: action.payload,
            }
        case USER_TOKEN_UPDATE:
            return {
                ...state,
                userToken: action.payload,
            }
        case LOGOUT_SUCCESS:
            return {
                ...INITIAL_STATE,
                app_started: state.app_started,
            }
        default:
            return state;
    }
}