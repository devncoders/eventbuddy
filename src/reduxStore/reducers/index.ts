import { combineReducers } from 'redux';
// import AsyncStorage from '@react-native-community/async-storage';
// import { persistReducer } from 'redux-persist';
import main from './Persistance/MainReducer';
/* 
const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  keyPrefix: ''
};
const main = persistReducer(persistConfig, MainReducer);
 */
const AppReducer = combineReducers({
  main,
});

export type RootState = ReturnType<typeof AppReducer>

export default AppReducer;
