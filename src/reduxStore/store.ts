import { createStore } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import reducer from "reduxStore/reducers";

const persistConfig = {
    key: "root",
    storage: storage
};

// const middlewares = [thunk];
export let store = null;

export default function configureStore() {
    const persistedReducer = persistReducer(persistConfig, reducer);
    store = createStore(persistedReducer);
    const persistor = persistStore(store);
    return { store, persistor };
}
