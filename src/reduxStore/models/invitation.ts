export interface CreateInvitationDataModel {
    "Title": string
    "Description": string
    "Time": string
    "Lat": number,
    "Lng": number,
    "Address": string
    "UserId": number
}
export interface CreateInvitationResponse {
    "Status": number,
    "Message": string,
    "Data": {
        "Description": string
        "Title": string
        "UserId": number,
        "CreatedById": number,
        "Location": {
            "Lat": number,
            "Lng": number
        },
        "Address": string
        "Time": string
        "CreatedAt": number,
        "UpdatedAt": number,
        "DeletedAt": number,
        "Id": number,
        "Status": string
    }
}
export interface InvitationListResponse {
    "Status": number,
    "Message": string,
    "Data": InvitationItemModel
}

export interface InvitationItemModel {
    "Id": number,
    "CreatedAt": number,
    "UpdatedAt": number,
    "DeletedAt": number,
    "Title": string
    "Description": string
    "Time": string
    "Location": {
        "Lat": number,
        "Lng": number
    },
    "Address": string
    "CreatedById": number,
    "UserId": number,
    "Status": string
    "CreatedBy": {
        "Id": number,
        "FullName": string
        "Media": {
            "Id": number,
            "CreatedAt": number,
            "UpdatedAt": number,
            "BasePath": string
            "Path": string
            "ThumbPath": string
            "UserId": number,
            "Meta": {
                "Name": string
                "Size": number,
                "MimeType": string
                "Extension": string
            },
            "Type": 1,
            "Status": 1
        }
    }
    "User": {
        "Id": number,
        "FullName": string
        "Media": {
            "Id": number,
            "CreatedAt": number,
            "UpdatedAt": number,
            "BasePath": string
            "Path": string
            "ThumbPath": string
            "UserId": number,
            "Meta": {
                "Name": string
                "Size": number,
                "MimeType": string
                "Extension": string
            },
            "Type": 1,
            "Status": 1
        }
    }
}

export interface InvitationDetailsResponse {
    "Status": number,
    "Message": string,
    "Data": Array<InvitationItemModel>
}