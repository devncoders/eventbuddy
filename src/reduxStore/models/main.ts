export interface CardItem {
    title: string
    phone_number: string
    email: string
    website: string
    color?: any
}

export interface LoginResp {
    "Status": number,
    "Message": string,
    "Data": {
        Token: string//"ce6333e0-00e2-44f3-b9a0-2da79e8c0133",
        User: UserModel
    }
}
export interface SignupResp {
    "Status": number,
    "Message": string,
    "Data": {
        "Token": string//"162e6ad6-ccda-4aad-b5f6-73aece7c8726",
        "User": UserModel,
        "DeviceUUID": string
        "AuthCode": number
    }
}
export interface ProfileResp {
    "Status": number,
    "Message": string,
    "Data": {
        "User": UserModel
    }
}
export interface UploadMediaResp {
    "Status": number,
    "Message": string
    "Data": {
        "Id": number,
        "MediaObject": {
            "Meta": {
                "Size": number,
                "Extension": string
                "MimeType": string
                "Name": string
            },
            "BasePath": string
            "Path": string
            "ThumbPath": string
            "UserId": number,
            "Type": number,
            "Status": number,
            "CreatedAt": number,
            "UpdatedAt": number,
            "Id": number
        }
    }
}
export interface UserModel {
    "Id": number,
    "CreatedAt": number,
    "UpdatedAt": number,
    "FullName": string
    "UserName": string
    "BirthDate": string
    "Email": string
    "PhoneNumber": string
    "Type": number,
    "Status": number,
    "IsVerified": boolean,
    "MediaId": number,
    "About": string
    "Media": {
        "Id": number,
        "CreatedAt": number,
        "UpdatedAt": number,
        "BasePath": string
        "Path": string
        "ThumbPath": string
        "UserId": number,
        "Meta": {
            "Name": string
            "Size": number,
            "MimeType": string
            "Extension": string
        },
        "Type": number,
        "Status": number
    },
    "AvailableForInvitation": boolean,
    "Location": {
        "Lat": number
        "Lng": number
    },
}