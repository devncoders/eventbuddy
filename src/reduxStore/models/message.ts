export interface MessageCreateResp {
    "Status": number,
    "Message": string,
    "Data": {
        "Message": string
        "CreatedById": number,
        "InvitationId": number,
        "MediaId": number
        "CreatedAt": number,
        "UpdatedAt": number,
        "Id": number
    }
}

export interface MessagesListResp {
    "Status": number,
    "Message": string,
    "Data": Array<MessageItemModel>
}

export interface MessageItemModel {
    "Id": number,
    "CreatedAt": number,
    "UpdatedAt": number,
    "DeletedAt": number,
    "Title": string
    "Description": string
    "Time": string
    "Location": {
        "Lat": number,
        "Lng": number
    },
    "Address": string
    "CreatedById": number
    "UserId": number
    "Status": string
    "CreatedBy": {
        "Id": number
        "FullName": string
        "Media": {
            "Id": number
            "CreatedAt": number
            "UpdatedAt": number
            "BasePath": string
            "Path": string
            "ThumbPath": string
            "UserId": number
            "Meta": {
                "Name": string
                "Size": number
                "MimeType": string
                "Extension": string
            },
            "Type": number
            "Status": number
        }
    }
    "User": {
        "Id": number
        "FullName": string
        "Media": {
            "Id": number
            "CreatedAt": number
            "UpdatedAt": number
            "BasePath": string
            "Path": string
            "ThumbPath": string
            "UserId": number
            "Meta": {
                "Name": string
                "Size": number
                "MimeType": string
                "Extension": string
            },
            "Type": number
            "Status": number
        }
    }
    "Chats": [
        {
            "Id": number,
            "CreatedAt": number,
            "UpdatedAt": number,
            "Message": string,
            "InvitationId": number,
            "CreatedById": number,
            "MediaId": number
        }
    ]
}

export interface MessageDetailsResp {
    "Status": number,
    "Message": string,
    "Data": Array<MessageItem>
}

export interface MessageItem {
    "Id": number,
    "CreatedAt": number,
    "UpdatedAt": number,
    "Message": string
    "InvitationId": number,
    "CreatedById": number,
    "MediaId": number
    "CreatedBy": {
        "Id": number,
        "FullName": string
        "Media": {
            "Id": number,
            "CreatedAt": number,
            "UpdatedAt": number,
            "BasePath": string
            "Path": string
            "ThumbPath": string
            "UserId": number,
            "Meta": {
                "Name": string
                "Size": number,
                "MimeType": string
                "Extension": string
            },
            "Type": number,
            "Status": number
        }
    },
    "Media": {
        "Id": number,
        "CreatedAt": number,
        "UpdatedAt": number,
        "BasePath": string
        "Path": string
        "ThumbPath": string
        "UserId": number,
        "Meta": {
            "Name": string
            "Size": number,
            "MimeType": string
            "Extension": string
        },
        "Type": number,
        "Status": number
    }
}