export interface GetNearbyUsersReponse {
    "Status": number,
    "Message": string,
    "Data": {
        "Users": Array<UsersItemModel>
    }
}
export interface UsersItemModel {
    "Id": number,
    "CreatedAt": number,
    "UpdatedAt": number,
    "FullName": string
    "UserName": string
    "BirthDate": string
    "Email": string
    "PhoneNumber": string
    "Type": number
    "Status": number
    "MediaId": number,
    "latitude": number,
    "longitude": number,
    "About": string,
    "ReviewCount": number
    "Location": {
        Lat: number,
        Lng: number
    }
    "Media": {
        "Id": number,
        "CreatedAt": number,
        "UpdatedAt": number,
        "BasePath": string
        "Path": string
        "ThumbPath": string
        "UserId": number
        "Meta": {
            "Name": string
            "Size": number
            "MimeType": string
            "Extension": string
        },
        "Type": number
        "Status": number
    }
}

export interface NotificationsListResponse {
    "Status": number,
    "Message": string,
    "Data": {
        notifications: Array<NotificationsItemModel>
    }
}

export interface NotificationsItemModel {
    "Id": number,
    "CreatedAt": number,
    "UpdatedAt": number,
    "ReadStatus": number,
    "UserId": number,
    "Title": string
    "InvitationId": number
    "Description": string
    "Meta": {
        "User": {
            "Id": number,
            "Type": number,
            "About": string
            "Email": string
            "Media": {
                "Id": number,
                "Meta": {
                    "Name": string
                    "Size": number,
                    "MimeType": string
                    "Extension": string
                },
                "Path": string
                "Type": number,
                "Status": number,
                "UserId": number,
                "BasePath": string
                "CreatedAt": number,
                "ThumbPath": string
                "UpdatedAt": number
            },
            "Status": number,
            "MediaId": number,
            "FullName": string
            "Location": string
            "UserName": string
            "BirthDate": string
            "CreatedAt": number,
            "UpdatedAt": number,
            "PhoneNumber": string
        },
        "InvitationId": number
    },
    "Type": number
}
export interface ReviewsListResponse {
    "Status": number,
    "Message": string,
    "Data": {
        Reviews: Array<ReviewsItemModel>,
        AvgRating: number
    }
}

export interface ReviewsItemModel {
    "Id": 11,
    "CreatedAt": 1640523086665,
    "UpdatedAt": 1640523086665,
    "Description": "Test",
    "Rating": 2,
    "InvitationId": null,
    "CreatedById": 15,
    "UserId": 2,
    "CreatedBy": {
        "Id": 15,
        "FullName": "testqwerqwer",
        "Media": {
            "Id": 2,
            "CreatedAt": 1638927307194,
            "UpdatedAt": 1638927307194,
            "BasePath": "event-buddy/",
            "Path": "data/071016056653585351638927306126user.png",
            "ThumbPath": "data/071016056653585351638927306126user.png",
            "UserId": 1,
            "Meta": {
                "Name": "user.png",
                "Size": 12666,
                "MimeType": "image/png",
                "Extension": "png"
            },
            "Type": 1,
            "Status": 1
        }
    }
}