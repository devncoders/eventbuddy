import io from "socket.io-client";

import { Constants } from "common";
import configureStore, { store } from "reduxStore/store";
import { RootState } from "reduxStore/reducers";
// import { changeSocketConnectivityStatus } from "../store/actions";
// let store = configureStore().store;
export const SocketTypes = {
    MESSAGE: "message",
    CHAT_STARTED: "chat_start",
    TYPING: "typing",
    CHAT_JOIN: "chat_join"
};
let userId: any = null;
let userToken: string = '';

export class Socket {
    static setUserDetails = (user_id: number, user_token: string) => {
        userId = user_id;
        userToken = user_token;
    }
    static socket = null;
    static getSocket = () => {
        if (Socket.socket === null) {
            let state: RootState = store.getState()
            let actorId = state.main.userData.Id;
            let authToken = state.main.userToken;
            let connectionUrl = `${Constants.SOCKET_URL}?actorId=${actorId}&authorization=${authToken}`;
            console.log('connectionUrl = = => ', connectionUrl);

            Socket.socket = io(connectionUrl, {
                transports: ["websocket"],
                upgrade: false,
                reconnection: true,
                reconnectionAttempts: Infinity,
                reconnectionDelay: 1000,
                reconnectionDelayMax: 5000,
                pingTimeout: 30000
            });
        }
        return Socket.socket;
    };

    static init = () => {
        Socket.getSocket().on("connect", () => {
            console.log("socket connected!");
            // store.dispatch(changeSocketConnectivityStatus(true));
        });

        Socket.getSocket().on("connect-error", err => {
            console.log("socket connection error", err);
        });

        Socket.getSocket().on("error", error => {
            console.log("socket error", error);
        });

        Socket.getSocket().on("disconnect", reason => {
            console.log("socket disconnected", reason);
            // store.dispatch(changeSocketConnectivityStatus(false));
        });
    };

    static emitChatStarted = (data, cb) => {
        Socket.getSocket().emit(
            SocketTypes.CHAT_STARTED,
            {
                ...data
            },
            cb
        );
    };

    static emitTyping = data => {
        Socket.getSocket().emit(SocketTypes.TYPING, {
            ...data
        });
    };

    static emitMessage = (data, cb) => {
        Socket.getSocket().emit(
            SocketTypes.MESSAGE,
            {
                ...data
            },
            cb
        );
    };

    static emitChatJoin = chatId => {
        Socket.getSocket().emit(SocketTypes.CHAT_JOIN, { ChatId: chatId });
    };

    static onChatStarted = cb => {
        Socket.getSocket().on(SocketTypes.CHAT_STARTED, cb);
    };

    static onMessageTyping = cb => {
        Socket.getSocket().on(SocketTypes.TYPING, cb);
    };

    static onMessageRecieved = cb => {
        Socket.getSocket().on(SocketTypes.MESSAGE, cb);
    };

    static disconnect = () => {
        Socket.getSocket().disconnect();
        Socket.socket = null;
    };

    static remove = (name, listener = null) => {
        if (Socket.socket) {
            if (listener) {
                Socket.getSocket().removeListener(name, listener);
            } else {
                Socket.getSocket().removeAllListeners(name);
            }
        }
    };
}
