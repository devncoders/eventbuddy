import { useNavigation } from "@react-navigation/native";

const useCustomNav = () => {
    const navigation = useNavigation();
    const goTo = (to: string) => navigation.reset({
        index: 0,
        routes: [
            { name: to }
        ]
    });
    return { goTo };
};

export default useCustomNav;