interface ErrorModel {
    errors?: any
}
const ErrorHandler = (error = {} as ErrorModel) => {
    let errorMessage = '';
    if (error.errors && typeof error.errors === 'object') {
        for (const key in error.errors) {
            if (Object.prototype.hasOwnProperty.call(error.errors, key)) {
                const element = error.errors[key];
                errorMessage = element;
                // Utils.showToast(element);

            }
        }
    } else {
        errorMessage = 'Something went wrong'
    }
    return errorMessage;
}
export default ErrorHandler;