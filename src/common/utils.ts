import { ToastAndroid, PermissionsAndroid, Alert, Platform } from "react-native"
import { Constants } from 'common';
import { check, PERMISSIONS, RESULTS, request } from 'react-native-permissions';
import RNFetchBlob from 'rn-fetch-blob';
import { useNavigation } from "@react-navigation/native";
import { useRef } from "react";
import useCustomNav from "./navigationhook";
interface onPress {
    (message: string): void
}

const Utils = {

    showToast: (msg: string, type?: string, callback?: onPress) => {
        if (msg === 'Not Authorized') {
            return
        }
        if (Platform.OS == 'android') {
            ToastAndroid.show(msg, ToastAndroid.SHORT);
        } else {
            let title = type || 'Error';
            Alert.alert(title, msg, [
                { text: 'OK', onPress: () => callback && callback('done') }
            ])
        }
    },
    confirmAlert(title: string, msg: string, callback: onPress) {
        Alert.alert(
            title,
            msg,
            [
                { text: 'No', onPress: () => callback("error") },
                { text: 'Yes', onPress: () => callback("success") },
            ],
            { cancelable: false }
        )
    },
    validateEmail(str: string) {
        var pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return pattern.test(str);
    },
    filterData: (data: Array<any> = [], text: string, key: string) => {
        if (text === '') return data;
        return data.filter(i => i[key].toLowerCase().includes(text.toLowerCase()))
    },
    checkPermissionForUsage: async (permissionTitle: string) => {
        let permission: any = permissionTitle === 'camera' ? Platform.select({
            'ios': PERMISSIONS.IOS.CAMERA,
            'android': PERMISSIONS.ANDROID.CAMERA
        }) : Platform.select({
            'ios': PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
            'android': PERMISSIONS.ANDROID.ACCESS_COARSE_LOCATION
        });
        return new Promise(async (resolve, reject) => {
            let resp = await request(permission);
            console.log(resp);
            switch (resp) {
                case RESULTS.UNAVAILABLE:
                    return reject(RESULTS)
                case RESULTS.DENIED:
                    return reject(RESULTS)
                case RESULTS.LIMITED:
                    return reject(RESULTS)
                case RESULTS.GRANTED:
                    console.log('The permission is granted');
                    return resolve('success')
                case RESULTS.BLOCKED:
                    return reject(RESULTS)
                default:
                    return reject(RESULTS)
            }
        })
    },
    getResolvedPath: async (uri: any) => {
        try {
            return await RNFetchBlob.fs.stat(decodeURIComponent(uri))
        } catch (err) {
            let resolvedPath = decodeURIComponent((uri.split("/").pop()))
            resolvedPath = resolvedPath.indexOf('primary') !== -1 ? `${RNFetchBlob.fs.dirs.SDCardDir}/${resolvedPath.split(":").pop()}` : `/storage/${(resolvedPath.replace(/:/g, "/"))}`
            console.log(err)
            return ({ path: resolvedPath })
        }
    }
}

export { Utils }