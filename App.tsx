/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, { useRef, useState } from 'react';
import {
  SafeAreaView,
  Image,
  useColorScheme,
  View,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import { Provider as PaperProvider, DefaultTheme, Portal } from 'react-native-paper';
import { NavigationContainer, useNavigation } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Colors, Constants } from 'common';
// import DrawerView from 'components/drawer';

import Splash from 'views/Splash';
import Login from 'views/Auth/Login';
import Register from 'views/Auth/Register';
import ForgotPass from 'views/Auth/ForgotPass';
import UploadPicture from 'views/UploadPicture';
import VerifyPicture from 'views/VerifyPicture';
import LocationView from 'views/LocationView';
import Main from 'views/Main';
import ProfileView from 'views/ProfileView';
import Messages from 'views/Messages';
import MessageDetail from 'views/MessageDetail';
import Notifications from 'views/Notifications';
import Settings from 'views/Settings';
import ProfileSettings from 'views/ProfileSettings';
import ChangePassword from 'views/ChangePassword';
import Invitations from 'views/Invitations';
import PrivacyPolicy from 'views/PrivacyPolicy';
import Reviews from 'views/Reviews';
import MainMap from 'views/MainMap';
import SearchView from 'views/SearchView';
import { Provider } from "react-redux";
import configureStore from "reduxStore/store";
import Icon from 'react-native-vector-icons/FontAwesome';
import NetworkLogScreen from 'views/NetworkLogScreen';
import LocationSearchView from 'views/LocationSearchView';


const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    ...Colors
  },
};

const _rennderTabIcon = (props: any, title: string) => {
  var img = Constants.tab_4;
  if (title == 'home') img = Constants.tab_1
  if (title == 'messages') img = Constants.tab_2
  if (title == 'notifications') img = Constants.tab_3

  return <Image style={{
    width: 25,
    height: 25,
    resizeMode: 'contain',
    tintColor: title != 'add' ? props.color : null
  }} source={img} />

}


const Stack = createStackNavigator();
const TabStack = createBottomTabNavigator();

const TabRoutes = () => (
  <TabStack.Navigator

    backBehavior='history'
    tabBarOptions={{
      showLabel: false,
      activeTintColor: Colors.primary,
      inactiveTintColor: Colors.textGrey,
      style: { backgroundColor: Colors.white },
    }}
  >
    <TabStack.Screen options={{ tabBarIcon: (props) => _rennderTabIcon(props, 'home') }} name="home" component={Main} />
    <TabStack.Screen options={{ tabBarIcon: (props) => _rennderTabIcon(props, 'messages') }} name="messages" component={Messages} />
    <TabStack.Screen options={{ tabBarIcon: (props) => _rennderTabIcon(props, 'notifications') }} name="notifications" component={Notifications} />
    <TabStack.Screen options={{ tabBarIcon: (props) => _rennderTabIcon(props, 'settings') }} name="settings" component={Settings} />

  </TabStack.Navigator>
)

const App = () => {
  const navigatorRef = useRef(null);
  const backgroundStyle = {
    backgroundColor: Colors.button1
  };


  return (
    <PaperProvider theme={theme}>
      <Portal>
        <SafeAreaView style={{ flex: 1, ...backgroundStyle }}>

          <Provider store={configureStore().store}>
            <StatusBar barStyle='dark-content' backgroundColor={Colors.white} />
            <NavigationContainer ref={navigatorRef}>
              <Stack.Navigator initialRouteName='splash' headerMode='none' >
                <Stack.Screen name="splash" component={Splash} />
                <Stack.Screen name="login" component={Login} />
                <Stack.Screen name="forgotPass" component={ForgotPass} />
                <Stack.Screen name="register" component={Register} />
                <Stack.Screen name="uploadPicture" component={UploadPicture} />
                <Stack.Screen name="verifyPicture" component={VerifyPicture} />
                <Stack.Screen name="locationView" component={LocationView} />
                <Stack.Screen name="main" component={TabRoutes} />
                <Stack.Screen name="profileView" component={ProfileView} />
                <Stack.Screen name="messages" component={Messages} />
                <Stack.Screen name="messageDetail" component={MessageDetail} />
                <Stack.Screen name="notifications" component={Notifications} />
                <Stack.Screen name="settings" component={Settings} />
                <Stack.Screen name="profileSettings" component={ProfileSettings} />
                <Stack.Screen name="changePassword" component={ChangePassword} />
                <Stack.Screen name="invitations" component={Invitations} />
                <Stack.Screen name="privacyPolicy" component={PrivacyPolicy} />
                <Stack.Screen name="reviews" component={Reviews} />
                <Stack.Screen name="mainMap" component={MainMap} />
                <Stack.Screen name="searchView" component={SearchView} />
                <Stack.Screen name="networkLog" component={NetworkLogScreen} />
                <Stack.Screen name="locationSearchView" component={LocationSearchView} />
              </Stack.Navigator>
            </NavigationContainer>
            {<TouchableOpacity
              style={{
                opacity: 1,
                right: 10,
                bottom: Constants.platform === 'ios' ? 150 : 50,
                backgroundColor: Colors.primary,
                position: 'absolute',
                zIndex: 99999999,
                elevation: 2,
                height: 70,
                width: 70,
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 35,
              }}
              onPress={() => {
                navigatorRef.current?.navigate('networkLog');
              }}>
              <Icon color="#fff" size={29} name="bug" />
            </TouchableOpacity>}
          </Provider>
        </SafeAreaView >
      </Portal >
    </PaperProvider>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
